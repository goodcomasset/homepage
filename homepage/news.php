<?php
/*******************************************************************************
SiteWiN 10 20 30（MySQL版）N3-2
新着情報の内容をFlashに出力するプログラム

※使用する際はファイル名を“news.php”にする事！


*******************************************************************************/

// 設定ファイル＆共通ライブラリの読み込み
if(file_exists('./common/config.php')){
	$com_path = './common/';
}
else{
	$com_path = '../common/';
}
require_once($com_path."config_N3_2.php");		// 共通設定情報
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');				// DB操作クラスライブラリ
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');	// 汎用処理クラスライブラリ

#-------------------------------------------------------------------------
# DBより新着情報のデータを取り出す
#-------------------------------------------------------------------------
$sql = "
SELECT
	RES_ID,
	CATEGORY_CODE,
	TITLE,
	CONTENT,
	URL,
	TARGET_FLG,
	YEAR(DISP_DATE) AS Y,
	MONTH(DISP_DATE) AS M,
	DAYOFMONTH(DISP_DATE) AS D,
	DISPLAY_FLG
FROM
	".N3_2WHATSNEW."
WHERE
	(DISPLAY_FLG = '1' )
AND
	(DEL_FLG = '0')
ORDER BY
	DISP_DATE DESC
LIMIT
	0 , ".N3_2DBMAX_CNT."
";

// ＳＱＬを実行
$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);


		for($i=0;$i<count($fetch);$i++):
		
		//ＨＴＭＬでの表示処理
			//ID
				$id[$i] = $fetch[$i]['RES_ID'];
			
			// 日付
				$time[$i] = $fetch[$i]['Y'].'.'.$fetch[$i]['M'].'.'.$fetch[$i]['D'].' ';
			
			// タイトル
				$title[$i] = ($fetch[$i]['TITLE'])?$fetch[$i]['TITLE']:"&nbsp;";
			
			// カテゴリー
				$category_code[$i] = $fetch[$i]['CATEGORY_CODE'];
			
			// コメント
				$content[$i] = ($fetch[$i]['CONTENT'])?nl2br($fetch[$i]['CONTENT']):"&nbsp";
				
			// URL
				if($fetch[$i]['URL']){
				
					$taget_check = ($fetch[$i]['TARGET_FLG'])?" target=\"_blank\"":"";
					$set_link[$i] = "<a href=\"".$fetch[$i]['URL']."\"".$taget_check.">";
					$set_link2 = "</a>";
				
				}else{
				
					$set_dir = ($fetch[$i]['CATEGORY_CODE'] == 5)?"./about/":"./news/";
					$set_link[$i] = "<a href=\"".$set_dir."#".$fetch[$i]['RES_ID']."\">";
					$set_link2 = "</a>";
				
				}
		
		endfor;

?>
