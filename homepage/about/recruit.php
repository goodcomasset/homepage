<?php 

/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム コントローラー
	

***********************************************************/

// 共通ライブラリ読み込み
require_once('../common/config_S5_1.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');

$sql1 = "
	SELECT
		RES_ID,
		TITLE,
		CONTENT,
		FILE,
		URL,
		FILE2,
		URL2,
		DISPLAY_FLG
	FROM
		".S5_1PRODUCT_LST."
	WHERE
		(RES_ID = '1111111111-111111')
	AND
		(DEL_FLG = '0')
";
$fetch1 = dbOpe::fetch($sql1,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

$sql2 = "
	SELECT
		RES_ID,
		TITLE,
		CONTENT,
		FILE,
		URL,
		FILE2,
		URL2,
		DISPLAY_FLG
	FROM
		".S5_1PRODUCT_LST."
	WHERE
		(RES_ID = '2222222222-222222')
	AND
		(DEL_FLG = '0')
";
$fetch2 = dbOpe::fetch($sql2,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja,zh-tw">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>求人情報｜グッドコムアセット（goodcomasset）とは｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="../js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body onLoad="MM_preloadImages('../common_img/navi1_on.gif','../common_img/navi2_on.gif','../common_img/navi3_on.gif','../common_img/navi4_on.gif','../common_img/navi5_on.gif','../common_img/navi6_on.gif','../common_img/menu1_on.gif','../common_img/menu2_on.gif','../common_img/menu3_on.gif','../common_img/menu4_on.gif','../common_img/header_btn03_on.gif')">
<div id="wrapper">
<div id="seo"><h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1><br class="clear"></div>

<div id="header">
  <span class="left2">
    <h2><a href="/"><img src="/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></a></h2>
  </span>
  <span class="right2">
    <p><img src="/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時～20時"></p>
    <div id="h_gnavi2">
      <ul>
        <li class="s_btn2"><a href="/" title="HOME" onMouseOver="MM_swapImage('header1','','/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
        <li class="s_btn2"><a href="/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
        <li class="s_btn2"><a href="/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
        <li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
      </ul>
    </div>
    <div class="clear"></div><!--ヘッダーナビ終了-->
    <p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
  </span>
  <div class="clear"></div>
</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_06on.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>

	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->

			<p><img src="../common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<p><img src="image/about_left_title01.jpg" width="215" height="33" alt="グッドコムアセットとは　About GOOD COM ASSET" class="mt2"></p>
			<div id="contents_navi">
				<ul>
				<li><a href="index.html" title="ご挨拶" onMouseOver="MM_swapImage('leftnavi1','','image/about_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn01.jpg" alt="ご挨拶" width="215" height="37" id="leftnavi1"></a></li>
				<li><a href="idea.html" title="経営理念" onMouseOver="MM_swapImage('leftnavi2','','image/about_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn02.jpg" alt="経営理念" width="215" height="33" id="leftnavi2"></a></li>
				<li><a href="guideline.html" title="行動規範" onMouseOver="MM_swapImage('leftnavi11','','image/about_btn11_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn11.jpg" alt="行動規範" width="215" height="34" id="leftnavi11"></a></li>
				<li><a href="company.html" title="会社概要" onMouseOver="MM_swapImage('leftnavi3','','image/about_btn03_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn03.jpg" alt="会社概要" width="215" height="34" id="leftnavi3"></a></li>
				<li><a href="enkaku.html" title="沿　革" onMouseOver="MM_swapImage('leftnavi4','','image/about_btn04_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn04.jpg" alt="沿　革" width="215" height="33" id="leftnavi4"></a></li>
				<li><a href="organizational.html" title="組織図" onMouseOver="MM_swapImage('leftnavi5','','image/about_btn05_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn05.jpg" alt="組織図" width="215" height="33" id="leftnavi5"></a></li>
				<li><a href="jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('leftnavi6','','image/about_btn06_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn06.jpg" alt="今。私たちにできること" width="215" height="33" id="leftnavi6"></a></li>
				<li><a href="ir.php" title="IR情報" onMouseOver="MM_swapImage('leftnavi7','','image/about_btn07_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn07_on.jpg" alt="IR情報" width="215" height="34" id="leftnavi7"></a></li>
        <li><a href="map.html" title="アクセスマップ" onMouseOver="MM_swapImage('leftnavi9','','image/about_btn09_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/about_btn09.jpg" alt="アクセスマップ" width="215" height="37" id="leftnavi9"></a></li>
				</ul>
			</div>		
			<p><img src="image/about_left_waku01.jpg" width="215" height="5" alt=""></p>

			<div id="leftmenu">
				<ul class="mt10">
					<li><a href="../property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','../common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/menu2.gif" alt="分譲実績　Results" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','../common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','../common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
					<li><img src="../common_img/okyakusama.png" alt="お客様相談室" width="215" height="214" id="banner5"></li>
				</ul>
			</div><!--左バナー終了-->
            
   <div id="left_info"> 
			<!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','../common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
			</div>
			<p><img src="../common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
		</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="about">
			
			<h3><img src="image/about_title09.jpg" width="655" height="50" alt="グッドコムアセットとは About GOOD COM ASSET 求人情報"></h3>
		 <p class="pan"><a href="../">トップページ</a> > <a href="index.html">グッドコムアセットとは</a> > 求人情報</p>

			<p><img src="image/recruit_ill01.jpg" width="655" height="43" alt="仕事を通じて、成長したい方お待ちしています！"></p>
			
			<div id="recruit_waku">
			<p><img src="image/recruit_waku01.gif" width="378" height="16"></p>
			<p class="haikei"><strong>常に成長していたいという思いを持つ方</strong>に、ぜひ仲間になっていただきたいですね。<br>
			仕事に対して貪欲に、教えてもらうことに対して謙虚になる。<br>そうすることで、成長する可能性を広げてほしいんです。<br><br>
			<strong>素直に他人からの意見を取り入れ、自分の考えや相談事を発信する。</strong><br>自分ひとりではなく、周りのメンバーとコミュニケーションを図りながら仕事を進められるような方をお待ちしています。<br>
		「会社を大きくしていくぞ！」といったやる気をお持ちの方でしたら、経験がなくても私たちがしっかりとフォローしていきます。<br>一緒に今後の展開を考えていきましょう。</p>
			<p><img src="image/recruit_waku02.gif" width="378" height="16"></p>
			</div>

			<h4><img src="image/recruit_title01.jpg" width="655" height="25" alt="新卒採用" class="mt15"></h4>
			<?php if($fetch1[0]["DISPLAY_FLG"] == 1){?>
			<p class="recruit_btn">
			<?php if($fetch1[0]["FILE"]){?>
			<?php if($fetch1[0]["URL"]){?><a href="<?php echo $fetch1[0]["URL"];?>" target="_blank"><?php }?><img src="up_img/<?php echo $fetch1[0]["FILE"];?>"><?php if($fetch1[0]["URL"]){?></a><?php }?>
			<?php }?>
			<?php if($fetch1[0]["FILE2"]){?>
			<?php if($fetch1[0]["URL2"]){?><a href="<?php echo $fetch1[0]["URL2"];?>" target="_blank"><?php }?><img src="up_img/<?php echo $fetch1[0]["FILE2"];?>" class="ml10"><?php if($fetch1[0]["URL2"]){?></a><?php }?>
			<?php }?>
			</p>
			<?php }else{?>
			<p class="recruit_btn"><?php echo $fetch1[0]["CONTENT"];?></p>
			<?php }?>
			<h4><img src="image/recruit_title02.jpg" width="655" height="25" alt="中途採用" class="mt15"></h4>

			<?php if($fetch2[0]["DISPLAY_FLG"] == 1){?>
			<p class="recruit_btn">
			<?php if($fetch2[0]["FILE"]){?>
			<?php if($fetch2[0]["URL"]){?><a href="<?php echo $fetch2[0]["URL"];?>" target="_blank"><?php }?><img src="up_img/<?php echo $fetch2[0]["FILE"];?>"><?php if($fetch2[0]["URL"]){?></a><?php }?>
			<?php }?>
			<?php if($fetch2[0]["FILE2"]){?>
			<?php if($fetch2[0]["URL2"]){?><a href="<?php echo $fetch2[0]["URL2"];?>" target="_blank"><?php }?><img src="up_img/<?php echo $fetch2[0]["FILE2"];?>" class="ml10"><?php if($fetch2[0]["URL2"]){?></a><?php }?>
			<?php }?>
			</p>
			<?php }else{?>
			<p class="recruit_btn"><?php echo $fetch2[0]["CONTENT"];?></p>
			<?php }?>

<div class="line">
<h3> 　　　　■ 募集職種 ： WEBデザイナー</h3>
	<table width="80%" border="1" cellspacing="1" cellpadding="5" align="center">
		<tr>
			<td nowrap valign="top"><font color="#990000"> [ 仕事の内容 ] </font></td>
			<td width="100%">会社ホームページの企画・制作・運用など</td>
		</tr>
		<tr>
			<td nowrap><font color="#990000"> [ 応 募 資 格 ]</font></td>
			<td width="100%">学歴・年齢不問<br>
					Web制作全般に携わった経験をお持ちの方優遇</td>
		</tr>
		<tr>
			<td><font color="#990000"> [ 勤　務　地 ]</font></td>
			<td>本社<br>
            	（東京都新宿区西新宿7丁目20番1号 住友不動産西新宿ビル17階）</td>
		</tr>
		<tr>
			<td valign="top"><font color="#990000"> [ 勤 務 時 間 ]</font></td>
			<td>１０：００～１９：３０</td>
		</tr>
		<tr>
			<td valign="top"><font color="#990000"> [ 休日・休暇 ]</font></td>
			<td>週休２日制（土・日祝）※土曜は当社カレンダーによる<br>
				夏季休暇、年末年始休暇、慶弔休暇、有給休暇</td>
		</tr>
		<tr>
			<td valign="top"><font color="#990000">[ 給与・待遇 ]</font></td>
			<td>経験、能力を考慮の上、当社規定より優遇 <br>
				年収：３５０万円～７００万円<br>
				昇給随時、賞与年２回（６月、１２月）<br>
				通勤手当、住宅手当、家族手当、役職手当、資格手当など </td>
		</tr>
		<tr>
			<td valign="top"><font color="#990000"> [ 福 利 厚 生 ]</font></td>
			<td>社会保険完備、従業員持株制度、社員旅行など</td>
		</tr>
		<tr>
			<td valign="top"> <font color="#990000">[ 応 募 方 法 ]</font></td>
			<td>E-mail又は郵送にて受け付けています。<br>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td nowrap>E-mail　  </td>
						<td nowrap>：</td>
						<td width="100%">
							<div align="left"><A href="mailto:recruit@goodcomasset.co.jp"><font color="#990000">recruit@goodcomasset.co.jp</font></A></div>
						</td>
					</tr>
					<tr>
						<td nowrap>郵送(本社)</td>
						<td nowrap>：</td>
						<td width="100%">
							<div align="left">〒160-0023<br>
                            東京都新宿区西新宿7-20-1<br>
                            住友不動産西新宿ビル17階<br>
                            株式会社グッドコムアセット<br>
                            総務・人事教育部　採用担当宛</div>
						</td>
					</tr>
					<tr>
						<td COLSPAN="3"><br>個人情報の取り扱いについては<a href="http://www.goodcomasset.co.jp/policy/index.html" TARGET="blank">こちら</a>をご覧下さい</td>
					</tr>
				</table>
		</tr>
	</table>
</div>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','../common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="../common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="../policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','../common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="../sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','../common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="../common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>

<map name="Map">
  <area shape="rect" coords="1,183,213,207" href="../chintai/" alt="物件をお探しの方へ" target="_blank">
</map>

<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
