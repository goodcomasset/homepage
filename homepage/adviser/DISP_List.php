<?php
/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム
View：取得したデータをHTML出力

***********************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("HTTP/1.0 404 Not Found");exit();
}


	#--------------------------------------------------------
	# ページング用リンク文字列処理
	#--------------------------------------------------------
	
		//ページリンクの初期化
		$link_prev = "";
		$link_next = "";

		// 次ページ番号
		$next = $p + 1;
		// 前ページ番号
		$prev = $p - 1;
		
		// 商品全件数
		$tcnt = count($fetchCNT);
		// 全ページ数
		$totalpage = ceil($tcnt/S5_2DISP_MAXROW);
	
		// カテゴリー別で表示していればページ遷移もカテゴリーパラメーターをつける
		if($ca)$cpram = "&ca=".urlencode($ca);

		// 前ページへのリンク
		if($p > 1){
			//$link_prev = "<a href=\"".$_SERVER['PHP_SELF']."?p=".urlencode($prev).$cpram."\">&lt;&lt; Prev</a>";
			$link_prev = "<a href=\"".$_SERVER['PHP_SELF']."?p=".urlencode($prev).$cpram."\" title=\"BACK\" onMouseOver=\"MM_swapImage('back1','','image/back_on.jpg',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"image/back.jpg\" alt=\"BACK\" width=\"65\" height=\"17\" id=\"back1\"></a>";
		}

		//次ページリンク
		if($totalpage > $p){
			//$link_next = "<a href=\"".$_SERVER['PHP_SELF']."?p=".urlencode($next).$cpram."\">Next &gt;&gt;</a>";
			$link_next = "<a href=\"".$_SERVER['PHP_SELF']."?p=".urlencode($next).$cpram."\" title=\"NEXT\" onMouseOver=\"MM_swapImage('next1','','image/next_on.jpg',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"image/next.jpg\" alt=\"NEXT\" width=\"65\" height=\"17\" id=\"next1\"></a>";
		}

#-------------------------------------------------------------
# HTTPヘッダーを出力
#	１．文字コードと言語：EUCで日本語
#	２．ＪＳとＣＳＳの設定：する
#	３．有効期限：設定しない
#	４．キャッシュ拒否：設定する
#	５．ロボット拒否：設定しない
#-------------------------------------------------------------
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>アドバイザー紹介｜グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="../js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body onLoad="MM_preloadImages('../common_img/navi1_on.gif','../common_img/navi2_on.gif','../common_img/navi3_on.gif','../common_img/navi4_on.gif','../common_img/navi5_on.gif','../common_img/navi6_on.gif','../common_img/menu1_on.gif','../common_img/menu2_on.gif','../common_img/menu3_on.gif','../common_img/menu4_on.gif')">
<div id="wrapper">
<div id="seo"><h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1><br class="clear"></div>

<div id="header">
  <span class="left2">
    <h2><a href="/"><img src="/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></a></h2>
  </span>
  <span class="right2">
    <p><img src="/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
    <div id="h_gnavi2">
      <ul>
        <li class="s_btn2"><a href="/" title="HOME" onMouseOver="MM_swapImage('header1','','/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
        <li class="s_btn2"><a href="/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
        <li class="s_btn2"><a href="/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
        <li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
      </ul>
    </div>
    <div class="clear"></div><!--ヘッダーナビ終了-->
    <p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
  </span>
  <div class="clear"></div>
</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_04on.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>

	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->
		
			<p><img src="../common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<div id="leftmenu">
				<ul>
					<li><a href="../property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','../common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/menu2.gif" alt="分譲実績　Results" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','../common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','../common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
					<li><img src="../common_img/okyakusama.png" alt="お客様相談室" width="215" height="214" id="banner5"></li>
				</ul>
			</div><!--左バナー終了-->

			<div id="left_info"> 
			<!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','../common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
	    </div>
			<p><img src="../common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
		</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="adviser">
			
			<h3><img src="image/adviser_title11.jpg" width="655" height="50" alt="アドバイザー Adviser 一覧"></h3>
		 <p class="pan"><a href="../">トップページ</a> > アドバイザー紹介（一覧）</p>
			
		<!--	<p><img src="image/adviser_ill01.jpg" width="655" height="388" alt="Adviser list　アドバイザー一覧"></p>-->
			<a name="adviserlist"></a>	
		<p class="line3"><span><img src="image/adviser_ill03.jpg" width="655" height="45" alt="ここでご紹介するのは、当社自慢のアドバイザーばかり。"></span><br>そのため、スカウト会社のターゲットにならないよう氏名をイニシャルで表記させていただいております。<br>あらかじめご了承ください。</p>
<?php
			// 登録がない場合のエラーメッセージ
			echo $disp_no_data;

				// 全商品分ループ(縦ループ)
				for($i=0;$i<count($fetch);$i++){
			?>
			<table border="0" cellspacing="0" cellpadding="0" class="list">
				<tr>
					<td width="211" valign="top">
					<?php if($fetch[$i]["RES_ID"]){?>
					<table width="211" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="image/adviser_title02.jpg" width="149" height="24" alt="Adviser"></td>
                      </tr>
                      <tr>
                        <td><p class="name"><?php echo $fetch[$i]["TITLE"];?></p></td>
                      </tr>
                      <tr>
                        <td><?php if(file_exists("up_img/".$fetch[$i]['RES_ID']."_1.jpg")){?><p><img src="up_img/<?php echo $fetch[$i]['RES_ID'];?>_1.jpg" alt="<?php echo $fetch[$i]["TITLE"];?>" width="204" class="photo"></p><?php }?><p class="btn"><a href="./?id=<?php echo $fetch[$i]['RES_ID'];?>&p=<?php echo $p;?>" title="もっと詳しく知りたい" onMouseOver="MM_swapImage('adviser<?php echo $i;?>','','image/adviser_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/adviser_btn01.jpg" alt="もっと詳しく知りたい" width="163" height="25" id="adviser<?php echo $i;?>"></a></p></td>
                      </tr>
                    </table>
					<?php }?>
					</td>
					<td width="12">&nbsp;</td>
					<td width="211" valign="top">
					
					<?php if($fetch[$i+1]["RES_ID"]){?>
					<table width="211" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="image/adviser_title02.jpg" width="149" height="24" alt="Adviser"></td>
                      </tr>
                      <tr>
                        <td><p class="name"><?php echo $fetch[$i+1]["TITLE"];?></p></td>
                      </tr>
                      <tr>
                        <td><?php if(file_exists("up_img/".$fetch[$i+1]['RES_ID']."_1.jpg")){?><p><img src="up_img/<?php echo $fetch[$i+1]['RES_ID'];?>_1.jpg" alt="<?php echo $fetch[$i+1]["TITLE"];?>" width="204" class="photo"></p><?php }?><p class="btn"><a href="./?id=<?php echo $fetch[$i+1]['RES_ID'];?>&p=<?php echo $p;?>" title="もっと詳しく知りたい" onMouseOver="MM_swapImage('adviser<?php echo ($i+1);?>','','image/adviser_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/adviser_btn01.jpg" alt="もっと詳しく知りたい" width="163" height="25" id="adviser<?php echo ($i+1);?>"></a></p></td>
                      </tr>
                    </table>
					<?php }?>
					
					</td>
					<td width="12">&nbsp;</td>
					<td width="211" valign="top">
					
					<?php if($fetch[$i+2]["RES_ID"]){?>
					<table width="211" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img src="image/adviser_title02.jpg" width="149" height="24" alt="Adviser"></td>
                      </tr>
                      <tr>
                        <td><p class="name"><?php echo $fetch[$i+2]["TITLE"];?></p></td>
                      </tr>
                      <tr>
                        <td><?php if(file_exists("up_img/".$fetch[$i+2]['RES_ID']."_1.jpg")){?><p><img src="up_img/<?php echo $fetch[$i+2]['RES_ID'];?>_1.jpg" alt="<?php echo $fetch[$i+2]["TITLE"];?>" width="204" class="photo"></p><?php }?><p class="btn"><a href="./?id=<?php echo $fetch[$i+2]['RES_ID'];?>&p=<?php echo $p;?>" title="もっと詳しく知りたい" onMouseOver="MM_swapImage('adviser<?php echo ($i+2);?>','','image/adviser_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/adviser_btn01.jpg" alt="もっと詳しく知りたい" width="163" height="25" id="adviser<?php echo ($i+2);?>"></a></p></td>
                      </tr>
                    </table>
					<?php }?>
					
					</td>
				</tr>
			</table>
<?php 
$i=$i+(LINE_MAXCOL-1);}
?>
<table width="655" border="0" cellspacing="0" cellpadding="0" class="list">
				<tr>
					<td width="590" align="left"><?php echo $link_prev;?></td>
				 <td width="65" align="right"><?php echo $link_next;?></td>
				</tr>
			</table>

			<div id="f_banner"><a href="https://ssl.goodcomasset.co.jp/contact" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('f_banner1','','../common_img/header_btn03_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/header_btn03.gif" alt="お問い合わせ・資料請求" width="156" height="22" id="f_banner1"></a></div>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','../common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="../common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="../policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','../common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="../sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','../common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="../common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
