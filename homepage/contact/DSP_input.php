<?php
/************************************************************************
  お問い合わせフォーム（POST渡しバージョン）
 View：入力画面	※デフォルトで表示する画面


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// HTTPヘッダー
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>お問い合わせ・資料請求｜グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="https://ssl.goodcomasset.co.jp/css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="https://ssl.goodcomasset.co.jp/js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="https://ssl.goodcomasset.co.jp/js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script src="https://ssl.goodcomasset.co.jp/js/text_area.js" type="text/javascript"></script>
<script type="text/javascript" src="common.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body onload="setupDes();">
<div id="wrapper">
<div id="seo"><h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1><br class="clear"></div>

	<div id="header">
		<span class="left2"><h2><img src="https://ssl.goodcomasset.co.jp/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></h2></span>
		<span class="right2"><p><img src="https://ssl.goodcomasset.co.jp/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
		<div id="h_gnavi2">
		<ul>
		<li class="s_btn2"><a href="http://www.goodcomasset.co.jp/" title="ホーム" onMouseOver="MM_swapImage('header1','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
		<li class="s_btn2"><a href="http://www.goodcomasset.co.jp/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
		<li class="s_btn2"><a href="http://www.goodcomasset.co.jp/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
		<li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--ヘッダーナビ終了-->
		<p><img src="https://ssl.goodcomasset.co.jp/common_img/space.jpg" width="292" height="13" alt=""></p>
		</span>
		<div class="clear"></div>
	</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="http://www.goodcomasset.co.jp/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>

	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->
		
			<p><img src="https://ssl.goodcomasset.co.jp/common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<div id="leftmenu">
				<ul>
					<li><a href="http://www.goodcomasset.co.jp/property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','https://ssl.goodcomasset.co.jp/common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/menu2.gif" alt="分譲実績　Results" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','https://ssl.goodcomasset.co.jp/common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="https://ssl.goodcomasset.co.jp/common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','https://ssl.goodcomasset.co.jp/common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="https://ssl.goodcomasset.co.jp/common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
				</ul>
			</div><!--左バナー終了-->

			<div id="left_info"> 
			<!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','https://ssl.goodcomasset.co.jp/common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="https://ssl.goodcomasset.co.jp/common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
	  </div>
			<p><img src="https://ssl.goodcomasset.co.jp/common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
		</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="contact">
			
			<h3><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_title01.jpg" width="655" height="50" alt="お問い合わせ・資料請求 Contact us"></h3>
		 <p class="pan"><a href="http://www.goodcomasset.co.jp/">トップページ</a> > お問い合わせ・資料請求</p>
			
			<p>ご意見、ご感想、ご相談はお気軽にお寄せください。<br>お客様の貴重なご意見・ご要望は、今後のサービス改善に役立たせていただきます。</p>
			<h4><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_title02.jpg" width="655" height="25" alt="お電話でのお問い合わせ・資料請求"></h4>
			<p><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_banner01.jpg" width="655" height="103" alt="お問い合わせ時間：平日10時〜20時　フリーダイヤル0800-919-9156"></p>
<a name="t"></a>
			<h4><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_title03.jpg" width="655" height="25" alt="フォームによるお問い合わせ・資料請求"></h4>
		 <p>下記のフォームに必要事項を入力して、入力内容確認画面へお進みください。<br><span class="red2">※印は必須項目です。</span></p>

			<p class="mt10"><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_ill01.jpg" width="655" height="5" alt=""></p>
			<div id="contact_waku">

   <div id="f_area1"> 
   <div id="f_area2"> 

   <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>#t" name="form1" onSubmit="return inputChk(this,false)">
   <table cellpadding="0" cellspacing="0" border="0" summary="お問い合わせフォーム">
    <tr> 
  	  <th><div><span class="red">※</span><label for="issue01">お問い合わせ項目</label></div></th>
     <td>
					<select name="select_name" id="select_name">
					 <option value="">▼選択してください</option>
					 <option value="アドバイザーに相談したい"<?php if($select_name == "アドバイザーに相談したい"){echo " selected";}else{}?>>アドバイザーに相談したい</option>
					 <!--<option value="物件について"<?php if($select_name == "物件について"){echo " selected";}else{}?>>物件について</option>-->
					 <option value="採用について"<?php if($select_name == "採用について"){echo " selected";}else{}?>>採用について</option>
					 <!--<option value="会社について"<?php if($select_name == "会社について"){echo " selected";}else{}?>>会社について</option>-->
					 <option value="その他"<?php if($select_name == "その他"){echo " selected";}else{}?>>その他</option>
                    </select></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="name">お名前</label></div></th>
     <td><input name="name" type="text" id="name" value="<?php echo $name;?>" size="40" style="ime-mode:active;"></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="ruby">お名前（フリガナ）</label></div></th>
					<td><input type="text" name="kana" id="kana" value="<?php echo $kana;?>" class="ime_on"></td>
    </tr>
    <tr> 
    	<th><label for="male">性別</label></th>
  			<td>
					<ul>
     	<li><input type="radio" name="sex" id="male" value="男性"<?php if($sex == "男性"){echo " checked";}else{}?>><label for="male">男性</label></li>
      <li><input type="radio" name="sex" id="female" value="女性"<?php if($sex == "女性"){echo " checked";}else{}?>><label for="female">女性</label></li>
     </ul>
					</td>
    </tr>
    <tr> 
     <th><label for="age">生年月日</label></th>
     <td><input name="year" type="text" class="ime_off" id="year" value="<?php echo $year;?>" maxlength="4">　
     年　<input type="text" name="month" id="month" value="<?php echo $month;?>" maxlength="2" class="ime_off">　
     月　<input type="text" name="day" id="day" value="<?php echo $day;?>" maxlength="2" class="ime_off">　
     日<br><span>（例）2009年12月3日</span></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="zip">住所</label></div></th>
     <td>〒
     <input type="text" name="zip" id="zip" maxlength="8" value="<?php echo $zip;?>" class="ime_off">　<span>（例）123-4567</span><br> <label for="state">都道府県名</label> 
     <select name="state" id="state">
			
			<option value="">▼都道府県を選択してください</option>
<optgroup label="北海道・東北地方">
<option value="北海道"<?php echo ($state == "北海道")?" selected":"";?>>北海道</option>
<option value="青森県"<?php echo ($state == "青森県")?" selected":"";?>>青森県</option>
<option value="岩手県"<?php echo ($state == "岩手県")?" selected":"";?>>岩手県</option>
<option value="秋田県"<?php echo ($state == "秋田県")?" selected":"";?>>秋田県</option>
<option value="宮城県"<?php echo ($state == "宮城県")?" selected":"";?>>宮城県</option>
<option value="山形県"<?php echo ($state == "山形県")?" selected":"";?>>山形県</option>
<option value="福島県"<?php echo ($state == "福島県")?" selected":"";?>>福島県</option>
</optgroup>
<optgroup label="関東地方">
<option value="東京都"<?php echo ($state == "東京都")?" selected":"";?>>東京都</option>
<option value="神奈川県"<?php echo ($state == "神奈川県")?" selected":"";?>>神奈川県</option>
<option value="埼玉県"<?php echo ($state == "埼玉県")?" selected":"";?>>埼玉県</option>
<option value="千葉県"<?php echo ($state == "千葉県")?" selected":"";?>>千葉県</option>
<option value="茨城県"<?php echo ($state == "茨城県")?" selected":"";?>>茨城県</option>
<option value="栃木県"<?php echo ($state == "栃木県")?" selected":"";?>>栃木県</option>
<option value="群馬県"<?php echo ($state == "群馬県")?" selected":"";?>>群馬県</option>
</optgroup>
<optgroup label="甲信越地方">
<option value="山梨県"<?php echo ($state == "山梨県")?" selected":"";?>>山梨県</option>
<option value="長野県"<?php echo ($state == "長野県")?" selected":"";?>>長野県</option>
<option value="新潟県"<?php echo ($state == "新潟県")?" selected":"";?>>新潟県</option>
</optgroup>
<optgroup label="東海地方">
<option value="静岡県"<?php echo ($state == "静岡県")?" selected":"";?>>静岡県</option>
<option value="愛知県"<?php echo ($state == "愛知県")?" selected":"";?>>愛知県</option>
<option value="岐阜県"<?php echo ($state == "岐阜県")?" selected":"";?>>岐阜県</option>
<option value="三重県"<?php echo ($state == "三重県")?" selected":"";?>>三重県</option>
</optgroup>
<optgroup label="北陸地方">
<option value="富山県"<?php echo ($state == "富山県")?" selected":"";?>>富山県</option>
<option value="石川県"<?php echo ($state == "石川県")?" selected":"";?>>石川県</option>
<option value="福井県"<?php echo ($state == "福井県")?" selected":"";?>>福井県</option>
</optgroup>
<optgroup label="近畿地方">
<option value="大阪府"<?php echo ($state == "大阪府")?" selected":"";?>>大阪府</option>
<option value="京都府"<?php echo ($state == "京都府")?" selected":"";?>>京都府</option>
<option value="奈良県"<?php echo ($state == "奈良県")?" selected":"";?>>奈良県</option>
<option value="滋賀県"<?php echo ($state == "滋賀県")?" selected":"";?>>滋賀県</option>
<option value="和歌山県"<?php echo ($state == "和歌山県")?" selected":"";?>>和歌山県</option>
<option value="兵庫県"<?php echo ($state == "兵庫県")?" selected":"";?>>兵庫県</option>
</optgroup>
<optgroup label="中国地方">
<option value="岡山県"<?php echo ($state == "岡山県")?" selected":"";?>>岡山県</option>
<option value="広島県"<?php echo ($state == "広島県")?" selected":"";?>>広島県</option>
<option value="鳥取県"<?php echo ($state == "鳥取県")?" selected":"";?>>鳥取県</option>
<option value="島根県"<?php echo ($state == "島根県")?" selected":"";?>>島根県</option>
<option value="山口県"<?php echo ($state == "山口県")?" selected":"";?>>山口県</option>
</optgroup>
<optgroup label="四国地方">
<option value="香川県"<?php echo ($state == "香川県")?" selected":"";?>>香川県</option>
<option value="徳島県"<?php echo ($state == "徳島県")?" selected":"";?>>徳島県</option>
<option value="愛媛県"<?php echo ($state == "愛媛県")?" selected":"";?>>愛媛県</option>
<option value="高知県"<?php echo ($state == "高知県")?" selected":"";?>>高知県</option>
</optgroup>
<optgroup label="九州・沖縄地方">
<option value="福岡県"<?php echo ($state == "福岡県")?" selected":"";?>>福岡県</option>
<option value="佐賀県"<?php echo ($state == "佐賀県")?" selected":"";?>>佐賀県</option>
<option value="長崎県"<?php echo ($state == "長崎県")?" selected":"";?>>長崎県</option>
<option value="大分県"<?php echo ($state == "大分県")?" selected":"";?>>大分県</option>
<option value="熊本県"<?php echo ($state == "熊本県")?" selected":"";?>>熊本県</option>
<option value="宮崎県"<?php echo ($state == "宮崎県")?" selected":"";?>>宮崎県</option>
<option value="鹿児島県"<?php echo ($state == "鹿児島県")?" selected":"";?>>鹿児島県</option>
<option value="沖縄県"<?php echo ($state == "沖縄県")?" selected":"";?>>沖縄県</option>
</optgroup>
			</select>
     <br> <label for="address">市区町村・番地・マンション名など</label>
					<br> <input type="text" name="address" id="address" value="<?php echo $address;?>" class="ime_on"> 
     </td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="email">メールアドレス</label></div></th>
     <td><input type="text" name="email" id="email" value="<?php echo $email;?>" class="ime_off">　<span>（例）aaa@bbb.jp</span></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="tel">電話番号</label></div></th>
     <td><input type="text" name="tel" id="tel" value="<?php echo $tel;?>" maxlength="13" class="ime_off"><span>　（例）03-1234-5678</span></td>
				</tr>
    <tr> 
  	  <th><label for="fax">ファックス番号</label></th>
     <td><input type="text" name="fax" id="fax" value="<?php echo $fax;?>" maxlength="13" class="ime_off"><span>　（例）03-1234-5678</span></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="comment">お問い合わせ内容</label></div></th>
     <td><textarea name="comment" cols="20" rows="8" id="comment" class="ime_on"><?php echo ($comment)?$comment:"物件のお問い合わせのお客様は物件番号を記載してください。";?></textarea></td>
    </tr>
   </table>
<h3 style="margin-bottom: -5px;"><span class="red">※</span>個人情報の取り扱いについて</h3>
<table border="1" bordercolor="#B0C4DE" cellpadding="0" cellspacing="0" width="640">
<tbody>
<tr>
<td>
<span style="font-size: 85%; color: #000099;"><b>以下をご確認頂き、｢同意する｣にチェックをお願い致します。</b></span>
<br />
<span style="font-size: 85%;">
１. 事業者の氏名又は名称　：　株式会社グッドコムアセット
<br />
２. 個人情報保護管理者（若しくはその代理人）の氏名又は職名、所属及び連絡先
<br />
　　管理者名：個人情報保護管理者　所属部署：株式会社グッドコムアセット　総務・人事教育部　連絡先：03-5338-0170
<br />
３. 個人情報の利用目的
<br />
　　＜お問い合わせ項目：アドバイザーに相談したい＞
<br />
　　　弊社商品・サービスに関する お問い合わせ対応（本人への連絡を含む）のため
<br />
　　＜お問い合わせ項目：採用について＞
<br />
　　　採用に関するお問い合わせ対応（本人への連絡を含む）のため
<br />
　　　採用応募者への連絡と当社の採用業務管理のため
<br />
　　＜お問い合わせ項目：その他＞
<br />
　　　上記以外のお問い合わせ対応（本人への連絡を含む）のため
<br />
４. 個人情報取扱いの委託
<br />
　　当社は事業運営上、前項利用目的の範囲に限って個人情報を外部に委託することがあります。この場合、個人
<br />
　情報保護水準の高い委託先を選定し、個人情報の適正管理・機密保持についての契約を交わし、適切な管理を実施
<br />
　させます。
<br />
５. 個人情報の開示等の請求
<br />
　　ご本人様は、当社に対して本件に関する個人情報の開示等に関して、下記の当社窓口に申し出ることができます。
<br />
　その際、当社はお客様ご本人を確認させていただいたうえで、合理的な期間内に対応いたします。
<br />
　【お問合せ窓口】　〒160-0023　東京都新宿区西新宿７丁目20番１号　住友不動産西新宿ビル17Ｆ
<br />
　　株式会社グッドコムアセット　個人情報問合せ窓口　TEL：03‐5338‐0170 （受付時間　10:00〜18:00※）
<br />
　　　※　土・日曜日、祝日、年末年始、ゴールデンウィーク期間は翌営業日以降の対応とさせていただきます。
<br />
６. 個人情報を提供されることの任意性について
<br />
　　ご本人様が当社に個人情報を提供されるかどうかは任意によるものです。ただし、必要な項目をいただけない場合、
<br />
　適切な対応ができない場合があります。
</span>
</td>
</tr>
</tbody>
</table>
<h3 style="margin-top: -5px;" Align="center"><input type="checkbox"  name="agree"  id="agree" value="同意する"<?php if($agree == "同意する"){echo " checked";}else{}?>   ><label for="agree">個人情報の取り扱いについて同意する</label></h3>

   <div id="formEnd">
    <input type="submit" name="Submit" value="入力内容確認画面へ　&raquo;">
	<input type="hidden" name="action" value="confirm">
   </div>
   </form>
   <div align="center">
   <!--<p>ご入力いただいた情報は、SSLにより暗号化され、安全に送信されます。</p>-->
   </div>

			</div><!--f_area1 end-->
			</div><!--f_area2 end-->

			</div>
			<p><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_ill01.jpg" width="655" height="5" alt=""></p>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','https://ssl.goodcomasset.co.jp/common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="https://ssl.goodcomasset.co.jp/common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="http://www.goodcomasset.co.jp/policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','https://ssl.goodcomasset.co.jp/common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="http://www.goodcomasset.co.jp/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','https://ssl.goodcomasset.co.jp/common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="https://ssl.goodcomasset.co.jp/common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
