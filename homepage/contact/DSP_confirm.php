<?php
/************************************************************************
  お問い合わせフォーム（POST渡しバージョン）
 View：入力画面	※デフォルトで表示する画面


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// HTTPヘッダー
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>お問い合わせ・資料請求｜グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="https://ssl.goodcomasset.co.jp/css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="https://ssl.goodcomasset.co.jp/js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="https://ssl.goodcomasset.co.jp/js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script src="https://ssl.goodcomasset.co.jp/js/text_area.js" type="text/javascript"></script>
<script type="text/javascript" src="common.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body onload="setupDes();">
<div id="wrapper">
<div id="seo"><h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1><br class="clear"></div>

	<div id="header">
		<span class="left2"><h2><img src="https://ssl.goodcomasset.co.jp/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></h2></span>
		<span class="right2"><p><img src="https://ssl.goodcomasset.co.jp/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
		<div id="h_gnavi2">
		<ul>
		<li class="s_btn2"><a href="http://www.goodcomasset.co.jp/" title="ホーム" onMouseOver="MM_swapImage('header1','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
		<li class="s_btn2"><a href="http://www.goodcomasset.co.jp/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
		<li class="s_btn2"><a href="http://www.goodcomasset.co.jp/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
		<li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','https://ssl.goodcomasset.co.jp/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--ヘッダーナビ終了-->
		<p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
		</span>
		<div class="clear"></div>
	</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="http://www.goodcomasset.co.jp/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="http://www.goodcomasset.co.jp/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','https://ssl.goodcomasset.co.jp/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="https://ssl.goodcomasset.co.jp/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>

	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->
		
			<p><img src="https://ssl.goodcomasset.co.jp/common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<div id="leftmenu">
				<ul>
					<li><a href="http://www.goodcomasset.co.jp/property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','https://ssl.goodcomasset.co.jp/common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/menu2.gif" alt="分譲実績　Results" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','https://ssl.goodcomasset.co.jp/common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="https://ssl.goodcomasset.co.jp/common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','https://ssl.goodcomasset.co.jp/common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="https://ssl.goodcomasset.co.jp/common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
				</ul>
			</div><!--左バナー終了-->

			<div id="left_info"> 
			<!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','https://ssl.goodcomasset.co.jp/common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="https://ssl.goodcomasset.co.jp/common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
    </div>
			<p><img src="https://ssl.goodcomasset.co.jp/common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
		</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="contact">
			
			<h3><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_title01.jpg" width="655" height="50" alt="お問い合わせ・資料請求 Contact us"></h3>
		 <p class="pan"><a href="http://www.goodcomasset.co.jp/">トップページ</a> > お問い合わせ・資料請求</p>
			
			<p>ご意見、ご感想、ご相談はお気軽にお寄せください。<br>お客様の貴重なご意見・ご要望は、今後のサービス改善に役立たせていただきます。</p>
			<h4><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_title02.jpg" width="655" height="25" alt="お電話でのお問い合わせ・資料請求"></h4>
			<p><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_banner01.jpg" width="655" height="103" alt="お問い合わせ時間：平日10時〜20時　フリーダイヤル0800-919-9156"></p>
<a name="t"></a>
			<h4><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_title03.jpg" width="655" height="25" alt="フォームによるお問い合わせ・資料請求"></h4>
		 <p>下記の内容で承ります。<br>
ご入力内容に誤りがないか再度ご確認ください。<br>
万が一誤りがあった場合は、「前に戻り修正します」ボタンを押して<br>
入力画面に戻り修正してください。<br>
間違いがなければ「上記の内容で送信します」ボタンを押してください。<br>
後日、担当者よりご連絡いたします。</p>

			<p class="mt10"><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_ill01.jpg" width="655" height="5" alt=""></p>
			<div id="contact_waku">

   <div id="f_area1"> 
   <div id="f_area2"> 

   
   <table cellpadding="0" cellspacing="0" border="0" summary="お問い合わせフォーム">
    <tr> 
  	  <th><div><span class="red">※</span><label for="issue01">お問い合わせ項目</label></div></th>
     <td>
					<?php echo ($select_name)?$select_name:"&nbsp;";?></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="name">お名前</label></div></th>
     <td><?php echo ($name)?$name:"&nbsp;";?></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="ruby">お名前（フリガナ）</label></div></th>
					<td><?php echo ($kana)?$kana:"&nbsp;";?></td>
    </tr>
    <tr> 
    	<th><label for="male">性別</label></th>
  			<td><?php echo ($sex)?$sex:"&nbsp;";?></td>
    </tr>
    <tr> 
     <th><label for="age">生年月日</label></th>
     <td><?php echo ($year)?$year:"&nbsp;";?>年<?php echo ($month)?$month:"&nbsp;";?>月<?php echo ($day)?$day:"&nbsp;";?>日</td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="zip">住所</label></div></th>
     <td>〒
							<?php echo ($zip)?$zip:"&nbsp;";?>
							<br>
							<?php echo ($state)?$state:"&nbsp;";?><?php echo ($address)?$address:"&nbsp;";?>
		     
     </td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="email">メールアドレス</label></div></th>
     <td><?php echo ($email)?$email:"&nbsp;";?></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="tel">電話番号</label></div></th>
     <td><?php echo ($tel)?$tel:"&nbsp;";?></td>
				</tr>
    <tr> 
  	  <th><label for="fax">ファックス番号</label></th>
     <td><?php echo ($fax)?$fax:"&nbsp;";?></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="comment">お問い合わせ内容</label></div></th>
     <td><?php echo ($comment)?nl2br($comment):"&nbsp;";?></td>
    </tr>
    <tr> 
     <th><div><span class="red">※</span><label for="agree">個人情報の取り扱いへの同意</label></div></th>
  			<td><?php echo ($agree)?$agree:"&nbsp;";?></td>
    </tr>
   </table>
   <div align="center">
    <table border="0" cellspacing="0" cellpadding="10" align="center">
  <tr>
    <td width="50%" align="center">
	<div class="contact_submit" align="right">
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>#t">
<input name="Submit" type="submit" value="&lt;&lt;&nbsp;前に戻り修正します">
<input name="select_name" type="hidden" value="<?php echo $select_name;?>">
<input name="company_name" type="hidden" value="<?php echo $company_name;?>">
<input name="company_kana" type="hidden" value="<?php echo $company_kana;?>">
<input name="post" type="hidden" value="<?php echo $post;?>">
<input name="name" type="hidden" value="<?php echo $name;?>">
<input name="kana" type="hidden" value="<?php echo $kana;?>">

<input name="sex" type="hidden" value="<?php echo $sex;?>">
<input name="year" type="hidden" value="<?php echo $year;?>">
<input name="month" type="hidden" value="<?php echo $month;?>">
<input name="day" type="hidden" value="<?php echo $day;?>">

<input name="zip" type="hidden" value="<?php echo $zip;?>">
<input name="state" type="hidden" value="<?php echo $state?>">
<input name="address" type="hidden" value="<?php echo $address;?>">
<input name="tel" type="hidden" value="<?php echo $tel;?>">
<input name="fax" type="hidden" value="<?php echo $fax;?>">
<input name="email" type="hidden" value="<?php echo $email;?>">
<input type="hidden" name="comment" value="<?php echo $comment;?>">
<input name="agree" type="hidden" value="<?php echo $agree;?>">
<input name="occupation" type="hidden" value="<?php echo $occupation;?>">
<input name="mail_mgs" type="hidden" value="<?php echo $mail_mgs;?>">
<input name="type" type="hidden" value="<?php echo $type;?>">
</form>
	</div>
</td>
    <td width="50%" align="center">
	<div class="contact_submit" align="left">
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>#t" onSubmit="return inputChk(this,true)">
<input name="Submit" type="submit" value="上記の内容で送信します&nbsp;&gt;&gt;">
<input name="select_name" type="hidden" value="<?php echo $select_name;?>">
<input name="company_name" type="hidden" value="<?php echo $company_name;?>">
<input name="company_kana" type="hidden" value="<?php echo $company_kana;?>">
<input name="post" type="hidden" value="<?php echo $post;?>">
<input name="name" type="hidden" value="<?php echo $name;?>">
<input name="kana" type="hidden" value="<?php echo $kana;?>">

<input name="sex" type="hidden" value="<?php echo $sex;?>">
<input name="year" type="hidden" value="<?php echo $year;?>">
<input name="month" type="hidden" value="<?php echo $month;?>">
<input name="day" type="hidden" value="<?php echo $day;?>">

<input name="zip" type="hidden" value="<?php echo $zip;?>">
<input name="state" type="hidden" value="<?php echo $state?>">
<input name="address" type="hidden" value="<?php echo $address;?>">
<input name="tel" type="hidden" value="<?php echo $tel;?>">
<input name="fax" type="hidden" value="<?php echo $fax;?>">
<input name="email" type="hidden" value="<?php echo $email;?>">
<input type="hidden" name="comment" value="<?php echo $comment;?>">
<input name="agree" type="hidden" value="<?php echo $agree;?>">
<input name="occupation" type="hidden" value="<?php echo $occupation;?>">
<input name="mail_mgs" type="hidden" value="<?php echo $mail_mgs;?>">
<input name="type" type="hidden" value="<?php echo $type;?>">
<input type="hidden" name="action" value="completion">
</form>
	</div>
</td>
  </tr>
</table></div>
   
   <div align="center">
   <!--<p>ご入力いただいた情報は、SSLにより暗号化され、安全に送信されます。</p>-->
   </div>

			</div><!--f_area1 end-->
			</div><!--f_area2 end-->

			</div>
			<p><img src="https://ssl.goodcomasset.co.jp/contact/image/contact_ill01.jpg" width="655" height="5" alt=""></p>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','https://ssl.goodcomasset.co.jp/common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="https://ssl.goodcomasset.co.jp/common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="http://www.goodcomasset.co.jp/policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','https://ssl.goodcomasset.co.jp/common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="http://www.goodcomasset.co.jp/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','https://ssl.goodcomasset.co.jp/common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="https://ssl.goodcomasset.co.jp/common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>
</html>
