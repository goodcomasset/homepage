<?php
/************************************************************************
 お問い合わせフォーム（POST渡しバージョン）
 処理ロジック：エラーチェック
	※POST送信されたデータに対して不備が無いかチェックする


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}



// 全角英数字→半角英数字に変換
$zip = mb_convert_kana($zip,"a");
/*
$zip1 = mb_convert_kana($zip1,"a");
$zip2 = mb_convert_kana($zip2,"a");
*/
$tel = mb_convert_kana($tel,"a");
$fax = mb_convert_kana($fax,"a");

$email = mb_convert_kana($email,"a");//スタイルのime-modeはIEのみ有効ですので、FireFoxで全角で入力される可能性があるため処理をします。

// フリガナは全角カタカナに統一"C"で全角カタカナ"c"で全角ひらがな
$kana = mb_convert_kana($kana,"C");
$company_kana = mb_convert_kana($company_kana,"C");

//メールアドレスの空白を除去する
$email = ereg_replace("[[:space:]]","",$email);//半角の空白を削除
$email = mb_ereg_replace("(　)","",$email);//全角の空白を削除

#----------------------------------------------------------------------------------
# エラーチェック	※strCheck(対象文字列,モード,偉ーメッセージ);を使用。	
#	0:	未入力チェック
#	1:	空白文字チェック
#	4:	最後の文字に不正な文字が使われているか
#	5:	不正かつ危険な文字が使われているか
#	6:	メールアドレスチェック（E-Mailのみ）
#----------------------------------------------------------------------------------
$error_mes .= utilLib::strCheck($select_name,0,"お問い合わせ項目をご選択ください。<br>\n");

//$error_mes .= utilLib::strCheck($member,0,"会員登録希望の有無をご選択ください。<br>\n");

//$error_mes .= utilLib::strCheck($company_name,0,"会社名をご記入ください。<br>\n");
//$error_mes .= utilLib::strCheck($company_kana,0,"会社名（フリガナ）をご記入ください。<br>\n");

$error_mes .= utilLib::strCheck($name,0,"お名前をご記入ください。<br>\n");
$error_mes .= utilLib::strCheck($kana,0,"お名前（フリガナ）をご記入ください。<br>\n");

/*
if(!empty($zip1) || !empty($zip2)){
	$zip = $zip1."-".$zip2;
	if(ereg("[^-0-9]",$zip)){
		$error_mes .= "郵便番号は半角数字のみでご記入してください。<br>\n";
	}
}
*/

$error_mes .= utilLib::strCheck($zip,0,"郵便番号をご記入ください。<br>\n");
if(!empty($zip)){
	if(ereg("[^-0-9]",$zip)){
		$error_mes .= "郵便番号は半角数字とハイフンのみでご記入してください。<br>\n";
	}
}

$error_mes .= utilLib::strCheck($state,0,"都道府県をご選択ください。<br>\n");
$error_mes .= utilLib::strCheck($address,0,"ご住所をご記入ください。<br>\n");

$error_mes .= utilLib::strCheck($email,0,"メールアドレスをご記入ください。<br>\n");
if($email){
	$mailchk = "";
	$mailchk .= utilLib::strCheck($email,1,true);
	$mailchk .= utilLib::strCheck($email,4,true);
	$mailchk .= utilLib::strCheck($email,5,true);
	$mailchk .= utilLib::strCheck($email,6,true);
	
	if($mailchk)$error_mes .= "メールアドレスの形式に誤りがあります。<br>\n";
}
/*
$error_mes .= utilLib::strCheck($email2,0,"確認用のE-mailをご記入ください。<br>\n");
if($email2){
	$mailchk2 = "";
	$mailchk2 .= utilLib::strCheck($email2,1,true);
	$mailchk2 .= utilLib::strCheck($email2,4,true);
	$mailchk2 .= utilLib::strCheck($email2,5,true);
	$mailchk2 .= utilLib::strCheck($email2,6,true);
	
	if($mailchk2)$error_mes .= "確認用のE-mailの形式に誤りがあります。<br>\n";
}

if($email && $email2){
	if($email != $email2){
		$error_mes .= "E-mailと確認用のE-mailが一致しません。<br>\n";
	}

}
*/

$error_mes .= utilLib::strCheck($tel,0,"電話番号をご記入ください。<br>\n");
if(!empty($tel)){
	if(ereg("[^-0-9]",$tel)){
		$error_mes .= "電話番号は半角数字とハイフンのみでご記入してください。<br>\n";
	}
}

/*
if(!empty($tel)){
	if(ereg("[^-0-9]",$tel)){
		$error_mes .= "電話番号は半角数字とハイフンのみでご記入してください。<br>\n";
	}
}
else{
	$error_mes .= "電話番号をご記入ください。<br>\n";
}
*/

if(!empty($fax)){
	if(ereg("[^-0-9]",$fax)){
		$error_mes .= "ファックス番号は半角数字とハイフンのみでご記入してください。<br>\n";
	}
}

//$error_mes .= utilLib::strCheck($type,0,"ご希望の連絡方法をご選択ください。<br>\n");

$error_mes .= utilLib::strCheck($comment,0,"お問い合わせ内容をご記入ください。<br>\n");

$error_mes .= utilLib::strCheck($agree,0,"個人情報の取り扱いについて同意してください。<br>\n");


	//スパム対策
	//メールの内容に【[/url] [/link] 】が入っていたらスパムと判定する
	$spam_flg = "";
	foreach($_POST as $key => $value){
		
		//該当の文字があるかチェックを行う
			if(substr_count($value,"[/url]")){
				$spam_flg = "1";//該当した場合
			}
		
			if(substr_count($value,"[/link]")){
				$spam_flg = "1";//該当した場合
			}
		
	}
	
	//エラーメッセージ
	if($spam_flg){$error_mes .= "誠に申し訳ございませんが入力に【[/url]】【 [/link] 】をご使用しないでください。<br><br>\n";}

?>
