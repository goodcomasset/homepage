// JavaScript Document

/////////////////////////////////////
// 汎用確認メッセージ
/////////////////////////////////////
function ConfirmMsg(msg){
	return (confirm(msg))?true:false;
}

/////////////////////////////////////////////////////////////////////////////////
// 未入力及び不正入力のチェック（※Safariのバグ（エスケープ文字認識）を回避）
/////////////////////////////////////////////////////////////////////////////////
function inputChk(f,confirm_flg){

	// フラグの初期化
	var flg = false;
	var error_mes = "Error Message\r\n恐れ入りますが、下記の内容をご確認ください\r\n\r\n";

	// 未入力と不正入力のチェック
	
	if(!f.select_name.value){
		error_mes += "・お問い合わせ項目をご選択ください。\r\n";flg = true;
	}
	
	/*
	if(f.action.value == "confirm"){
	
		if(!f.member[0].checked && !f.member[1].checked){
			error_mes += "・会員登録希望の有無をご選択ください。\r\n";flg = true;
		}
	
	}
	if(!f.company_name.value){
		error_mes += "・会社名をご記入ください。\r\n";flg = true;
	}
	
	if(!f.company_kana.value){
		error_mes += "・会社名（フリガナ）をご記入ください。\r\n";flg = true;
	}
	*/
	
	if(!f.name.value){
		error_mes += "・お名前をご記入ください。\r\n";flg = true;
	}
	
	if(!f.kana.value){
		error_mes += "・お名前（フリガナ）をご記入ください。\r\n";flg = true;
	}
	
	
	
	if(!f.zip.value){
		error_mes += "・郵便番号をご記入ください。\r\n";flg = true;
	}
	
	if(!f.state.value){
		error_mes += "・都道府県をご選択ください。\r\n";flg = true;
	}
	
	if(!f.address.value){
		error_mes += "・ご住所をご記入ください。\r\n";flg = true;
	}
	
	
	if(!f.email.value){
		error_mes += "・メールアドレスをご記入ください。\r\n";flg = true;
	}
	else if(!f.email.value.match(/^[^@]+@[^.]+\..+/)){
		error_mes += "・メールアドレスの形式に誤りがあります。\r\n";flg = true;
	}
	
	if(!f.tel.value){
		error_mes += "・電話番号をご記入ください。\r\n";flg = true;
	}

	/*
	if(!f.email2.value){
		error_mes += "・確認用のE-mailをご記入ください。\r\n";flg = true;
	}
	else if(!f.email2.value.match(/^[^@]+@[^.]+\..+/)){
		error_mes += "・確認用のE-mailの形式に誤りがあります。\r\n";flg = true;
	}
	
	if(f.email.value && f.email2.value){
		if(f.email.value != f.email2.value){
			error_mes += "・E-mailと確認用のE-mailが一致しません。\r\n";flg = true;
		}
	
	}
	
	if(f.action.value == "confirm"){
	
		if(!f.type[0].checked && !f.type[1].checked){
			error_mes += "・ご希望の連絡方法をご選択ください。\r\n";flg = true;
		}
	
	}
	*/
	
	if(!f.comment.value){
		error_mes += "・お問い合わせ内容をご記入ください。\r\n";flg = true;
	}
	
	// 判定
	if(flg){
		// アラート表示して再入力を警告
		window.alert(error_mes);return false;
	}
	else{

		// 確認メッセージ
		if(confirm_flg){
			return ConfirmMsg('ご入力いただいた内容で送信します。\nよろしいですか？');
		}
		return true;
	}


}

