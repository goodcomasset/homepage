<?php
/************************************************************************
 お問い合わせフォーム（POST渡しバージョン）
 処理ロジック：最終処理（メール送信）


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// 送信先メールアドレス情報を取得
//$mailto = getInitData("EMAIL1");
$mailto = 'm.higashi@goodcomasset.co.jp;s.morimoto@goodcomasset.co.jp;t.kawamura@goodcomasset.co.jp;t.sumi@goodcomasset.co.jp;t.maeda@goodcomasset.co.jp;k.fujisawa@goodcomasset.co.jp';

#-------------------------------------------------------------------------------------------
# メール送信処理（送信先はindex.phpで設定した$mailto宛）
#-------------------------------------------------------------------------------------------

// Subjectを設定
$subject = "【自動送信メール】Webよりお問い合わせがありました";

// Headerとbodyとsubjectを設定（送信元はお客様 $email）
$headers = "Reply-To: ".$email."\r\n";
$headers .= "Return-Path: ".$email."\r\n";
$headers .= "From: ".mb_encode_mimeheader("自動送信メール")."<{$mailto}>\r\n";

//メールアドレスの入力がある場合は下記の文言を表示する
$disp_words = ($email)?"折り返しご連絡される際は、下記メールアドレス宛にご送信ください。":"";

// メール本文
$mailbody = "
----本メールはメールサーバーから自動的に送信されています。-----

以下URLのフォームより、お客様からお問い合わせをいただきました。

https://".$_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"]."

※このメールに直接ご返信いただくことはできません。
{$disp_words}

●お問い合わせ項目
	$select_name

●お名前
	$name

●お名前（フリガナ）
	$kana

●性別
	$sex

●生年月日
	{$year}年{$month}月{$day}日

●住所
	〒{$zip}
	{$state}{$address}

●メールアドレス
	$email

●電話番号
	$tel

●ファックス番号
	$fax

●お問い合わせ内容：
$comment

●個人情報の取り扱いへの同意
	$agree

========================================================
";

// Subjectを設定
$subject2 = $name." 様　お問い合わせ有り難うございます。";

// Headerとbodyとsubjectを設定（送信元はお客様 $email）
$headers2 = "Reply-To: ".$mailto."\r\n";
$headers2 .= "Return-Path: ".$mailto."\r\n";
$headers2 .= "From: ".mb_encode_mimeheader("自動送信メール")."<{$mailto}>\r\n";

// メール本文
$mailbody2 = "
{$name} 様

お問い合わせ有り難うございます。
株式会社サンメンテナンスです。

後日メールまたはお電話にて担当よりご連絡させていただきます。
また、ご返信に時間がかかる場合がございますのでご了承ください。

万が一本メールに心あたりの無い方は当社までお問い合わせください。

宜しくお願いいたします。

■━━━━━━━━━━━━━━━━━━━━━━━━━━■



■━━━━━━━━━━━━━━━━━━━━━━━━━━■
";

	$mailbody = str_replace("\r","", $mailbody);//qmailでメールを送信するため勝手に改行コードをサーバー側で変換されるのでCRを全て除去LFのみにする（smatとpop3ではこの処理はさせない方がいい）
	$mailbody2 = str_replace("\r","", $mailbody2);//qmailでメールを送信するため勝手に改行コードをサーバー側で変換されるのでCRを全て除去LFのみにする（smatとpop3ではこの処理はさせない方がいい）

//エラーがあれば格納をする、ここでエラーを表示するとデザインが崩れるため
$err_mes = "";

// メール送信実行（結果を取得しコントローラーで次の処理を判断）
if(!empty($mailto) && ereg("^(.+)@(.+)\\.(.+)$",$mailto)){

	$sendmail_result = mb_send_mail($mailto,$subject,$mailbody,$headers);
	//$sendmail_result2 = mb_send_mail($email,$subject2,$mailbody2,$headers2);
	
	if(!$sendmail_result){
	//if(!$sendmail_result || !$sendmail_result2){
		$err_mes = "メール送信に失敗しました<br>\n誠に申し訳ございませんが最初から操作をやり直してください。";
		//utilLib::errorDisp("メール送信に失敗しました<br>\n誠に申し訳ございませんが最初から操作をやり直してください。");
	}
}
else{
	$err_mes = "メールを送信する事が出来ませんでした。<br>\n誠に申し訳ございませんがWebマスター宛に直接メールにて<br>お問い合わせしていただけますようお願い申し上げます";
	//utilLib::errorDisp("メールを送信する事が出来ませんでした。<br>\n誠に申し訳ございませんがWebマスター宛に直接メールにて<br>お問い合わせしていただけますようお願い申し上げます");
}
?>
