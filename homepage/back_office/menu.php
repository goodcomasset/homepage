<?php
/*******************************************************************************
アパレル対応(カテゴリ二つ)
ショッピングカートプログラム バックオフィス

	メニュー画面

2005/4/12 tanaka
*******************************************************************************/
require_once("../common/config.php");
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');		// 汎用処理クラスライブラリ

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
/*
if(!$_SERVER['PHP_AUTH_USER']||!$_SERVER['PHP_AUTH_PW']){
	header("Location: ../index.php");exit();
}
*/
#=============================================================
# HTTPヘッダーを出力
#	文字コードと言語：EUCで日本語
#	他：ＪＳとＣＳＳの設定／キャッシュ拒否／ロボット拒否
#=============================================================
utilLib::httpHeadersPrint("EUC-JP",true,true,true,true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title></title>
<link href="for_bkmanu.css" rel="stylesheet" type="text/css">
</head>
<body>
<div align="center">
<form action="index.php" method="post" target="_parent">
<input type="submit" value="管理画面トップへ" style="width:150px;">
</form>
</div>
<p><strong>処理を選択してください</strong></p>
	<!--メニューテーブル-->
	<table border="0" cellpadding="0" cellspacing="0" width="90%">
		<tr>
			<td class="menutitle">
			▼ 更新プログラム管理
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="n3_2whatsnew/" target="main">お知らせ・IRの更新 </a>
			</td>
		</tr>
		<tr>
			<td class="explanation">
			お知らせ・IRの新規登録や既存データの更新などを行います。
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="s5_2product/" target="main">アドバイザー紹介の更新</a>
			</td>
		</tr>

		<tr>
  		<td class="explanation"> アドバイザー紹介ページの新規登録や既存データの更新などを行います。<br>
			また、表示順番の変更等の管理もできます。
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="s6_1product/" target="main">物件情報・分譲実績の更新</a>
			</td>
		</tr>
		<tr>		
  		<td class="explanation"> 物件情報・分譲実績ページの新規登録や既存データの更新などを行います。<br>
			また、表示順番の変更等の管理もできます。
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="s5_1product/" target="main">採用情報の更新</a>
			</td>
		</tr>
		<tr>		
  		<td class="explanation"> 採用情報ページの新規登録や既存データの更新などを行います。<br>
			また、表示順番の変更等の管理もできます。
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="menutitle">
			▼ 管理情報管理
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="config/change_config.php" target="main">管理情報の更新</a>
			</td>
		</tr>
		<tr>
			<td class="explanation">
			お問合せ用メールアドレスなどを設定します。
			<!--//お問い合せフォームが無い場合//-->
			<!--//管理ID/パスワードの通知用メールアドレスの設定をします。//-->
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="menutitle">
			▼ アクセス解析
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="log/" target="main">アクセス解析</a>
			</td>
		</tr>
		<tr>
			<td class="explanation">
			アクセス状況を解析した結果を表示します。
			</td>
		</tr>
		<tr>
			<td class="space">&nbsp;</td>
		</tr>
		<tr>
			<td class="subtitle">
			・<a href="./fmanager/index.php" target="main">ファイルマネージャー</a>
			</td>
		</tr>
		<tr>
			<td class="explanation">
			アクセスログファイル管理を行います。
			</td>
		</tr>
	</table>
	<div class="largespace"></div>
	<!--メニューテーブルここまで-->
<div align="center">
<form action="index.php" method="post" target="_parent">
<input type="submit" value="管理画面トップへ" style="width:150px;">
</form>
</div>
</body>
</html>