<?php
/*******************************************************************************
Nx系プログラム バックオフィス（MySQL対応版）
View：更新画面表示


*******************************************************************************/
/*
#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#=============================================================
# HTTPヘッダーを出力
#	文字コードと言語：EUCで日本語
#	他：ＪＳとＣＳＳの設定／有効期限の設定／キャッシュ拒否／ロボット拒否
#=============================================================
utilLib::httpHeadersPrint("EUC-JP",true,false,false,true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title>管理画面</title>
<script type="text/javascript" src="inputcheck.js"></script>
<link href="../for_bk.css" rel="stylesheet" type="text/css">
<script src="../tag_pg/cms.js" type="text/javascript"></script>
</head>
<body>
<div class="header"></div>
<p class="page_title"><?php echo N3_2TITLE;?>：既存データ編集画面</p>
<p class="explanation">
▼現在のデータ内容が初期表示されています。<br>
▼内容を編集したい場合は上書きをして「更新する」をクリックしてください。
</p>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data" onSubmit="return confirm_message(this);" style="margin:0px;">
<table width="580" border="1" cellpadding="2" cellspacing="0">
	<tr>
		<th colspan="2" nowrap class="tdcolored">■データの更新</th>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">表示日付：</th>
		<td class="other-td">
		現在表示されている日付です。<br>
		<select name="y">
		<?php for($i=date("Y")-3;$i<=(date("Y")+10);$i++):?>
		<option value="<?php printf("%04d",$i);?>"<?php echo ($fetch[0]["Y"] == $i)?" selected":"";?>>
		<?php echo $i;?>		</option>
		<?php endfor;?>
		</select>
		年
		<select name="m">
		<?php for($i=1;$i<=12;$i++):?>
		<option value="<?php printf("%02d",$i);?>"<?php echo ($fetch[0]["M"] == $i)?" selected":"";?>>
		<?php echo $i;?>		</option>
		<?php endfor;?>
		</select>
		月
		<select name="d">
		<?php for($i=1;$i<=31;$i++):?>
		<option value="<?php printf("%02d",$i);?>"<?php echo ($fetch[0]["D"] == $i)?" selected":"";?>>
		<?php echo $i;?>		</option>
		<?php endfor;?>
		</select>
		日		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">タイトル：</th>
		<td class="other-td">
		<input name="title" type="text" value="<?php echo $fetch[0]["TITLE"];?>" size="60" maxlength="125" style="ime-mode:active">		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">本文：</th>
		<td class="other-td">
			<a href="javascript:void(0)" onClick="CheckObj();addLink(Temp.name); return false;"><img src="../tag_pg/img/link.png" width="16" height="16" alt="リンク" border="0"></a>
			<a href="javascript:void(0)" onClick="CheckObj();addTag(Temp.name,'b'); return false;"><img src="../tag_pg/img/text_bold.png" width="16" height="16" alt="太字" border="0"></a>
			<a href="javascript:void(0)" onClick="CheckObj();addTag(Temp.name,'i'); return false;"><img src="../tag_pg/img/text_italic.png" width="16" height="16" alt="斜体" border="0"></a>
			<a href="javascript:void(0)" onClick="CheckObj();addTag(Temp.name,'u'); return false;"><img src="../tag_pg/img/text_underline.png" width="16" height="16" alt="下線" border="0"></a>
			<a href="javascript:void(0)" onClick="CheckObj();obj=Temp.name;MM_showHideLayers('<?php echo $layer_free;?>',obj.name,'show');OnLink('<?php echo $layer_free;?>',event.x,event.y,event.pageX,event.pageY); return false;"><img src="../tag_pg/img/rainbow.png" alt="テキストカラー" border="0"></a>
			<br>
		<textarea name="content" cols="60" rows="10" style="ime-mode:active" onFocus="SaveOBJ(this)"><?php echo $fetch[0]["CONTENT"];?></textarea></td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">アイコン：</th>
		<td class="other-td">
		  <input name="category_code" id="category_code2" type="radio" value="2"<?php echo ($fetch[0]["CATEGORY_CODE"]==2)?" checked":"";?>><label for="category_code2"><img src="../../image/index_ico02.jpg"></label>
		  <input name="category_code" id="category_code3" type="radio" value="3"<?php echo ($fetch[0]["CATEGORY_CODE"]==3)?" checked":"";?>><label for="category_code3"><img src="../../image/index_ico03.jpg"></label>
		  <input name="category_code" id="category_code4" type="radio" value="4"<?php echo ($fetch[0]["CATEGORY_CODE"]==4)?" checked":"";?>><label for="category_code4"><img src="../../image/index_ico04.jpg"></label>
		  <input name="category_code" id="category_code5" type="radio" value="5"<?php echo ($fetch[0]["CATEGORY_CODE"]==5)?" checked":"";?>><label for="category_code5"><img src="../../image/index_ico05.jpg"></label>		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">URL：</th>
		<td class="other-td">
		<input name="url" type="text" value="<?php echo $fetch[0]["URL"];?>" size="60" style="ime-mode:disabled;">		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">リンク表示方法：</th>
		<td class="other-td">
		<input name="target_flg" id="target_flg1" type="radio" value="1"<?php echo ($fetch[0]["TARGET_FLG"] == "1")?" checked":"";?>><label for="target_flg1">新しいウィンドウ</label>&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="target_flg" id="target_flg2" type="radio" value="0"<?php echo ($fetch[0]["TARGET_FLG"] == "0")?" checked":"";?>><label for="target_flg2">使用しているウィンドウ</label>		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">画像：</th>
		<td class="other-td">
		<?php if(file_exists(N3_2IMG_PATH.$fetch[0]['RES_ID'].".jpg")):?>
		現在表示中の画像<br>
		<img src="<?php echo N3_2IMG_PATH.$fetch[0]['RES_ID'].".jpg";?>?r=<?php echo rand();?>"><br>
		<input type="checkbox" name="del_img" value="1" id="del_img"><label for="del_img">この画像を削除</label>
		<br>
		<?php endif;?>
		アップロード後画像サイズ：<strong>横<?php echo N3_2IMGSIZE_MX;?>px×縦<?php //echo N3_2IMGSIZE_MY."px";?> 自動算出</strong><br>
		<input type="file" name="up_img" value="">	  </td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">資料書類アップロード:</th>
		<td class="other-td">
		<?php if(file_exists(N3_2IMG_PATH.$res_id.".".$old_extension)):?>
		<a href="<?php echo N3_2IMG_PATH.$fetch[0]["RES_ID"].".".$old_extension;?>" target="_blank"><img src="./icon_img/icon_<?php echo $old_extension;?>.jpg" border="0"></a><br>
		ファイルサイズ：<?php echo $old_size;?><br>
		MIMEタイプ　　：<?php echo $old_type;?><br>
		<input type="checkbox" name="del_pdf" value="1" id="dpdf_flg"><label for="dpdf_flg">このファイルを削除</label><br>
		<?php endif;?>
		<input type="file" name="up_file_pdf" value=""><br>
		
		※アップロードファイルサイズ：<strong><?php echo (LIMIT_SIZE - 1);#余裕を持たす為アップできる容量より小さく表記しておく?>MB以内</strong>
	  <?php if(file_exists(N3_2IMG_PATH.$res_id.".".$old_extension)):?>
	  <br><a href="<?php echo N3_2IMG_PATH.$res_id.".".$old_extension;?>" target="_blank"><?php echo N3_2IMG_PATH.$res_id.".".$old_extension;?></a>
	  <?php endif;?>
	  </td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">表示／非表示：</th>
		<td class="other-td">
		<input name="display_flg" id="dispon" type="radio" value="1"<?php echo ($fetch[0]["DISPLAY_FLG"]==1)?" checked":"";?>>
		<label for="dispon">表示</label>&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="display_flg" id="dispoff" type="radio" value="0"<?php echo ($fetch[0]["DISPLAY_FLG"]==0)?" checked":"";?>>
		<label for="dispoff">非表示</label>		</td>
	</tr>
</table>
<input type="submit" value="更新する" style="width:150px;margin-top:1em;">
<input type="hidden" name="action" value="completion">
<input type="hidden" name="regist_type" value="update">
<input type="hidden" name="res_id" value="<?php echo $fetch[0]["RES_ID"];?>">

<input type="hidden" name="old_type" value="<?php echo $old_type;?>">
<input type="hidden" name="old_size" value="<?php echo $old_size;?>">
<input type="hidden" name="old_extension" value="<?php echo $old_extension;?>">

</form>

<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<input type="submit" value="リスト画面へ戻る" style="width:150px;">
</form>

<?php 

//ボタン付近に表示する
cp_disp($layer_free,"0","0");

?>
</body>
</html>