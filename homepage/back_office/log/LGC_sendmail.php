<?php
/************************************************************************
 お問い合わせフォーム（POST渡しバージョン）
 処理ロジック：最終処理（メール送信）

 2004/10/31 Yossee
************************************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("HTTP/1.0 404 Not Found");exit();
}

// 送信先メールアドレス情報を取得
//$mailto = getInitData("EMAIL1");


#-------------------------------------------------------------------------------------------
# メール送信処理（送信先はindex.phpで設定した$mailto宛）
#-------------------------------------------------------------------------------------------

// Subjectを設定
$subject = "【警告】サイトの月間PV数が規定値を越えています";

// Headerとbodyとsubjectを設定（送信元はお客様 $email）
//$headers = "Reply-To: ".$email."\r\n";
//$headers .= "Return-Path: ".$email."\r\n";
$headers = "From: ".mb_encode_mimeheader("自動送信メール")."<{$mailto}>\r\n";

// メール本文
$mailbody = "
以下のサイトのPV数が規定の１０万PVを超えています。

http://".$_SERVER["HTTP_HOST"]."/

管理画面：
http://".$_SERVER["HTTP_HOST"]."/back_office/

";

// メール送信実行（結果を取得しコントローラーで次の処理を判断）
if(!empty($mailto) && ereg("^(.+)@(.+)\\.(.+)$",$mailto)){

	$sendmail_result = mb_send_mail($mailto,$subject,$mailbody,$headers);
	
	if(!$sendmail_result){
		utilLib::errorDisp("警告メールの送信に失敗しました。");
	}else{
		// クッキーで二重投稿の抑制
		$value = 'yes';	
		setcookie("sendmail", $value,time()+3600*24*7);
		
	}
}
else{
	utilLib::errorDisp("警告メールの送信に失敗しました。");
}
?>
