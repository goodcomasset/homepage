<?php
/*******************************************************************************
アクセス解析

	メインコントローラー
	
	SQLite対応版

*******************************************************************************/
/*
#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("HTTP/1.0 404 Not Found"); exit();
}
*/
// 不正アクセスチェックのフラグ
$injustice_access_chk = 1;

// 設定ファイル＆共通ライブラリの読み込み
require_once("../../common/logconfig.php");		// 設定情報
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');					// 汎用処理クラスライブラリ
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/sqliteOpe.php');					// SQLite操作クラスライブラリ

//$mailto = "m_yoshida@zeek.jp";
$mailto = "t_yoshikawa@zeek.jp";

#-------------------------------------------------------------------
# デフォルト表示画面
# 結果一覧表示
#-------------------------------------------------------------------
/*
$dir = opendir(ACCESS_PATH);
while($strfile[] = readdir($dir));
reset($strfile);
closedir($dir);
$size = 0;

foreach($strfile as $v){
	if(strstr($v,"access_log_db")){
		$size = $size + filesize(ACCESS_PATH.$v);
	}
}
*/

include("./LGC_getDBlog.php");

//月間PV数が規定数を超えていたときのメール送信
/*
if(count($fetch)>LOGFILE_SIZE_MAX && ALERT_MAIL_SEND == 1 && !isset($_COOKIE['sendmail'])){
include("./LGC_sendmail.php");
}
*/

include("./DISP_data.php");

?>