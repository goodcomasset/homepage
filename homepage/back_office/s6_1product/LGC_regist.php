<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
Logic：DB登録・更新処理


*******************************************************************************/
/*
#=================================================================================
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#=================================================================================
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
// 不正アクセスチェック（直接このファイルにアクセスした場合）
if(!$accessChk){
	header("Location: ../index.php");exit();
}


#=================================================================================
# POSTデータの受取と文字列処理（共通処理）	※汎用処理クラスライブラリを使用
#=================================================================================
// タグ、空白の除去／危険文字無効化／“\”を取る／半角カナを全角に変換
extract(utilLib::getRequestParams("post",array(8,7,1,4),true));

// MySQLにおいて危険文字をエスケープしておく
$title = utilLib::strRep($title,5);
//$content = utilLib::strRep($content,5);
//$detail_content = utilLib::strRep($detail_content,5);

$title2 = html_tag($_POST['title2']);
$kyachi = html_tag($_POST['kyachi']);
$content = html_tag($_POST['content']);
$hanbaibi = html_tag($_POST['hanbaibi']);
$price = html_tag($_POST['price']);
$meisyo = html_tag($_POST['meisyo']);
$address = html_tag($_POST['address']);
$koutu = html_tag($_POST['koutu']);
$sikichi = html_tag($_POST['sikichi']);
$senyu = html_tag($_POST['senyu']);
$sotosu = html_tag($_POST['sotosu']);
$youto = html_tag($_POST['youto']);
$tate = html_tag($_POST['tate']);
$yoseki = html_tag($_POST['yoseki']);
$detail_content = html_tag($_POST['detail_content']);

#=================================================================================
# 新規か更新かによって処理を分岐	※判断は$_POST["regist_type"]
#=================================================================================
switch($_POST["regist_type"]):
case "update":
//////////////////////////////////////////////////////////
// 対象IDのデータ更新


	// 対象記事IDデータのチェック
	if(!ereg("([0-9]{10,})-([0-9]{6})",$res_id)||empty($res_id)){
		die("致命的エラー：不正な処理データが送信されましたので強制終了します！<br>{$res_id}");
	}

	// 画像ファイル名の決定（POSTで渡された既存の記事ID（$res_id）を使用）
	$for_imgname = $res_id; // POSTで渡された既存記事IDを使用

	// 削除指示がされていたら実行(複数)
	if($_POST["regist_type"]=="update" && $del_img){
		  foreach($del_img as $k => $v){
			if(file_exists(S6_1IMG_PATH.$res_id."_".$v.".jpg")){
			  unlink(S6_1IMG_PATH.$res_id."_".$v.".jpg") or die("画像{$v}の削除に失敗しました。");
			}
			}
	}

	// DB格納用のSQL文
	$sql = "
	UPDATE
		S6_1PRODUCT_LST
	SET
		TITLE = '$title',
		TITLE2 = '$title2',
		
		KYACHI = '$kyachi',
		CONTENT = '$content',
		HANBAIBI = '$hanbaibi',
		
		ITEM_NO = '$item_no',
		PRICE = '$price',
		
		MEISYO = '$meisyo',
		
		ADDRESS = '$address',
		
		URL = '$url',
		DETAIL_URL = '$detail_url',
		KOUTU = '$koutu',
		
		SIKICHI = '$sikichi',
		SENYU = '$senyu',
		
		SOTOSU = '$sotosu',
		YOUTO = '$youto',
		TATE = '$tate',
		YOSEKI = '$yoseki',
		
		RECOMMEND_FLG = '$recommend_flg',
		
		URL2 = '$url2',
		TARGET_FLG = '$target_flg',
		
		DETAIL_CONTENT = '$detail_content',
		
		DISP_DATE = NOW(),
		DISPLAY_FLG = '$display_flg',
		DEL_FLG = '0'
	WHERE
		(RES_ID = '$res_id')
	";
	break;

case "new":
//////////////////////////////////////////////////////////////////
// 新規登録


	// 画像ファイル名の決定（新しいIDを生成して使用。DB登録時のRES_IDにも使用）
	$res_id = $makeID();
	$for_imgname = $res_id;

	// 現在の登録件数が設定した件数未満の場合のみDBに格納
	$cnt_sql = "SELECT COUNT(*) AS CNT FROM S6_1PRODUCT_LST WHERE(DEL_FLG = '0')";
	$fetchCNT = dbOpe::fetch($cnt_sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

	if($fetchCNT[0]["CNT"] < S6_1DBMAX_CNT):

		#-----------------------------------------------------------------
		#	VIEW_ORDER用の値を作成
		#		※現在登録されている記事データ中の最大VIEW_ORDER値を取得
		#		  それに1足したものを$view_orderに格納して使用
		#		※登録場所チェックが入っていたらVIEW_ORDER値を全て1繰上げ
		#		　$view_orderに1をセットし結果的に登録を一番上にする
		#-----------------------------------------------------------------
		
		if($_POST["regist_type"]=="new" && $ins_chk == 1){
			$vosql ="UPDATE S6_1PRODUCT_LST SET VIEW_ORDER = VIEW_ORDER+1";
			if(!empty($vosql)){
				$db_result = dbOpe::regist($vosql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
				if($db_result)die("DB登録失敗しました<hr>{$db_result}");
			}
			$view_order = 1;
		}
		else{
			$vosql = "SELECT MAX(VIEW_ORDER) AS VO FROM S6_1PRODUCT_LST WHERE(DISPLAY_FLG = '1')";
			$fetchVO = dbOpe::fetch($vosql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
			$view_order = ($fetchVO[0]["VO"] + 1);
		}


		
		$sql = "
		INSERT INTO S6_1PRODUCT_LST
			SET
				RES_ID = '$res_id',
				VIEW_ORDER = '$view_order',
						
				TITLE = '$title',
				TITLE2 = '$title2',
				
				KYACHI = '$kyachi',
				CONTENT = '$content',
				HANBAIBI = '$hanbaibi',
				
				ITEM_NO = '$item_no',
				PRICE = '$price',
				
				MEISYO = '$meisyo',
				
				ADDRESS = '$address',
				
				URL = '$url',
				DETAIL_URL = '$detail_url',
				KOUTU = '$koutu',
				
				SIKICHI = '$sikichi',
				SENYU = '$senyu',
				
				SOTOSU = '$sotosu',
				YOUTO = '$youto',
				TATE = '$tate',
				YOSEKI = '$yoseki',
				
				RECOMMEND_FLG = '$recommend_flg',
				
				URL2 = '$url2',
				TARGET_FLG = '$target_flg',
				
				DETAIL_CONTENT = '$detail_content',
				
				DISP_DATE = NOW(),
				DISPLAY_FLG = '$display_flg',
				DEL_FLG = '0'
		";
	else:
		header("Location: {$_SERVER['PHP_SELF']}");
	endif;

	break;
default:
	die("致命的エラー：登録フラグ（regist_type）が設定されていません");
endswitch;

// ＳＱＬを実行
if(!empty($sql)){
	$db_result = dbOpe::regist($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	if($db_result)die("DB登録失敗しました<hr>{$db_result}");	
}

#=================================================================================
# 共通処理；画像アップロード件数分変更処理
#=================================================================================

// 画像処理クラスimgOpeのインスタンス生成
$imgObj = new imgOpe(S6_1IMG_PATH);

// 設定ファイルの画像最大登録枚数分ループ
for($i=1;$i<=S6_1IMG_CNT;$i++):

// アップロードされた画像ファイルがあればアップロード処理
if(is_uploaded_file($_FILES['up_img']['tmp_name'][$i])){

	if($i==1){
		
		// 画像のアップロード：画像名は(記事ID_画像番号.jpg)
		$size1 = getimagesize($_FILES['up_img']['tmp_name'][$i]);
		
		$size_x = S6_1IMGSIZE_MX1;//横の固定サイズ
		$size_y = $size1[1]/($size1[0]/$size_x);
		
		$imgObj->setSize($size_x,$size_y);
		
	}else{
		
		// 画像のアップロード：画像名は(記事ID_画像番号.jpg)
		$size2 = getimagesize($_FILES['up_img']['tmp_name'][$i]);
		
		$size_x2 = S6_1IMGSIZE_MX2;//横の固定サイズ
		$size_y2 = $size2[1]/($size2[0]/$size_x2);
		
		$imgObj->setSize($size_x2,$size_y2);
		
	}
	/*
	//定数を配列に格納しておく
	$ox = array(S6_1IMGSIZE_MX1,S6_1IMGSIZE_MX2,S6_1IMGSIZE_LX,S6_1IMGSIZE_LX,S6_1IMGSIZE_LX);
	$oy = array(S6_1IMGSIZE_MY1,S6_1IMGSIZE_MY2,S6_1IMGSIZE_LY,S6_1IMGSIZE_LY,S6_1IMGSIZE_LY);

	// 画像のアップロード：画像名は(記事ID_画像番号.jpg)
	$imgObj->setSize($ox[$i-1],$oy[$i-1]);
	$imgObj->isFixed=true;
	*/
	if(!$imgObj->up($_FILES['up_img']['tmp_name'][$i],$for_imgname."_".$i)){
		exit("画像のアップロード処理に失敗しました。");
	}

}
endfor;
?>