<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
Logic：ＤＢ情報取得処理ファイル


*******************************************************************************/
/*
#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#--------------------------------------------------------------------------------
# 選択された処理action（$_POST["action"]）により発行するＳＱＬを分岐
#--------------------------------------------------------------------------------
switch($_POST["action"]):
case "update":
///////////////////////////////////////////
// 更新指示のあった該当記事データの取得

	// POSTデータの受け取りと共通な文字列処理
	extract(utilLib::getRequestParams("post",array(8,7,1,4)));

	// 対象記事IDデータのチェック
	if(!ereg("([0-9]{10,})-([0-9]{6})",$res_id)||empty($res_id)){
		die("致命的エラー：不正な処理データが送信されましたので強制終了します！<br>{$res_id}");
	}

	$sql = "
	SELECT
	    RES_ID,
		TITLE,
		TITLE2,
		
		KYACHI,
		CONTENT,
		HANBAIBI,
		
		ITEM_NO,
		PRICE,
		
		MEISYO,
		
		ADDRESS,
		
		URL,
		DETAIL_URL,
		KOUTU,
		
		SIKICHI,
		SENYU,
		
		SOTOSU,
		YOUTO,
		TATE,
		YOSEKI,
		
		RECOMMEND_FLG,
		
		URL2,
		TARGET_FLG,
		
		DETAIL_CONTENT,
		DISPLAY_FLG
	FROM
		S6_1PRODUCT_LST
	WHERE
		(RES_ID = '$res_id')
	";

	break;
default:
///////////////////////////////////////////
// 記事リスト一覧用データの取得と

	// オススメ商品の件数チェック
	$sqlRE = "
	SELECT
		RES_ID
	FROM
		S6_1PRODUCT_LST
	WHERE
		(DEL_FLG='0')
	AND
		(RECOMMEND_FLG='1')
	";
	$fetchre = dbOpe::fetch($sqlRE,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	
	// 一覧表示用データの取得（リスト順番は設定ファイルに従う）
	$sql = "
	SELECT
		RES_ID,
		TITLE,
		RECOMMEND_FLG,
		YEAR(DISP_DATE) AS Y,
		MONTH(DISP_DATE) AS M,
		DAYOFMONTH(DISP_DATE) AS D,
		VIEW_ORDER,DISPLAY_FLG
	FROM
		S6_1PRODUCT_LST
	WHERE
		(DEL_FLG = '0')
	ORDER BY
		VIEW_ORDER ASC
	";

endswitch;

// ＳＱＬを実行
$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

?>