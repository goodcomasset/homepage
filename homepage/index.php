<?php 

$path_flg = 1;

include("news.php");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja,zh-tw">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script src="js/ie7_flash.js" type="text/javascript"></script>
<script type="text/javascript"></script>
 <link href="css/flexslider.css" rel="stylesheet" type="text/css" />
 <script src="js/jquery-2.1.1.js"></script>
 <script src="js/jquery.flexslider.js"></script>
 <script type="text/javascript" charset="utf-8">

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
	slideshowSpeed: 5000, 
    animationDuration: 1500,
    animationLoop: false,
	closeOnClick: true,
	controlNav: false,
	directionNav: false,
    pauseOnHover: false,  });
});

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--[if lt IE 9]><script src="./js/html5shiv-printshiv.js"></script><![endif]-->
</head>

<body onLoad="MM_preloadImages('common_img/navi1_on.gif','common_img/navi2_on.gif','common_img/navi3_on.gif','common_img/navi4_on.gif','common_img/navi5_on.gif','common_img/navi6_on.gif','common_img/menu1_on.gif','common_img/menu2_on.gif','common_img/menu3_on.gif','common_img/menu4_on.gif')">
<div id="wrapper">
  <div id="seo">
    <h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1>
    <br class="clear">
  </div>
  <div id="header"> <span class="left2">
    <h2><img src="/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></h2>
    </span> <span class="right2">
    <p><a href="/tw/" title="繁體中文">繁體中文</a>
    <p><img src="/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
    <div id="h_gnavi2">
      <ul>
        <li class="s_btn2"><a href="/" title="ホーム" onMouseOver="MM_swapImage('header1','','/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
        <li class="s_btn2"><a href="/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
        <li class="s_btn2"><a href="/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
        <li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
      </ul>
    </div>
    <div class="clear"></div>
    <!--ヘッダーナビ終了-->
    <p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
    </span>
    <div class="clear"></div>
  </div>
  <!--ヘッダー終了-->
  
  <div id="gnavi">
    <ul>
      <li><a href="/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
      <li><a href="/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
      <li><a href="/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
      <li><a href="/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
      <li><a href="/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
      <li><a href="/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
    </ul>
    <div class="clear"></div>
    <!--float.hack box--> 
  </div>
  <div id="main">
   <div class="flexslider">
       <ul class="slides">
        <li><img src="common_img/top/top_1.jpg" /></li>
        <li><img src="common_img/top/top_2.jpg" /></li>
        <li><img src="common_img/top/top_3.jpg" /></li>
        <li><img src="common_img/top/top_4.jpg" /></li>
        <li><img src="common_img/top/top_5.jpg" /></li>
      </ul>
     </div>
			<p><img src="common_img/top/top_pict2.jpg"></p>
            <br><br>

    <div id="leftside"><!--左カラムここから-->

      <p><img src="common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
      <div id="leftmenu">
        <ul>
          <li><a href="property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="common_img/menu2.gif" alt="分譲実績　Results" width="215" height="48" border="0" id="leftmenu2"></a></li>
        </ul>
      </div>
      <!--左メニュー終了-->
      
      <div id="banner">
        <ul>
          <li><a href="/about/anniversary.html" title="設立10周年のご挨拶" onMouseOver="MM_swapImage('banner6','','common_img/10th_4.png',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="/common_img/10th_4.png" alt="設立10周年のご挨拶" width="216" height="180" id="banner6"></a></li>
          <li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="/common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
          <li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="/common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
          <li><img src="/common_img/okyakusama2.png" alt="お客様相談室" width="216" height="114" id="banner5"></li>
        </ul>
      </div>
      <!--左バナー終了-->
      
      <p><img src="common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
    </div>
    <!--左カラム終了-->

    <div id="content"><!--メインコンテンツ-->
      
      <div id="top-page">
        <br><br>
        <table width="655" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="359"><p><img src="image/index_title01.jpg" width="267" height="25" alt="お知らせ"><a href="news/" title="一覧" onMouseOver="MM_swapImage('index2','','image/index_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/index_btn02.jpg" alt="一覧" width="42" height="25" id="index2"></a><a href="rss.php" title="RSS" target="_blank" onMouseOver="MM_swapImage('rss1','','image/rss_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/rss.jpg" alt="RSS" name="rss1" width="50" height="25" border="0" id="rss1"></a></p>
              <p><img src="image/index_line01.jpg" width="359" height="3"></p>
              <div id="nProgram">
                <dl>
                  <?php for($i=0;$i < count($fetch);$i++):?>
                  <dt>
                    <p class="hiduke"><span class="n_left"><?php echo $set_link[$i];?><?php echo $time[$i];?><?php echo $set_link2;?></span><span class="n_right"><img src="image/index_ico0<?php echo $category_code[$i];?>.jpg"></span>
                    <div class="clear"></div>
                    </p>
                    <p class="text"><?php echo $set_link[$i];?><?php echo $title[$i];?><?php echo $set_link2;?></p>
                  </dt>
                  <?php endfor;?>
                </dl>
              </div></td>
            <td width="25">&nbsp;</td>
            <td width="271">
			<p><img src="image/lifeplan.png" alt="年金問題や税金などで、将来に不安を抱えていませんか？　生涯にわたるパートナーとして幸せなライフプランを提案いたします。"></p>
			<p class="index_btn"><a href="future/index.html" title="将来に不安を抱えている皆様はこちらから" onMouseOver="MM_swapImage('index3','','image/index_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/index_btn01.jpg" alt="将来に不安を抱えている皆様はこちらから" width="258" height="25" id="index3"></a></p>
            <p><img src="image/index_title02.jpg" width="271" height="75" alt="任せて安心、一括マンション経営！安心のサポート体制〜G・Cシステムのご紹介〜"></p>
              <p><img src="image/index_ill02.jpg" width="271" height="168" alt="オーナーの皆様にマンション経営を円滑に行っていただけるように独自の管理システムをご用意いたします。優良な入居者の確保、家賃滞納者への集金、更新手続き、室内のクリーニングから確定申告のお手伝いまで、一貫してサポートします。"></p>
              <p><img src="image/index_ill03.jpg" width="137" height="23" alt=""><a href="future/support.html" title="詳しくはこちら" onMouseOver="MM_swapImage('index1','','image/index_btn03_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/index_btn03.jpg" alt="詳しくはこちら" width="126" height="23" border="0" id="index1"></a><img src="image/index_ill04.jpg" width="8" height="23" alt=""></p>
              <p><img src="image/index_ill05.jpg" width="271" height="17" alt=""></p></td>
          </tr>
        </table>
      </div>
      <!--トップ・コンテンツ終了--> 
      
    </div>
    <!--コンテンツ終了-->
    
    <div class="clear"></div>
    <!--float.hack box-->
    
    <div id="f_gnavi">
      <ul>
        <li><img src="common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
        <li><a href="policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
        <li><a href="sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
      </ul>
    </div>
    <div class="clear"></div>
    <!--フッターナビ終了--> 
    
  </div>
  <!--コンテンツラッパー終了--> 
  
</div>
<!--ラッパー終了-->

<div id="footer">
  <p><img src="common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
  <table cellpadding="0" cellspacing="0" summary="フッターテーブル">
    <tr>
      <th><a href="http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" title="FLASHプラグインをダウンロード" onfocus="this.blur()" onblur="this.blur()" target="_blank"><img src="image/get_flash_player.gif" alt="GET Flash Player !" width="112" height="32"></a></th>
      <td><a href="http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" title="FLASHプラグインをダウンロード" target="_blank">このページを正常に閲覧するためには、最新のFlash Playerが必要です。<br>
        こちらからダウンロードしてください（無料）</a></td>
      <td><div id="ai"></div></td>
    </tr>
  </table>
</div>
<!--フッター終了-->
</body>
<map name="Map">
  <area shape="rect" coords="-29,-971,-2,-959" href="" title="" alt="">
  <area shape="rect" coords="4,183,212,206" href="chintai/" alt="物件をお探しの方へ" target="_blank">
</map>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="./log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
