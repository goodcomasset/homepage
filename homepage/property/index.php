<?php
/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム コントローラー
	

***********************************************************/
error_reporting (E_ALL);
//error_reporting (E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
error_reporting (E_ERROR | E_WARNING | E_PARSE);
ini_set('track_errors',1);
ini_set('display_errors',1);

// 共通ライブラリ読み込み
require_once('../common/config_S6_1.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/tmpl2.class.php');// テンプレートクラスライブラリ

#=============================================================================
# 共通処理：GETデータの受け取りと共通な文字列処理
#=============================================================================
if($_GET)extract(utilLib::getRequestParams("get",array(8,7,1,4,5)));

	// 不正アクセスチェックのフラグ
	$injustice_access_chk = 1;

	// 商品情報取得
	include("LGC_getDB-data.php");
	
	if($ca){
	
		// 取得件数分のデータをHTML出力
		include("DISP_List2.php");
	
	}else{
		
		// 商品IDが送信されパラメーターが不正でなければ商品詳細を表示
		if($_GET['id']){
			
			// 取得件数分のデータをHTML出力
			include("DISP_detail.php");
		
		}else{
		
			include("DISP_List.php");
		
		}
		
	}

?>