<?php
/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム
View：取得したデータをHTML出力

***********************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("Location: ../");exit();
}

#-------------------------------------------------------------
# HTTPヘッダーを出力
#	１．文字コードと言語：EUCで日本語
#	２．ＪＳとＣＳＳの設定：する
#	３．有効期限：設定しない
#	４．キャッシュ拒否：設定する
#	５．ロボット拒否：設定しない
#-------------------------------------------------------------
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、投資用マンション（<?php echo $fetch[0]["TITLE"];?> ）の販売を通じて、お客様の資産運用のお手伝いをしております。東京23区の賃貸マンション、ワンルームマンションの経営、マンション投資、マンション経営をお考えの方は、お気軽にご相談ください。">
<meta name="keywords" content="<?php echo $fetch[0]["TITLE"];?>,グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title><?php echo $fetch[0]["TITLE"];?>｜グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="../js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>

</head>

<body onLoad="MM_preloadImages('../common_img/navi1_on.gif','../common_img/navi2_on.gif','../common_img/navi3_on.gif','../common_img/navi4_on.gif','../common_img/navi5_on.gif','../common_img/navi6_on.gif','../common_img/menu1_on.gif','../common_img/menu2_on.gif','../common_img/menu3_on.gif','../common_img/menu4_on.gif')">
<div id="wrapper">
<div id="seo"><h1>東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営なら株式会社グッドコムアセットが提供する<?php echo $fetch[0]["TITLE"];?>。</h1><br class="clear"></div>

	<div id="header">
		<span class="left2"><h2><img src="/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></h2></span>
		<span class="right2"><p><img src="/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
		<div id="h_gnavi2">
		<ul>
		<li class="s_btn2"><a href="/" title="ホーム" onMouseOver="MM_swapImage('header1','','/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
		<li class="s_btn2"><a href="/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
		<li class="s_btn2"><a href="/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
		<li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--ヘッダーナビ終了-->
		<p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
		</span>
		<div class="clear"></div>
	</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>

	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->
		
			<p><img src="../common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<div id="leftmenu">
				<ul>
					<li><a href="../property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','../common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/menu2.gif" alt="分譲実績　Results" name="leftmenu2" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','../common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','../common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
				</ul>
			</div><!--左バナー終了-->

			<div id="left_info"> 
			<!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','../common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
	  </div>
			<p><img src="../common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
		</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="property">
			
			<h3><img src="image/property_title01.jpg" width="655" height="50" alt="物件情報　Property　詳細"></h3>
		 <p class="pan"><a href="../">トップページ</a> > <a href="../property/">物件情報（一覧）</a> > 物件一覧（詳細）</p>
			
			<div class="b_title"><p class="title"><?php echo nl2br($fetch[0]['TITLE']);?></p></div>
			<?php if($fetch[0]['DETAIL_URL']){//物件を詳しく見るのURLが入力してあれば表示する?>
				<div class="l_btn"><a href="<?php echo $fetch[0]['DETAIL_URL'];?>" title="物件を詳しく見る" onMouseOver="MM_swapImage('look','','image/property_btn07_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/property_btn07.jpg" alt="物件を詳しく見る" width="134" height="22" id="look"></a></div>
			<?php }?>
			<div class="clear"></div>
			<table width="655" border="0" cellspacing="0" cellpadding="0">
				<tr>
				 <td>
				 <?php 
				 $main_img = "./up_img/".$fetch[0]['RES_ID']."_2.jpg";
				 if(file_exists($main_img)){
				 ?>
				 <img src="./up_img/<?php echo $fetch[0]['RES_ID'];?>_2.jpg" width="500" alt="<?php echo $fetch[0]['TITLE'];?>" class="photo" name="main_img">
				 <?php }?>
				 </td>
					<td>
					<?php
					$count_img = 0;  
					for($i=2;$i<=4;$i++){
					
						$img = "./up_img/".$fetch[0]['RES_ID']."_".$i.".jpg";
						if(file_exists($img)){
						$count_img .= ($count_img + 1);
					?>
					<p class="mb5"><input type="image" onClick="document.main_img.src='<?php echo $img;?>'" src="<?php echo $img;?>" width="133" border="0" class="photo2"></p>
					<?php
						}
					
					}
					?>
					<?php if($count_img > 0){?>
					<p><img src="image/property_ico02.jpg" width="136" height="31" alt="画像クリックで拡大画像が見られます。"></p>
					<?php }?>
					</td>
				</tr>
			</table>
			<?php if($fetch[0]['CONTENT']){?>
			<p class="mt15"><img src="image/property_ill01.jpg" width="655" height="5" alt=""></p>
			<div id="p_waku">
			<p><strong><?php echo nl2br($fetch[0]['CONTENT']);?></strong></p>
			</div>
			<p><img src="image/property_ill01.jpg" width="655" height="5" alt=""></p>
			<?php }?>
			<table cellpadding="0" cellspacing="1" border="0" summary="物件詳細" class="p_shosai">
				<?php if($fetch[0]['ITEM_NO']){?>
				<tr>
					<th>物件番号</th>
					<td colspan="3">No.<?php echo nl2br($fetch[0]['ITEM_NO']);?></td>
				</tr>
				<?php }?>
				<?php if($fetch[0]['HANBAIBI']){?>
				<tr>
					<th>販売日</th>
					<td colspan="3"><?php echo nl2br($fetch[0]['HANBAIBI']);?></td>
				</tr>
				<?php }?>
				<?php if($fetch[0]['PRICE']){?>
				<tr>
					<th>販売価格（税込）</th>
					<td colspan="3"><?php echo nl2br($fetch[0]['PRICE']);?></td>
				</tr>
				<?php }?>
				<?php if($fetch[0]['MEISYO']){?>
				<tr>
					<th>名称</th>
					<td colspan="3"><?php echo nl2br($fetch[0]['MEISYO']);?></td>
				</tr>
				<?php }?>
				<?php if($fetch[0]['ADDRESS']){?>
				<tr>
					<th>所在地</th>
					<td colspan="3"><?php echo nl2br($fetch[0]['ADDRESS']);?>
					<?php if($fetch[0]['URL']){?>
					<p><a href="<?php echo $fetch[0]['URL'];?>" title="Googleマップを見る" onMouseOver="MM_swapImage('google1','','image/map_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="image/map_btn01.jpg" alt="Googleマップを見る" width="163" height="25" id="google1" class="mb5"></a></p>
					<?php }?>
					</td>
				</tr>
				<?php }?>
				<?php if($fetch[0]['KOUTU']){?>
				<tr>
					<th>交通</th>
					<td colspan="3"><?php echo nl2br($fetch[0]['KOUTU']);?></td>
				</tr>
				<?php }?>
				<tr>
					<th>敷地面積</th>
					<td><?php echo ($fetch[0]['SIKICHI'])?nl2br($fetch[0]['SIKICHI']):"-";?></td>
					<th>専有面積</th>
					<td><?php echo ($fetch[0]['SENYU'])?nl2br($fetch[0]['SENYU']):"-";?></td>
				</tr>
				<tr>
					<th>販売戸数</th>
					<td><?php echo ($fetch[0]['SOTOSU'])?nl2br($fetch[0]['SOTOSU']):"-";?></td>
					<th>用途地域</th>
					<td><?php echo ($fetch[0]['YOUTO'])?nl2br($fetch[0]['YOUTO']):"-";?></td>
				</tr>
				<tr>
					<th>建ぺい率</th>
					<td><?php echo ($fetch[0]['TATE'])?nl2br($fetch[0]['TATE']):"-";?></td>
					<th>容積率</th>
					<td><?php echo ($fetch[0]['YOSEKI'])?nl2br($fetch[0]['YOSEKI']):"-";?></td>
				</tr>
				<?php if($fetch[0]['DETAIL_CONTENT']){?>
				<tr>
					<th>備考</th>
					<td colspan="3"><?php echo nl2br($fetch[0]['DETAIL_CONTENT']);?></td>
				</tr>
				<?php }?>
				
			</table>
			<?php if($fetch[0]['URL2']){?>
			<p class="btn4"><a href="<?php echo $fetch[0]['URL2'];?>" title="この物件について、もっと詳しく知りたい方はこちら" onMouseOver="MM_swapImage('detail1','','image/property_btn06_on.jpg',1)" onMouseOut="MM_swapImgRestore()"<?php echo ($fetch[0]['TARGET_FLG'])?" target=\"_blank\"":"";?>><img src="image/property_btn06.jpg" alt="この物件について、もっと詳しく知りたい方はこちら" width="388" height="28" id="detail1"></a></p>
			<?php }?>
			<p class="mt15"><img src="image/property_title06.jpg" alt="お申し込みから購入までの流れ" width="350" height="36"></p>

			<p><img src="image/property_ill02.jpg" width="655" height="148" alt="Step1お問い合わせ　気になる物件があれば、気軽にお問い合わせください。TEL：0800-919-9156　Step2物件のご案内　ご希望の物件までご一緒し、部屋の中までご案内させていただきます。　Step3お申し込み　お申し込み書にご記入いただきます。　Step4売買契約　重要事項を説明後、売買契約書にご署名・ご捺印をしていただきます。"></p>
			<p><img src="image/property_ill03.jpg" width="655" height="152" alt="Step5ローンの手続き　当社の提携先である金融機関とローン契約手続きを行っていただきます。　Step6登記・申請　登記に関する書類にご署名・ご捺印をしていただきます。　Step7お引渡し　この日よりマンション経営がスタートとなります。また、実際にお住まいになる方もご入居が可能となります。　Step8登記済権利証　登記の手続きが完了し、諸費用の清算が完了いたしましたら、登記済権利証をご送付いたします。"></p>

			<p class="mt15"><a href="../property/?p=<?php echo $p;?>" title="一覧へ戻る" onMouseOver="MM_swapImage('ichiran1','','image/property_btn05_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/property_btn05.jpg" alt="一覧へ戻る" name="ichiran1" width="105" height="25" border="0" id="ichiran1"></a></p>

			<div id="f_banner"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('f_banner1','','../common_img/header_btn03_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/header_btn03.gif" alt="お問い合わせ・資料請求" width="156" height="22" id="f_banner1"></a></div>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','../common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="../common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="../policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','../common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="../sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','../common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="../common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>
</html>
