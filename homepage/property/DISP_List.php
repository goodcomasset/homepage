<?php
/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム
View：取得したデータをHTML出力

***********************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("Location: ../");exit();
}

#-------------------------------------------------------------
# HTTPヘッダーを出力
#	１．文字コードと言語：EUCで日本語
#	２．ＪＳとＣＳＳの設定：する
#	３．有効期限：設定しない
#	４．キャッシュ拒否：設定する
#	５．ロボット拒否：設定しない
#-------------------------------------------------------------
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>物件情報（一覧）｜グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="../js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
</script>

</head>

<body onLoad="MM_preloadImages('../common_img/navi1_on.gif','../common_img/navi2_on.gif','../common_img/navi3_on.gif','../common_img/navi4_on.gif','../common_img/navi5_on.gif','../common_img/navi6_on.gif','../common_img/menu1_on.gif','../common_img/menu2_on.gif','../common_img/menu3_on.gif','../common_img/menu4_on.gif')">
<div id="wrapper">
<div id="seo"><h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1><br class="clear"></div>

	<div id="header">
		<span class="left2"><h2><img src="/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></h2></span>
		<span class="right2"><p><img src="/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
		<div id="h_gnavi2">
		<ul>
		<li class="s_btn2"><a href="/" title="ホーム" onMouseOver="MM_swapImage('header1','','/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
		<li class="s_btn2"><a href="/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
		<li class="s_btn2"><a href="/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
		<li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--ヘッダーナビ終了-->
		<p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
		</span>
		<div class="clear"></div>
	</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>
	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->
		
			<p><img src="../common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<div id="leftmenu">
				<ul>
					<li><a href="../property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','../common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/menu2.gif" alt="分譲実績　Results" name="leftmenu2" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','../common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','../common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
				</ul>
			</div><!--左バナー終了-->

			
   <div id="left_info"> 			
	  <!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','../common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
    
			</div>
            
		 <p><img src="../common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
			</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="property">
			
			<h3><img src="image/property_title05.jpg" width="655" height="50" alt="物件情報 Property 一覧"></h3>
		 <p class="pan"><a href="../">トップページ</a> > 物件情報（一覧）</p>
			<?php 
			// 登録がない場合のエラーメッセージ
			echo $disp_no_data;
			
			for($i=0;$i<count($fetch);$i++){
			
			// 画像
			$img = "./up_img/".$fetch[$i]['RES_ID']."_1.jpg"; // 本番はこうなる
			
			?>
			<p class="title"><?php echo nl2br($fetch[$i]['TITLE']);?></p>
			<p class="text"><?php echo nl2br($fetch[$i]['ADDRESS']);?>／<?php echo nl2br($fetch[$i]['KOUTU']);?></p>
			<p><img src="image/property_ill01.jpg" width="655" height="5" alt=""></p>
			<div id="p_waku">
			<table width="625" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<?php if(file_exists($img)){?>
					<td width="256"><img src="<?php echo $img;?>?r=<?php echo rand();?>" width="240" alt="<?php echo nl2br($fetch[$i]['TITLE']);?>" class="photo"></td>
					<?php }?>
					<td>
					<?php if($fetch[$i]['TITLE2']){?><p class="title2"><?php echo nl2br($fetch[$i]['TITLE2']);?></p><?php }?>
					<?php if($fetch[$i]['KYACHI']){?><p class="mt10"><strong><?php echo nl2br($fetch[$i]['KYACHI']);?></strong></p><?php }?>
					<?php if($fetch[$i]['CONTENT']){?><p class="mt10"><?php echo nl2br($fetch[$i]['CONTENT']);?></p><?php }?>
					<?php if($fetch[$i]['HANBAIBI']){?><p class="kanbai"><?php echo nl2br($fetch[$i]['HANBAIBI']);?></p><?php }?>
					<p class="btn"><a href="./?id=<?php echo $fetch[$i]['RES_ID'];?>&p=<?php echo $p;?>" title="物件を詳しく見る" onMouseOver="MM_swapImage('property<?php echo $fetch[$i]['RES_ID'];?>','','image/property_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/property_btn01.jpg" alt="物件を詳しく見る" width="138" height="25" id="property<?php echo $fetch[$i]['RES_ID'];?>"></a></p>
					</td>
				</tr>
			</table>
			</div>
			<p class="mb15"><img src="image/property_ill01.jpg" width="655" height="5" alt=""></p>
			<?php }?>
			<?php
			#--------------------------------------------------------
			# ページング用リンク文字列処理
			#--------------------------------------------------------
			
				// 次ページ番号
				$next = $p + 1;
				// 前ページ番号
				$prev = $p - 1;
				
				// 商品全件数
				$tcnt = count($fetchCNT);
				// 全ページ数
				$totalpage = ceil($tcnt/S6_1DISP_MAXROW);
			
				// カテゴリー別で表示していればページ遷移もカテゴリーパラメーターをつける
				if($ca)$cpram = "&ca=".urlencode($ca);
			?>
			<table width="655" border="0" cellspacing="0" cellpadding="0" class="list">
				<tr>
					<td width="590" align="left"><?php
// 前ページへのリンク
if($p > 1){?><a href="<?php echo $_SERVER['PHP_SELF']."?p=".urlencode($prev).$cpram;?>" title="BACK" onMouseOver="MM_swapImage('back1','','image/back_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/back.jpg" alt="BACK" width="65" height="17" id="back1"></a><?php }?></td>
				 <td width="65" align="right"><?php 
//次ページリンク
if($totalpage > $p){
?><a href="<?php echo $_SERVER['PHP_SELF']."?p=".urlencode($next).$cpram;?>" title="NEXT" onMouseOver="MM_swapImage('next1','','image/next_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/next.jpg" alt="NEXT" width="65" height="17" id="next1"></a><?php }?></td>
				</tr>
			</table>

			<p class="btn2"><a href="../property/?ca=2" title="分譲実績一覧" onMouseOver="MM_swapImage('jisseki1','','image/property_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/property_btn02.jpg" alt="分譲実績一覧" width="269" height="28" id="jisseki1"></a></p>

			<div id="f_banner"><a href="https://ssl.goodcomasset.co.jp/contact" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('f_banner1','','../common_img/header_btn03_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/header_btn03.gif" alt="お問い合わせ・資料請求" width="156" height="22" id="f_banner1"></a></div>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','../common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="../common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="../policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','../common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="../sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','../common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="../common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
