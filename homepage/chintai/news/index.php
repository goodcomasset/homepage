<?php
/*******************************************************************************
SiteWiN20 20 30（MySQL版）N3_2
新着情報の内容をFlashに出力するプログラム

コントローラー


*******************************************************************************/


	// 不正アクセスチェックのフラグ
	$injustice_access_chk = 1;
	
// 設定ファイル＆共通ライブラリの読み込み
require_once("../common/config.php");		// 共通設定情報
require_once("../common/config_N3_2.php");		// 共通設定情報
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');				// DB操作クラスライブラリ
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');			// 汎用処理クラスライブラリ

// 実行プログラム読み込み
include("LGC_getDB-list.php");	// DB情報取得
include("DSP_contents.php");	// 取得した情報を表示

?>