<?php
/*******************************************************************************
SiteWin10 20 30（MySQL版）
共通設定情報ファイルの定義	


*******************************************************************************/
/*
error_reporting (E_ALL);
//error_reporting (E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
error_reporting (E_ERROR | E_WARNING | E_PARSE);
ini_set('track_errors',1);
ini_set('display_errors',1);
*/
#=================================================================================
# マルチバイト関数用に言語と文字コードを指定する（文字列置換、メール送信等で必須）
#=================================================================================
mb_language("Japanese");
mb_internal_encoding("EUC-JP");

//エラーメッセージの表示の時エンコードがsjisで文字化けをするのを回避
	//header("Content-Type: text/html; charset=EUC-JP");
	//header("Content-Language: ja");

#=================================================================================
#  本番アップ後の処理 ※本番アップする際に以下の記述をする事！
#=================================================================================
// demoURLへのアクセス 0：アクセス許可（制作時は“0”） ／ 1：許可しない（本番アップ後は“1”にする）
$siteopne_flg = 0;

// 本番のURL
$domain = "http://www.zeek.jp/back_office/";

#=================================================================================
#  RSSのパス設定
#=================================================================================
//rss.phpファイルの設置する階層をあわせる（デフォルトはトップ階層を指定）
define('SITE_LINK','http://'.$_SERVER["HTTP_HOST"].'/chintai');	// サイトURL(httpからの絶対パス)

#=================================================================================
# ＤＢ接続の情報（定数化）
#=================================================================================
/*
define('DB_SERVER','localhost');
define('DB_NAME','goodcomdemo');
define('DB_USER','goodcomdemo');
define('DB_PASS','0LG3aF6Z');
define('DSN',"mysql://sdg_sample:893SmpL7@localhost/sdg_sample");	// PEAR用
*/

define('DB_SERVER','mysql01.in.shared-server.net');
//define('DB_SERVER','localhost');
define('DB_NAME','CTrN497');
define('DB_USER','CTrN497');
define('DB_PASS','Goodcom9156');
define('DSN',"mysql://CTrN497:Goodcom9156@localhost/CTrN497");	// PEAR用

#=================================================================================
# 管理画面のIDとパスワード
#=================================================================================
define('BO_ID','test');		# ID
define('BO_PW','pass');		# PW

#=================================================================================
# 頻繁に行う簡易処理のファンクション化（匿名関数）
#=================================================================================
// UNIX時間＋マイクロ秒によるID生成
$makeID = create_function('','return date("U")."-".sprintf("%06d",(microtime() * 1000000));');	

// パスワード作成
$makePass = create_function('','$pass = crypt(mt_rand(0,99999999),"CP");return ereg_replace("[^a-zA-Z0-9]","",$pass);');

#=================================================================================
# “htmlspecialchars()”でエンティティ化したHTML特殊文字を
#	Flashで正常表示できるように全角に変換するファンクション
#=================================================================================
function h14s_han2zen(&$str){

	$str = str_replace("&amp;","＆",$str);
	$str = str_replace("&quot;","”",$str);
	$str = str_replace("&lt;","＜",$str);
	$str = str_replace("&gt;","＞",$str);
	$str = str_replace("&#39","’",$str);
	$str = str_replace("'","’",$str);
	$str = str_replace("&","＆",$str);
	$str = str_replace("%","%25",$str);
	$str = str_replace("+","%2b",$str);

	return $str;
}

#=================================================================================
# 管理情報テーブルよりメールアドレスを取得するファンクション
#	メソッド名：getInitData("カラム名")
#=================================================================================
function getInitData($colum = ""){

	if(!$colum)$colum = "EMAIL1";

	$sql = "SELECT {$colum} FROM C_APP_INIT_DATA WHERE(RES_ID = '1')";
	$con = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
	mysql_select_db(DB_NAME,$con);

	if($query = mysql_query($sql)):

		$fetch = mysql_result($query,0,$colum);
		mysql_free_result($query);
		mysql_close($con);

		return $fetch;

	endif;

}

#=================================================================================
# 転送処理を行うファンクション（引数：対象URL、戻り値：なし）
#=================================================================================
$layer_free = "Layer_free";//カラーパレットに使用するレイヤー名

function location($url){

echo '<html><head><title></title></head>',
'<body onLoad="Javascript:document.location.submit();">',
'<form name="location" action="'.$url.'" method="post">',
'</form></body></html>',
'<noscript>header("Location: '.$url.'");</noscript>';
exit();
}

#=================================================================================
# ＨＴＭＬタグの有効処理
#
# 【back_office/n○whatsnew/LGC_regist.php】でＨＴＭＬタグの有効処理の上に
# addslashesの処理を行っておりますが、こちらの処理でもaddslashesをおこなっております。
# 二重に処理を行わないよう注意をしてください。
# （二重に処理をしますとaddslashesが無効になります。）
#=================================================================================
function html_tag($str){

	$str  = mb_convert_kana($str,"KV");//半角を全角に変換処理
	$str = strip_tags($str,"<a><b><span><i><u>");
	$str = utilLib::strRep($str ,7); // 前後の空白除去
	$str  = utilLib::strRep($str ,4); // stripslashes
	$str  = utilLib::strRep($str ,5); // addslashes

	return $str;
}
$layer_free = "Layer_free";//カラーボタンの付近にレイヤーを表示するレイヤー用

?>