<?php
/*******************************************************************************
SiteWiN 10 20 30（MySQL版）
設定ファイルの定義	


*******************************************************************************/

// 設定ファイル＆共通ライブラリの読み込み
require_once("config.php");	// 共通設定情報

#=================================================================================
# 管理画面に共通して表示させる値
#=================================================================================
define('TITLE','物件情報の更新');	// Sx系（商品紹介）のタイトル
define('CATE_TITLE','物件情報カテゴリーの更新');	// Sx系（商品紹介）カテゴリーのタイトル

#=================================================================================
# 最大登録件数の指定
# カテゴリーは無制限で登録が可能
#=================================================================================
define('DBMAX_CNT',100);	// Sx系（商品紹介）の最大登録件数

#=================================================================================
# 最大表示件数の指定
#=================================================================================
define('DISP_MAXROW',10);	// Sx系（商品紹介）の 1ページの最大表示件数

// 1行の列数※クロス表示でない場合は１を設定
define('LINE_MAXCOL',1);

#=================================================================================
# データベースでのテーブル名の指定
#=================================================================================
define('S7_PRODUCT_LST','C_PRODUCT_LST');	//商品データ
define('S7_CATEGORY_MST','C_CATEGORY_MST');	//カテゴリー
define('ROOM_MST','C_ROOM_LST');//部屋
 
#=================================================================================
# 画像情報の指定
#=================================================================================
// 画像ファイルパス（管理画面のみで使用）
define('IMG_PATH','../../property/up_img/');
//部屋用画像
define('R_IMG_PATH','../../property/up_room_img/');

// 画像枚数
define('IMG_CNT',1);

// 画像ファイルサイズ
define('IMGSIZE_SX',40);	// 管理画面サムネイル用
define('IMGSIZE_SY',30);	// 管理画面サムネイル用
define('IMGSIZE_MX1',245);	// アップロード画像幅（商品紹介／高自動算出）
define('IMGSIZE_MY1',100);	// アップロード画像高（商品紹介／幅自動算出）
define('IMGSIZE_MX2',200);	// アップロード画像幅（商品紹介／高自動算出）
define('IMGSIZE_MY2',200);	// アップロード画像高（商品紹介／幅自動算出）

//部屋用画像サイズ
define('IMGSIZE_R_MX',207);
define('IMGSIZE_R_LX',527);
define('IMGSIZE_R_BX',101);

// 画像枚数
define('R_IMG_CNT',7);

define('R_DBMAX_CNT',100);

	//定数を配列に格納しておく（back_office/s7_product/LGC_regist.phpで使用）
	$ox = array(IMGSIZE_R_MX,IMGSIZE_R_LX,IMGSIZE_R_LX,IMGSIZE_R_LX,IMGSIZE_R_LX,IMGSIZE_R_LX,IMGSIZE_R_LX);
	$oy = array(IMGSIZE_MY1,IMGSIZE_MY2);


define('PDF_PATH','../../property/up_pdf/');
define('LIMIT_SIZE',3);

$per_array = array(
	array(
		'maparea'=>'26,150,92,182',
		'category_code'=>'26',
		'image_name'=>'bg_area_r1.jpg'
	),
	array(
		'maparea'=>'311,171,354,193',
		'category_code'=>'4',
		'image_name'=>'bg_area_r24.jpg'
	),
	array(
		'maparea'=>'296,151,341,171',
		'category_code'=>'3',
		'image_name'=>'bg_area_r19.jpg'
	),
	array(
		'maparea'=>'355,162,398,184',
		'category_code'=>'10',
		'image_name'=>'bg_area_r20.jpg'
	),
	array(
		'maparea'=>'358,124,401,146',
		'category_code'=>'9',
		'image_name'=>'bg_area_r8.jpg'
	),
	array(
		'maparea'=>'322,122,365,144',
		'category_code'=>'8',
		'image_name'=>'bg_area_r10.jpg'
	),
	array(
		'maparea'=>'284,123,327,145',
		'category_code'=>'7',
		'image_name'=>'bg_area_r11.jpg'
	),
	array(
		'maparea'=>'320,92,377,115',
		'category_code'=>'20',
		'image_name'=>'bg_area_r9.jpg'
	),
	array(
		'maparea'=>'408,144,465,167',
		'category_code'=>'25',
		'image_name'=>'bg_area_r7.jpg'
	),
	array(
		'maparea'=>'385,80,439,108',
		'category_code'=>'24',
		'image_name'=>'bg_area_r6.jpg'
	),
	array(
		'maparea'=>'334,49,385,76',
		'category_code'=>'23',
		'image_name'=>'bg_area_r5.jpg'
	),
	array(
		'maparea'=>'269,72,312,95',
		'category_code'=>'19',
		'image_name'=>'bg_area_r4.jpg'
	),
	array(
		'maparea'=>'252,106,295,129',
		'category_code'=>'18',
		'image_name'=>'bg_area_r12.jpg'
	),
	array(
		'maparea'=>'219,66,265,90',
		'category_code'=>'21',
		'image_name'=>'bg_area_r3.jpg'
	),
	array(
		'maparea'=>'159,85,212,111',
		'category_code'=>'22',
		'image_name'=>'bg_area_r2.jpg'
	),
	array(
		'maparea'=>'218,129,262,155',
		'category_code'=>'16',
		'image_name'=>'bg_area_r13.jpg'
	),
	array(
		'maparea'=>'253,141,296,164',
		'category_code'=>'6',
		'image_name'=>'bg_area_r18.jpg'
	),
	array(
		'maparea'=>'236,174,282,199',
		'category_code'=>'15',
		'image_name'=>'bg_area_r17.jpg'
	),
	array(
		'maparea'=>'282,195,325,218',
		'category_code'=>'5',
		'image_name'=>'bg_area_r21.jpg'
	),
	array(
		'maparea'=>'229,224,281,246',
		'category_code'=>'12',
		'image_name'=>'bg_area_r16.jpg'
	),
	array(
		'maparea'=>'266,245,325,271',
		'category_code'=>'11',
		'image_name'=>'bg_area_r22.jpg'
	),
	array(
		'maparea'=>'250,284,316,316',
		'category_code'=>'13',
		'image_name'=>'bg_area_r23.jpg'
	),
	array(
		'maparea'=>'120,292,186,324',
		'category_code'=>'27',
		'image_name'=>'bg_area_r25.jpg'
	),
	array(
		'maparea'=>'493,64,559,96',
		'category_code'=>'27',
		'image_name'=>'bg_area_r25.jpg'
	),
	array(
		'maparea'=>'165,202,231,234',
		'category_code'=>'14',
		'image_name'=>'bg_area_r15.jpg'
	),
	array(
		'maparea'=>'170,139,219,168',
		'category_code'=>'17',
		'image_name'=>'bg_area_r14.jpg'
	)
);
?>