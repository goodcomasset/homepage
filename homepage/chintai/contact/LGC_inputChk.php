<?php
/************************************************************************
 お問い合わせフォーム（POST渡しバージョン）
 処理ロジック：エラーチェック
	※POST送信されたデータに対して不備が無いかチェックする


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// POSTデータの受け取りと共通な文字列処理
extract(utilLib::getRequestParams("post",array(8,7,1,4),true));

// 全角英数字→半角英数字に変換
$zip = mb_convert_kana($zip,"a");
$tel = mb_convert_kana($tel,"a");
$fax = mb_convert_kana($fax,"a");

$email = mb_convert_kana($email,"a");//スタイルのime-modeはIEのみ有効ですので、FireFoxで全角で入力される可能性があるため処理をします。

// フリガナは全角カタカナに統一"C"で全角カタカナ"c"で全角ひらがな
$kana = mb_convert_kana($kana,"C");

//メールアドレスの空白を除去する
$email = ereg_replace("[[:space:]]","",$email);//半角の空白を削除
$email = mb_ereg_replace("(　)","",$email);//全角の空白を削除

#----------------------------------------------------------------------------------
# エラーチェック	※strCheck(対象文字列,モード,偉ーメッセージ);を使用。	
#	0:	未入力チェック
#	1:	空白文字チェック
#	4:	最後の文字に不正な文字が使われているか
#	5:	不正かつ危険な文字が使われているか
#	6:	メールアドレスチェック（E-Mailのみ）
#----------------------------------------------------------------------------------
$error_mes .= utilLib::strCheck($type,0,"お問い合わせ項目をご選択ください。<br>\n");
$error_mes .= utilLib::strCheck($name,0,"お名前をご記入ください。<br>\n");

$error_mes .= utilLib::strCheck($zip,0,"郵便番号をご記入ください。<br>\n");
if(!empty($zip)){
	if(ereg("[^-0-9]",$zip)){
		$error_mes .= "郵便番号は半角数字とハイフンのみでご記入してください。<br>\n";
	}
}

$error_mes .= utilLib::strCheck($state,0,"都道府県をご選択ください。<br>\n");
$error_mes .= utilLib::strCheck($address,0,"ご住所をご記入ください。<br>\n");

$error_mes .= utilLib::strCheck($tel,0,"電話番号をご記入ください。<br>\n");
if(!empty($tel)){
	if(ereg("[^-0-9]",$tel)){
		$error_mes .= "電話番号は半角数字とハイフンのみでご記入してください。<br>\n";
	}
}

$error_mes .= utilLib::strCheck($email,0,"メールアドレスをご記入ください。<br>\n");
if($email){
	$mailchk = "";
	$mailchk .= utilLib::strCheck($email,1,true);
	$mailchk .= utilLib::strCheck($email,4,true);
	$mailchk .= utilLib::strCheck($email,5,true);
	$mailchk .= utilLib::strCheck($email,6,true);
	
	//メールアドレスに全角文字と半角カタカナの入力は拒否させる
	if((mb_strlen($email, 'EUC-JP') != strlen($email)) || mb_ereg("[ｱ-ﾝ]", $email)){
		$mailchk .= "メールアドレスに不正な文字が含まれております。";
	}
	
	if($mailchk)$error_mes .= "メールアドレスの形式に誤りがあります<br><br>\n";
}

$error_mes .= utilLib::strCheck($agree,0,"個人情報の取り扱いについて同意してください。<br>\n");

?>
