<?php
/************************************************************************
  お問い合わせフォーム（POST渡しバージョン）
 View：入力画面	※デフォルトで表示する画面


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// HTTPヘッダー
utilLib::httpHeadersPrint("EUC-JP",true,false,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。都心に通いやすく、住みやすい物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,賃貸マンション,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title>グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="https://ssl.goodcomasset.co.jp/chintai/css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="https://ssl.goodcomasset.co.jp/chintai/js/dw_common.js" type="text/javascript"></script>
<script src="https://ssl.goodcomasset.co.jp/chintai/js/JUDGE.JS" type="text/javascript"></script>
<script src="https://ssl.goodcomasset.co.jp/chintai/js/ie7_flash.js" type="text/javascript"></script>
<script type="text/javascript" src="common.js"></script>
</head>

<body class="contact" id="pTop" onLoad="MM_preloadImages('https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04_r.gif','https://ssl.goodcomasset.co.jp/chintai/contact/images/bnr_list_r.jpg','https://ssl.goodcomasset.co.jp/chintai/contact/images/bnr_corporate_r.jpg','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区を中心とした賃貸マンションのお部屋探しなら株式会社グッドコムまで。</h1>
<div id="header">
<p id="logo"><a href="http://www.goodcomasset.co.jp/chintai/"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="http://www.goodcomasset.co.jp/chintai/"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="http://www.goodcomasset.co.jp/chintai/sitemap"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="http://www.goodcomasset.co.jp/chintai/news"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<!--<h3><img src="https://ssl.goodcomasset.co.jp/chintai/images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="http://www.goodcomasset.co.jp/chintai/property/" method="get">
<h4><img src="https://ssl.goodcomasset.co.jp/chintai/images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="https://ssl.goodcomasset.co.jp/chintai/images/btn_search.gif" onmouseover="this.src='https://ssl.goodcomasset.co.jp/chintai/images/btn_search_r.gif'" onmouseout="this.src='https://ssl.goodcomasset.co.jp/chintai/images/btn_search.gif'">
</form>
-->
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="http://www.goodcomasset.co.jp/chintai/qa/" onMouseOver="MM_swapImage('id_useful_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="http://www.goodcomasset.co.jp/chintai/flow/" onMouseOver="MM_swapImage('id_useful_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="http://www.goodcomasset.co.jp/chintai/trouble/" onMouseOver="MM_swapImage('id_useful_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="https://ssl.goodcomasset.co.jp/chintai/up_img/1264486822-728359.pdf" target="_blank"><img src="https://ssl.goodcomasset.co.jp/chintai/images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','https://ssl.goodcomasset.co.jp/chintai/images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/sld.gif" alt="お問い合わせ・資料請求"></h2>

<p id="pan"><a href="http://www.goodcomasset.co.jp/chintai/">トップページ</a> > お問い合わせ・資料請求</p>
<p>ご意見、ご感想、ご相談はお気軽にお寄せください。<br>
お客様の貴重なご意見・ご要望は、今後のサービス改善に役立たせていただきます。</p>
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/title_02.gif" alt="お電話でのお問い合わせ・資料請求" width="645" height="33"></h3>
<p class="photo"><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/contact_banner01.jpg" alt="お問い合わせ時間：平日10時〜20時　TEL 03-5338-0160" width="645" height="103"></p>
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/title_01.gif" alt="フォームでのお問い合わせ・資料請求"></h3>
<table width="95%" align="center" cellpadding="8" cellspacing="1" class="listTable">
<tr>
<td colspan="2">
<?php //↓ここから↓ ?>

<br>
<?php
//エラーが発生していない場合
if(!$err_mes){
?>
<p align=center style="line-height:160%;text-align:center;">
正常に送信されました。<br>
後日メールまたはお電話にて担当よりご連絡させていただきます。<br>
また、ご返信に時間がかかる場合がございますのでご了承ください。</p>
			<?php
}else{
//エラーが発生している場合エラー内容を表示する
echo "<br><p style=\"color:#FF0000;\">".$err_mes."</p>";
}?>
<br><br>
<p align=center><a href="../">トップページへ</a></p>


<?php //↑ここまで↑ ?></td>
</tr>
</table>


<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>


<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
	<li class="nav_01">
		<a href="http://www.goodcomasset.co.jp/chintai/company">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
	<li class="nav_02">
		<a href="http://www.goodcomasset.co.jp/chintai/privacy">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
	<li class="nav_03">
		<a href="http://www.goodcomasset.co.jp/chintai/sitemap">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">

<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>
こちらからダウンロードしてください。</a></p>
</div>
<div class="clear"></div>
<!--　footer end　--></div>




</body>
</html>
