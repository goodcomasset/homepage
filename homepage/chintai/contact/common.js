// JavaScript Document

/////////////////////////////////////
// 汎用確認メッセージ
/////////////////////////////////////
function ConfirmMsg(msg){
	return (confirm(msg))?true:false;
}

/////////////////////////////////////////////////////////////////////////////////
// 未入力及び不正入力のチェック（※Safariのバグ（エスケープ文字認識）を回避）
/////////////////////////////////////////////////////////////////////////////////
function inputChk(f,confirm_flg){

	// フラグの初期化
	var flg = false;
	var error_mes = "Error Message\r\n恐れ入りますが、下記の内容をご確認ください\r\n\r\n";

	// 未入力と不正入力のチェック
	if(!f.type.value){
		error_mes += "・お問い合わせ項目をご選択ください。\r\n";flg = true;
	}
	
	if(!f.name.value){
		error_mes += "・お名前をご記入ください。\r\n";flg = true;
	}
	if(!f.zip.value){
		error_mes += "・郵便番号をご記入ください。\r\n";flg = true;
	}
	if(!f.state.value){
		error_mes += "・都道府県をご選択ください。\r\n";flg = true;
	}
	if(!f.address.value){
		error_mes += "・ご住所をご記入ください。\r\n";flg = true;
	}
	if(!f.tel.value){
		error_mes += "・電話番号をご記入ください。\r\n";flg = true;
	}
	
	if(!f.email.value){
		error_mes += "・メールアドレスをご記入ください。\r\n";flg = true;
	}
	else if(!f.email.value.match(/^[^@]+@[^.]+\..+/)){
		error_mes += "・メールアドレスの形式に誤りがあります。\r\n";flg = true;
	}

	
	// 判定
	if(flg){
		// アラート表示して再入力を警告
		window.alert(error_mes);return false;
	}
	else{

		// 確認メッセージ
		if(confirm_flg){
			return ConfirmMsg('ご入力いただいた内容で送信します。\nよろしいですか？');
		}
		return true;
	}


}

