<?php
/************************************************************************
  お問い合わせフォーム（POST渡しバージョン）
 View：入力画面	※デフォルトで表示する画面


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// HTTPヘッダー
utilLib::httpHeadersPrint("EUC-JP",true,false,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。都心に通いやすく、住みやすい物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,賃貸マンション,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title>グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="https://ssl.goodcomasset.co.jp/chintai/css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="https://ssl.goodcomasset.co.jp/chintai/js/dw_common.js" type="text/javascript"></script>
<script src="https://ssl.goodcomasset.co.jp/chintai/js/JUDGE.JS" type="text/javascript"></script>
<script src="https://ssl.goodcomasset.co.jp/chintai/js/ie7_flash.js" type="text/javascript"></script>
<script type="text/javascript" src="common.js"></script>
</head>

<body class="contact" id="pTop" onLoad="MM_preloadImages('https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04_r.gif','https://ssl.goodcomasset.co.jp/chintai/contact/images/bnr_list_r.jpg','https://ssl.goodcomasset.co.jp/chintai/contact/images/bnr_corporate_r.jpg','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区を中心とした賃貸マンションのお部屋探しなら株式会社グッドコムまで。</h1>
<div id="header">
<p id="logo"><a href="http://www.goodcomasset.co.jp/chintai/"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="http://www.goodcomasset.co.jp/chintai/"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="http://www.goodcomasset.co.jp/chintai/sitemap"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="http://www.goodcomasset.co.jp/chintai/news"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<!--<h3><img src="https://ssl.goodcomasset.co.jp/chintai/images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="http://www.goodcomasset.co.jp/chintai/property/" method="get">
<h4><img src="https://ssl.goodcomasset.co.jp/chintai/images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="https://ssl.goodcomasset.co.jp/chintai/images/btn_search.gif" onmouseover="this.src='https://ssl.goodcomasset.co.jp/chintai/images/btn_search_r.gif'" onmouseout="this.src='https://ssl.goodcomasset.co.jp/chintai/images/btn_search.gif'">
</form>
-->
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="http://www.goodcomasset.co.jp/chintai/qa/" onMouseOver="MM_swapImage('id_useful_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="http://www.goodcomasset.co.jp/chintai/flow/" onMouseOver="MM_swapImage('id_useful_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="http://www.goodcomasset.co.jp/chintai/trouble/" onMouseOver="MM_swapImage('id_useful_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="https://ssl.goodcomasset.co.jp/chintai/up_img/1264486822-728359.pdf" target="_blank"><img src="https://ssl.goodcomasset.co.jp/chintai/images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','https://ssl.goodcomasset.co.jp/chintai/images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/sld.gif" alt="お問い合わせ・資料請求"></h2>

<p id="pan"><a href="http://www.goodcomasset.co.jp/chintai/">トップページ</a> > お問い合わせ・資料請求</p>
<p>ご意見、ご感想、ご相談はお気軽にお寄せください。<br>
お客様の貴重なご意見・ご要望は、今後のサービス改善に役立たせていただきます。</p>
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/title_02.gif" alt="お電話でのお問い合わせ・資料請求" width="645" height="33"></h3>
<p class="photo"><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/contact_banner01.jpg" alt="お問い合わせ時間：平日10時〜20時　TEL 03-5338-0160" width="645" height="103"></p>
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/title_01.gif" alt="フォームでのお問い合わせ・資料請求"></h3>
<p> 
下記の内容で承ります。<br>
ご入力内容に誤りがないか再度ご確認ください。<br>
万が一誤りがあった場合は、「前に戻り修正します」ボタンを押して<br>
入力画面に戻り修正してください。<br>
間違いがなければ「上記の内容で送信します」ボタンを押してください。<br>
後日、担当者よりご連絡いたします。
</p>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" name="form1" class="style5" onSubmit="return inputChk(this,true)">
<table width="95%" align="center" cellpadding="8" cellspacing="1" class="listTable">
<tr>
<th width="26%"><div><span class="attention">※</span>
<label for="issue01">お問い合わせ項目</label>
</div></th>
<td width="74%">
<?php echo ($type)?$type:"&nbsp;";?><input name="type" type="hidden" value="<?php echo $type;?>">
</td>
</tr>
<!--
<tr>
<th><div><span class="attention">※</span>
<label for="issue02a">お問い合わせ項目</label>
</div></th>
<td><ul>
<li>
<input type="radio" name="issue02" id="issue02a" value="Aについて">
<label for="issue02a">Aについて</label>
</li>
<li>
<input type="radio" name="issue02" id="issue02b" value="Bについて">
<label for="issue02b">Bについて</label>
</li>
<li>
<input type="radio" name="issue02" id="issue02c" value="Cについて">
<label for="issue02c">Cについて</label>
</li>
</ul>
<ul>
<li>
<input type="radio" name="issue02" id="issue02d" value="その他お問い合わせ">
<label for="issue02d">その他</label>
<input name="issue02d_text" type="text" id="issue_text" value="" class="ime_on w50per">
</li>
</ul></td>
</tr>-->
<tr>
<th><label for="company">会社名</label></th>
<td>
<?php echo ($company)?$company:"&nbsp;";?><input name="company" type="hidden" value="<?php echo $company;?>">
</td>
</tr>
<tr>
<th><label for="post">部署・役職名</label></th>
<td>
<?php echo ($post)?$post:"&nbsp;";?><input name="post" type="hidden" value="<?php echo $post;?>">
</td>
</tr>
<tr>
<th><div><span class="attention">※</span>
<label for="name">お名前</label>
</div></th>
<td>
<?php echo ($name)?$name:"&nbsp;";?><input name="name" type="hidden" value="<?php echo $name;?>">
</td>
</tr>
<tr>
<th><label for="ruby">フリガナ</label></th>
<td>
<?php echo ($kana)?$kana:"&nbsp;";?><input name="kana" type="hidden" value="<?php echo $kana;?>">
</td>
</tr>
<tr>
<th><label for="male">性別</label></th>
<td>
<?php echo ($sex)?$sex:"&nbsp;";?><input name="sex" type="hidden" value="<?php echo $sex;?>">
</td>
</tr>
<tr>
<th><label for="age">年齢</label></th>
<td><?php echo ($age)?$age.'歳':"&nbsp;";?><input name="age" type="hidden" value="<?php echo $age;?>">
</td>
</tr>
<tr>
<th><div><span class="attention">※</span><label for="zip">住所</label></div></th>
<td>
<?php echo ($zip)?'〒'.$zip:"&nbsp;";?><input name="zip" type="hidden" value="<?php echo $zip;?>">
<?php echo ($state)?'<br>'.$state:"&nbsp;";?><input name="state" type="hidden" value="<?php echo $state?>">
<?php echo ($address)?'<br>'.$address:"&nbsp;";?><input name="address" type="hidden" value="<?php echo $address;?>">
</td>
</tr>
<tr>
<th><div><span class="attention">※</span><label for="tel">電話番号</label></div></th>
<td>
<?php echo ($tel)?$tel:"&nbsp;";?><input name="tel" type="hidden" value="<?php echo $tel;?>">
</td>
</tr>
<tr>
<th><label for="fax">ファックス番号</label></th>
<td>
<?php echo ($fax)?$fax:"&nbsp;";?><input name="fax" type="hidden" value="<?php echo $fax;?>">
</td>
</tr>
<tr>
<th><div><span class="attention">※</span>メールアドレス</div></th>
<td>
<?php echo ($email)?$email:"&nbsp;";?><input name="email" type="hidden" value="<?php echo $email;?>">
</td>
</tr>
<tr>
<th><label for="comment">お問い合わせ内容</label></th>
<td>
<?php echo ($comment)?nl2br($comment):"&nbsp;";?><input type="hidden" name="comment" value="<?php echo $comment;?>">
</td>
</tr>
<tr>
<th><label for="agree">個人情報の取り扱いへの同意</label></th>
<td>
<?php echo ($agree)?$agree:"&nbsp;";?><input name="agree" type="hidden" value="<?php echo $agree;?>">
</td>
</tr>
</table>
<div id="formEnd">
<input type="button" value="&lt;&lt;&nbsp;前に戻り修正します" onClick="javascript:history.back();">
<input name="Submit" type="submit" value="上記の内容で送信します&nbsp;&gt;&gt;">
<input type="hidden" name="action" value="completion">
</div>
</form>


<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>


<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
	<li class="nav_01">
		<a href="http://www.goodcomasset.co.jp/chintai/company">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
	<li class="nav_02">
		<a href="http://www.goodcomasset.co.jp/chintai/privacy">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
	<li class="nav_03">
		<a href="http://www.goodcomasset.co.jp/chintai/sitemap">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">

<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>
こちらからダウンロードしてください。</a></p>
</div>
<div class="clear"></div>
<!--　footer end　--></div>




</body>
</html>
