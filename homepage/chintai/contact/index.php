<?php
/*************************************************************************************
 お問い合わせフォーム（POST渡しバージョン）
 Main Controller：全体の処理の制御・イニシャライズ
 
 	★基本的仕様
	
		１．新規入力（送信先action:confirm）
			↓
		２．エラーチェック
				・未入力または不備がある → エラーページを表示して終了
				・問題なし → 確認画面表示へ
			↓
		３．確認画面表示（送信先action:completion）
				・修正 → 戻るボタンで前のページへ戻る（JavaScriptのhistoryを使用）
				・ＯＫ → 完了画面表示へ
			↓
		４．メール送信の処理を行い、完了画面表示して終了


*************************************************************************************/

// 設定ファイル＆共通ライブラリの読み込み
require_once("../common/config.php");	// 設定情報
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');			// 汎用処理クラスライブラリ

// エラーメッセージと不正アクセスフラグ（index.php以外からのコールを防~）の初期化
$error_mes = "";
$accessChk = true;

#--------------------------------------------------------------
# 全体のコントロール	※キーはhiddenで送っている$status
#--------------------------------------------------------------
switch($_POST["action"]):

case "completion":
/////////////////////////////////////////////////////////////////////////////
//　メール送信処理と完了画面を表示

	include("LGC_inputChk.php");		

	if(!$error_mes){
		include("LGC_sendmail.php");
		include("DSP_completion.php");
	}
	else{
	
		utilLib::errorDisp("<p>不正な処理が行われました。<br>お手数をおかけして誠に申し訳ございませんが、送信内容をご確認の上、もう一度送信しなおしてください。</p>");
	}

	break;
case "confirm":
/////////////////////////////////////////////////////////////////////////////
// エラーがあれば再入力、OKなら確認画面表示（送信先：completion）

	include("LGC_inputChk.php");
		
	if($error_mes):
		//utilLib::errorDisp($error_mes);
		include("DSP_error.php");
	else:
		include("DSP_confirm.php");
	endif;

	break;
default:
/////////////////////////////////////////////////////////////////////////////
// 新規入力画面を表示（送信先：confirm）

	include("DSP_input.php");

endswitch;
?>





<?php
/*** 入力画面で使用するソース
<script type="text/javascript" src="common.js"></script>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" onSubmit="return inputChk(this,false)">
<input name="Submit" type="submit" value="確認画面へ&nbsp;&gt;&gt;">                      
<input type="hidden" name="action" value="confirm">
</form>
***/?>

<?php
/*** 確認画面で使用するソース
<script type="text/javascript" src="common.js"></script>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" onSubmit="return inputChk(this,true)">
<input name="Submit" type="submit" value="上記の内容で送信します&nbsp;&gt;&gt;">
<input type="hidden" name="action" value="completion">
<input type="button" value="&lt;&lt;&nbsp;前に戻り修正します" onClick="javascript:history.back();">
</form> 
***/
?>

<?php
/*** 完了画面で使用するソース
<meta http-equiv="refresh" content="3;URL=../">
<p align=center style="line-height:160%;height:200px;">
正常に送信されました。<br>
後日メールまたはお電話にて担当よりご連絡させていただきます。<br>
また、ご返信に時間がかかる場合がございますのでご了承ください。</p>

<p align=center><a href="../">トップページへ</a></p>
***/?>
