<?php
/************************************************************************
  お問い合わせフォーム（POST渡しバージョン）
 View：入力画面	※デフォルトで表示する画面


************************************************************************/

// 不正アクセスチェック
if(!$accessChk){
	header("HTTP/1.0 404 Not Found");exit();
}

// HTTPヘッダー
utilLib::httpHeadersPrint("EUC-JP",true,false,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。都心に通いやすく、住みやすい物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,賃貸マンション,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title>グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="https://ssl.goodcomasset.co.jp/chintai/css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="https://ssl.goodcomasset.co.jp/chintai/js/dw_common.js" type="text/javascript"></script>
<script src="https://ssl.goodcomasset.co.jp/chintai/js/JUDGE.JS" type="text/javascript"></script>
<script src="https://ssl.goodcomasset.co.jp/chintai/js/ie7_flash.js" type="text/javascript"></script>
<script type="text/javascript" src="common.js"></script>
</head>

<body class="contact" id="pTop" onLoad="MM_preloadImages('https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04_r.gif','https://ssl.goodcomasset.co.jp/chintai/contact/images/bnr_list_r.jpg','https://ssl.goodcomasset.co.jp/chintai/contact/images/bnr_corporate_r.jpg','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02_r.gif','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区を中心とした賃貸マンションのお部屋探しなら株式会社グッドコムまで。</h1>
<div id="header">
<p id="logo"><a href="http://www.goodcomasset.co.jp/chintai/"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="http://www.goodcomasset.co.jp/chintai/"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="http://www.goodcomasset.co.jp/chintai/sitemap"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="http://www.goodcomasset.co.jp/chintai/news"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','https://ssl.goodcomasset.co.jp/chintai/common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<!--<h3><img src="https://ssl.goodcomasset.co.jp/chintai/images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="http://www.goodcomasset.co.jp/chintai/property/" method="get">
<h4><img src="https://ssl.goodcomasset.co.jp/chintai/images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="https://ssl.goodcomasset.co.jp/chintai/images/btn_search.gif" onmouseover="this.src='https://ssl.goodcomasset.co.jp/chintai/images/btn_search_r.gif'" onmouseout="this.src='https://ssl.goodcomasset.co.jp/chintai/images/btn_search.gif'">
</form>
-->
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="http://www.goodcomasset.co.jp/chintai/qa/" onMouseOver="MM_swapImage('id_useful_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="http://www.goodcomasset.co.jp/chintai/flow/" onMouseOver="MM_swapImage('id_useful_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="http://www.goodcomasset.co.jp/chintai/trouble/" onMouseOver="MM_swapImage('id_useful_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="https://ssl.goodcomasset.co.jp/chintai/up_img/1264486822-728359.pdf" target="_blank"><img src="https://ssl.goodcomasset.co.jp/chintai/images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','https://ssl.goodcomasset.co.jp/chintai/images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/sld.gif" alt="お問い合わせ・資料請求"></h2>

<p id="pan"><a href="http://www.goodcomasset.co.jp/chintai/">トップページ</a> > お問い合わせ・資料請求</p>
<p>ご意見、ご感想、ご相談はお気軽にお寄せください。<br>
お客様の貴重なご意見・ご要望は、今後のサービス改善に役立たせていただきます。</p>
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/title_02.gif" alt="お電話でのお問い合わせ・資料請求" width="645" height="33"></h3>
<p class="photo"><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/contact_banner01.jpg" alt="お問い合わせ時間：平日10時〜20時　TEL 03-5338-0160" width="645" height="103"></p>
<h3><img src="https://ssl.goodcomasset.co.jp/chintai/contact/images/title_01.gif" alt="フォームでのお問い合わせ・資料請求"></h3>
<p>下記フォームに必要事項をご入力のうえ、「入力内容確認画面へ」ボタンをクリックしてください。<br>
なお、お問い合わせの内容によっては、ご返答が遅れる場合がございます。ご了承ください。</p>
<p><span class="attention">※</span>印は必須項目です。</p>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" name="form1" class="style5" onSubmit="return inputChk(this,false)">
<table width="95%" align="center" cellpadding="8" cellspacing="1" class="listTable">
<tr>
<th width="35%"><div><span class="attention">※</span>
<label for="issue01">お問い合わせ項目</label>
</div></th>
<td width="65%"><select name="type" id="issue01">
<option value="">▼選択してください</option>
<option value="掲載物件について">掲載物件について</option>
<!--<option value="会社について">会社について</option>-->
<option value="その他">その他</option>
</select>
</td>
</tr>
<!--
<tr>
<th><div><span class="attention">※</span>
<label for="issue02a">お問い合わせ項目</label>
</div></th>
<td><ul>
<li>
<input type="radio" name="issue02" id="issue02a" value="Aについて">
<label for="issue02a">Aについて</label>
</li>
<li>
<input type="radio" name="issue02" id="issue02b" value="Bについて">
<label for="issue02b">Bについて</label>
</li>
<li>
<input type="radio" name="issue02" id="issue02c" value="Cについて">
<label for="issue02c">Cについて</label>
</li>
</ul>
<ul>
<li>
<input type="radio" name="issue02" id="issue02d" value="その他お問い合わせ">
<label for="issue02d">その他</label>
<input name="issue02d_text" type="text" id="issue_text" value="" class="ime_on w50per">
</li>
</ul></td>
</tr>-->
<tr>
<th><label for="company">会社名</label></th>
<td><input name="company" type="text" id="company" value="" class="ime_on w90per"></td>
</tr>
<tr>
<th><label for="post">部署・役職名</label></th>
<td><input type="text" name="post" id="post" value="" class="ime_on w90per"></td>
</tr>
<tr>
<th><div><span class="attention">※</span>
<label for="name">お名前</label>
</div></th>
<td><input type="text" name="name" id="name" value="" class="ime_on w90per"></td>
</tr>
<tr>
<th><label for="ruby">フリガナ</label></th>
<td><input type="text" name="kana" id="ruby" value="" class="ime_on w90per"></td>
</tr>
<tr>
<th><label for="male">性別</label></th>
<td>
<input type="radio" name="sex" id="male" value="男性">
<label for="male">男性</label>

<input type="radio" name="sex" id="female" value="女性">
<label for="female">女性</label>
</td>
</tr>
<tr>
<th><label for="age">年齢</label></th>
<td><input type="text" name="age" id="age" value="" maxlength="3" class="ime_off">
歳</td>
</tr>
<tr>
<th><div><span class="attention">※</span><label for="zip">住所</label></div></th>
<td>〒
<input type="text" name="zip" id="zip" maxlength="8" value="" class="ime_off">
　<span>（例）123-4567</span><br>
<label for="state">都道府県名</label>
<select name="state" id="state">
<option value="">▼都道府県を選択してください</option>
<optgroup label="北海道・東北地方">
<option value="北海道">北海道</option>
<option value="青森県">青森県</option>
<option value="岩手県">岩手県</option>
<option value="秋田県">秋田県</option>
<option value="宮城県">宮城県</option>
<option value="山形県">山形県</option>
<option value="福島県">福島県</option>
</optgroup>
<optgroup label="関東地方">
<option value="東京都">東京都</option>
<option value="神奈川県">神奈川県</option>
<option value="埼玉県">埼玉県</option>
<option value="千葉県">千葉県</option>
<option value="茨城県">茨城県</option>
<option value="栃木県">栃木県</option>
<option value="群馬県">群馬県</option>
</optgroup>
<optgroup label="甲信越地方">
<option value="山梨県">山梨県</option>
<option value="長野県">長野県</option>
<option value="新潟県">新潟県</option>
</optgroup>
<optgroup label="東海地方">
<option value="静岡県">静岡県</option>
<option value="愛知県">愛知県</option>
<option value="岐阜県">岐阜県</option>
<option value="三重県">三重県</option>
</optgroup>
<optgroup label="北陸地方">
<option value="富山県">富山県</option>
<option value="石川県">石川県</option>
<option value="福井県">福井県</option>
</optgroup>
<optgroup label="近畿地方">
<option value="大阪府">大阪府</option>
<option value="京都府">京都府</option>
<option value="奈良県">奈良県</option>
<option value="滋賀県">滋賀県</option>
<option value="和歌山県">和歌山県</option>
<option value="兵庫県">兵庫県</option>
</optgroup>
<optgroup label="中国地方">
<option value="岡山県">岡山県</option>
<option value="広島県">広島県</option>
<option value="鳥取県">鳥取県</option>
<option value="島根県">島根県</option>
<option value="山口県">山口県</option>
</optgroup>
<optgroup label="四国地方">
<option value="香川県">香川県</option>
<option value="徳島県">徳島県</option>
<option value="愛媛県">愛媛県</option>
<option value="高知県">高知県</option>
</optgroup>
<optgroup label="九州・沖縄地方">
<option value="福岡県">福岡県</option>
<option value="佐賀県">佐賀県</option>
<option value="長崎県">長崎県</option>
<option value="大分県">大分県</option>
<option value="熊本県">熊本県</option>
<option value="宮崎県">宮崎県</option>
<option value="鹿児島県">鹿児島県</option>
<option value="沖縄県">沖縄県</option>
</optgroup>
</select>
<br>
<label for="address">市区町村・番地・マンション名など</label>
<br>
<input type="text" name="address" id="address" value="" class="ime_on w90per">
</td>
</tr>
<tr>
<th><div><span class="attention">※</span><label for="tel">電話番号</label></div></th>
<td><input name="tel" type="text" id="tel" value="" maxlength="13" class="ime_off w50per"></td>
</tr>
<tr>
<th><label for="fax">ファックス番号</label></th>
<td><input type="text" name="fax" id="fax" value="" maxlength="13" class="ime_off w50per"></td>
</tr>
<tr>
<th><div><span class="attention">※</span>メールアドレス</div></th>
<td><input type="text" name="email" id="email" value="" class="ime_off w90per"></td>
</tr>
<tr>
<th><label for="comment">お問い合わせ内容</label></th>
<td><textarea name="comment" cols="20" rows="8" id="comment" class="ime_on w90per"></textarea></td>
</tr>
</table>

<h3 style="margin-bottom: 5px;"><span class="attention">※</span>個人情報の取り扱いについて</h3>
<table width="95%" align="center" border="1" bordercolor="#B0C4DE" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<span style="font-size: 95%; color: #000099;"><b>以下をご確認頂き、｢同意する｣にチェックをお願い致します。</b></span>
<br />
<span style="font-size: 95%;">
１. 事業者の氏名又は名称　：　株式会社グッドコム
<br />
２. 個人情報保護管理者（若しくはその代理人）の氏名又は職名、所属及び連絡先
<br />
　　管理者名：個人情報保護管理者　所属部署：株式会社グッドコム　賃貸・建物管理部　連絡先：03-5338-0160
<br />
３. 個人情報の利用目的
<br />
　　＜お問い合わせ項目：掲載物件について＞
<br />
　　　掲載物件に関するお問い合わせ対応（本人への連絡を含む）のため
<br />
　　＜お問い合わせ項目：その他＞
<br />
　　　上記以外のお問い合わせ対応（本人への連絡を含む）のため
<br />
４. 個人情報取扱いの委託
<br />
　　当社は事業運営上、前項利用目的の範囲に限って個人情報を外部に委託することがあります。この場合、個人
<br />
　情報保護水準の高い委託先を選定し、個人情報の適正管理・機密保持についての契約を交わし、適切な管理を実施
<br />
　させます。
<br />
５. 個人情報の開示等の請求
<br />
　　ご本人様は、当社に対して本件に関する個人情報の開示等に関して、下記の当社窓口に申し出ることができます。
<br />
　その際、当社はお客様ご本人を確認させていただいたうえで、合理的な期間内に対応いたします。
<br />
　【お問合せ窓口】　〒160-0023　東京都新宿区西新宿７丁目20番１号　住友不動産西新宿ビル17Ｆ
<br />
　　株式会社グッドコム　個人情報問合せ窓口　TEL：03‐5338‐0160 （受付時間　10:00〜18:00※）
<br />
　　　※　土・日曜日、祝日、年末年始、ゴールデンウィーク期間は翌営業日以降の対応とさせていただきます。
<br />
６. 個人情報を提供されることの任意性について
<br />
　　ご本人様が当社に個人情報を提供されるかどうかは任意によるものです。ただし、必要な項目をいただけない場合、
<br />
　適切な対応ができない場合があります。
</span>
</td>
</tr>
</tbody>
</table>
<h3 style="margin-top: -5px;" Align="center"><input type="checkbox" name="agree" id="agree" value="同意する">
<label for="agree">個人情報の取り扱いについて同意する</label></h3>

<div id="formEnd">
<input type="submit" name="Submit" value="入力内容確認画面へ　&raquo;">
<input type="hidden" name="action" value="confirm">
</div>
</form>


<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','https://ssl.goodcomasset.co.jp/chintai/common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>


<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
	<li class="nav_01">
		<a href="http://www.goodcomasset.co.jp/chintai/company">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
	<li class="nav_02">
		<a href="http://www.goodcomasset.co.jp/chintai/privacy">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
	<li class="nav_03">
		<a href="http://www.goodcomasset.co.jp/chintai/sitemap">
			<img src="https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','https://ssl.goodcomasset.co.jp/chintai/common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()">
		</a>
	</li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">

<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="https://ssl.goodcomasset.co.jp/chintai/common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>
こちらからダウンロードしてください。</a></p>
</div>
<div class="clear"></div>
<!--　footer end　--></div>




</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
