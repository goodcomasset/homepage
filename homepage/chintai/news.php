<?php
/*******************************************************************************
SiteWiN 10 20 30（MySQL版）N3-2
新着情報の内容をFlashに出力するプログラム

※使用する際はファイル名を“news.php”にする事！


*******************************************************************************/

// 設定ファイル＆共通ライブラリの読み込み
if(file_exists('./common/config.php')){
	$com_path = './common/';
}
else{
	$com_path = '../common/';
}
require_once($com_path."config_N3_2.php");		// 共通設定情報
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');				// DB操作クラスライブラリ
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');	// 汎用処理クラスライブラリ

#-------------------------------------------------------------------------
# DBより新着情報のデータを取り出す
#-------------------------------------------------------------------------
$sql = "
SELECT
	RES_ID,TITLE,CONTENT,
	YEAR(DISP_DATE) AS Y,
	MONTH(DISP_DATE) AS M,
	DAYOFMONTH(DISP_DATE) AS D,
	DISPLAY_FLG
FROM
	".N3_2WHATSNEW."
WHERE
	(DISPLAY_FLG = '1' )
AND
	(DEL_FLG = '0')
ORDER BY
	DISP_DATE DESC
LIMIT
	0 , ".N3_2DBMAX_CNT."
";

// ＳＱＬを実行
$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);


		for($i=0;$i<count($fetch);$i++):

		//ＨＴＭＬでの表示処理
			//ID
				$id[$i] = $fetch[$i]['RES_ID'];
	
			// 日付
				$time[$i] = $fetch[$i]['Y'].'/'.$fetch[$i]['M'].'/'.$fetch[$i]['D'].' ';

			// タイトル
				$title[$i] = ($fetch[$i]['TITLE'])?$fetch[$i]['TITLE']:"&nbsp;";

			// コメント
				$content[$i] = ($fetch[$i]['CONTENT'])?nl2br($fetch[$i]['CONTENT']):"&nbsp";

		endfor;

//部屋一覧データ
$sql_room = "
SELECT
	C_ROOM_LST.RES_ID,
	C_PRODUCT_LST.TITLE,
	C_PRODUCT_LST.CONTENT2,
	C_ROOM_LST.CONTENT3,
	C_ROOM_LST.CONTENT4,
	C_ROOM_LST.CONTENT8,
	C_ROOM_LST.CONTENT9,
	C_ROOM_LST.CONTENT21
FROM
	C_ROOM_LST
	INNER JOIN
			C_PRODUCT_LST 
		ON
			C_ROOM_LST.PRO_CODE = C_PRODUCT_LST.RES_ID
WHERE
	(C_PRODUCT_LST.DEL_FLG = '0')
AND
	(C_PRODUCT_LST.DISPLAY_FLG = '1')
AND
	(C_ROOM_LST.RECOMMEND_FLG = '1')
AND
	(C_ROOM_LST.DISPLAY_FLG = '1')
ORDER BY
	C_ROOM_LST.RECOMMEND_VO ASC
LIMIT
	0 , 5
";

$fetch_rec = dbOpe::fetch($sql_room,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

?>
