<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
メインコントローラー


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
/*if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}*/

// 不正アクセスチェックのフラグ
$accessChk = 1;

//IDをチェックする
function check_id($id,$table){
	if(empty($id))return false;
	$sql = "
	SELECT
		RES_ID
	FROM
		".$table."
	WHERE
		(RES_ID = '".$id."')
	";
	$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	return (count($fetch) < 1)?false:true;
}

// 設定ファイル＆共通ライブラリの読み込み
require_once("../../common/config_S7.php");	// 共通設定情報
require_once("../tag_pg/LGC_color_table.php");	// タグ処理のプログラム
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');					// DB操作クラスライブラリ
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');				// 汎用処理クラスライブラリ
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/imgOpe.php');					// 画像アップロードクラスライブラリ

#===============================================================================
# $_POST["action"]の内容により処理を分岐
#===============================================================================
switch($_POST["action"]):
case "completion":
	
	// データ登録処理を行い一覧へ戻る
	include("LGC_regist.php");

	header("Location: ../s7_product/?ca=".urlencode($_POST['ca']).'&action=update&res_id='.$pro_code.'#room_list');

	break;
case "update":
//////////////////////////////////////////////////
//	更新画面出力

	//IDが正しくなければ、戻る
	if(check_id($_POST['pro_id'],S7_PRODUCT_LST)==false)header("Location: ../s7_product/");
	
	include("LGC_getDB-data.php");
	include("DISP_update.php");

	break;
case "recommend":
///////////////////////////////////////
//	おすすめ商品登録
//
	include("LGC_changeRecommendFLG.php");

	header("Location: ../s7_product/?ca=".urlencode($_POST['ca']).'&action=update&res_id='.$_POST['pro_id'].'#room_list');	

	break;
case "new_entry":
//////////////////////////////////////////////////
//	新規登録画面出力
	
	//IDが正しくなければ、戻る
	if(check_id($_POST['pro_id'],S7_PRODUCT_LST)==false)header("Location: ../s7_product/");
	
	//include("LGC_getDB-data.php");
	include("DISP_newInput.php");

	break;
case "display_change":case "del_data":
/////////////////////////////////////////////////
//	対象データの表示・非表示の切替 OR 削除
	include("LGC_del_and_dispchng.php");
	header("Location: ../s7_product/?ca=".urlencode($_POST['ca']).'&action=update&res_id='.$_POST['pro_id'].'#room_list');

default:
/////////////////////////////////////////////////
// DBより情報を取得し、一覧表示する


	include("LGC_getDB-data.php");
	include("DISP_listview.php");

endswitch;
?>