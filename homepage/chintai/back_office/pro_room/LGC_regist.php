<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
Logic：DB登録・更新処理


*******************************************************************************/

#=================================================================================
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#=================================================================================
/*if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}*/
// 不正アクセスチェック（直接このファイルにアクセスした場合）
if(!$accessChk){
	header("Location: ../index.php");exit();
}

//カテゴリー情報の取得
	$sql = "
	SELECT
		CATEGORY_CODE,CATEGORY_NAME,VIEW_ORDER
	FROM
		".S7_CATEGORY_MST."
	WHERE
		(DEL_FLG = '0')
	ORDER BY
		VIEW_ORDER ASC
	";
	
	// ＳＱＬを実行
	$fetchCA = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);


#=================================================================================
# POSTデータの受取と文字列処理（共通処理）	※汎用処理クラスライブラリを使用
#=================================================================================
// タグ、空白の除去／危険文字無効化／“\”を取る／半角カナを全角に変換
extract(utilLib::getRequestParams("post",array(8,7,1,4),true));

// MySQLにおいて危険文字をエスケープしておく
$title = utilLib::strRep($title,5);
$room_no = utilLib::strRep($room_no,5);
$regist_no = utilLib::strRep($regist_no,5);
$url = utilLib::strRep($url,5);

$content1 = utilLib::strRep($content1,5);
$content2 = utilLib::strRep($content2,5);
$content3 = utilLib::strRep($content3,5);
$content4 = utilLib::strRep($content4,5);
$content5 = utilLib::strRep($content5,5);
$content6 = utilLib::strRep($content6,5);
$content7 = utilLib::strRep($content7,5);
$content8 = utilLib::strRep($content8,5);
$content9 = utilLib::strRep($content9,5);
$content10 = utilLib::strRep($content10,5);
$content11 = utilLib::strRep($content11,5);
$content12 = utilLib::strRep($content12,5);
$content13 = utilLib::strRep($content13,5);
$content14 = utilLib::strRep($content14,5);
$content15 = utilLib::strRep($content15,5);
$content16 = utilLib::strRep($content16,5);
$content17 = utilLib::strRep($content17,5);
$content18 = utilLib::strRep($content18,5);
$content19 = utilLib::strRep($content19,5);
$content20 = utilLib::strRep($content20,5);
$content21 = utilLib::strRep($content21,5);
$content22 = utilLib::strRep($content22,5);

//$content = utilLib::strRep($content,5);
//$detail_content = utilLib::strRep($detail_content,5);

//ＨＴＭＬタグの有効化の処理（【utilLib::getRequestParams】の文字処理を行う前の情報を使用するためPOSTを使用する）
//$content = html_tag($_POST['content']);
//$detail_content = html_tag($_POST['detail_content']);

#==================================================================
# 更新する内容をここで記述をする
# フィールドの追加・変更はここで修正
#==================================================================
	$sql_update_data = "
		PRO_CODE = '".$pro_code."',
		ROOM_NO = '".$room_no."',
		REGIST_NO = '".$regist_no."',
		URL = '".$url."',
		TITLE = '".$title."',
		CONTENT1 = '".$content1."',
		CONTENT2 = '".$content2."',
		CONTENT3 = '".$content3."',
		CONTENT4 = '".$content4."',
		CONTENT5 = '".$content5."',
		CONTENT6 = '".$content6."',
		CONTENT7 = '".$content7."',
		CONTENT8 = '".$content8."',
		CONTENT9 = '".$content9."',
		CONTENT10 = '".$content10."',
		CONTENT11 = '".$content11."',
		CONTENT12 = '".$content12."',
		CONTENT13 = '".$content13."',
		CONTENT14 = '".$content14."',
		CONTENT15 = '".$content15."',
		CONTENT16 = '".$content16."',
		CONTENT17 = '".$content17."',
		CONTENT18 = '".$content18."',
		CONTENT19 = '".$content19."',
		CONTENT20 = '".$content20."',
		CONTENT21 = '".$content21."',
		CONTENT22 = '".$content22."',
		DISP_DATE = NOW(),
		DISPLAY_FLG = '$display_flg',
		OPEN_FLG = '$open_flg',
		DEL_FLG = '0'
	";
	
#=================================================================================
# 新規か更新かによって処理を分岐	※判断は$_POST["regist_type"]
#=================================================================================
switch($_POST["regist_type"]):
case "update":
//////////////////////////////////////////////////////////
// 対象IDのデータ更新


	// 対象記事IDデータのチェック
	if(!ereg("([0-9]{10,})-([0-9]{6})",$res_id)||empty($res_id)){
		die("致命的エラー：不正な処理データが送信されましたので強制終了します！<br>{$res_id}");
	}

	// 画像ファイル名の決定（POSTで渡された既存の記事ID（$res_id）を使用）
	$for_imgname = $res_id; // POSTで渡された既存記事IDを使用

	// 削除指示がされていたら実行(複数)
	if($_POST["regist_type"]=="update" && $del_img){
		  foreach($del_img as $k => $v){
			if(file_exists(R_IMG_PATH.$res_id."_".$v.".jpg")){
			  unlink(R_IMG_PATH.$res_id."_".$v.".jpg") or die("画像{$v}の削除に失敗しました。");
			}
			}
	}


	// DB格納用のSQL文
	$sql = "
	UPDATE
		".ROOM_MST."
	SET
		$sql_update_data
	WHERE
		(RES_ID = '$res_id')
	";

	break;

case "new":
//////////////////////////////////////////////////////////////////
// 新規登録

	// 画像ファイル名の決定（新しいIDを生成して使用。DB登録時のRES_IDにも使用）
	$res_id = $makeID();
	$for_imgname = $res_id;

	// 現在の登録件数が設定した件数未満の場合のみDBに格納
	$cnt_sql = "SELECT COUNT(*) AS CNT FROM ".S7_PRODUCT_LST." INNER JOIN ".S7_CATEGORY_MST." ON (".S7_PRODUCT_LST.".CATEGORY_CODE = ".S7_CATEGORY_MST.".CATEGORY_CODE) WHERE(".S7_PRODUCT_LST.".DEL_FLG = '0')AND(".S7_CATEGORY_MST.".DEL_FLG = '0')";
	$fetchCNT = dbOpe::fetch($cnt_sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

	//最大登録件数に達していない、そして、カテゴリーが存在している場合登録をする
	if(($fetchCNT[0]["CNT"] < R_DBMAX_CNT) && count($fetchCA)):
		
		#-----------------------------------------------------------------
		#	VIEW_ORDER用の値を作成
		#		※現在登録されている記事データ中の最大VIEW_ORDER値を取得
		#		  それに1足したものを$view_orderに格納して使用
		#		※登録場所チェックが入っていたらVIEW_ORDER値を全て1繰上げ
		#		　$view_orderに1をセットし結果的に登録を一番上にする
		#-----------------------------------------------------------------
		
		if($_POST["regist_type"]=="new" && $ins_chk == 1){
			$vosql ="UPDATE ".S7_PRODUCT_LST." SET VIEW_ORDER = VIEW_ORDER+1 WHERE (CATEGORY_CODE = '$ca')";
			if(!empty($vosql)){
				$db_result = dbOpe::regist($vosql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
				if($db_result)die("DB登録失敗しました<hr>{$db_result}");
			}
			$view_order = 1;
		}
		else{
			$vosql = "SELECT MAX(VIEW_ORDER) AS VO FROM ".S7_PRODUCT_LST." WHERE (CATEGORY_CODE = '$ca')";
			$fetchVO = dbOpe::fetch($vosql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
			$view_order = ($fetchVO[0]["VO"] + 1);
		}
		
		
		$sql = "
		INSERT INTO ".ROOM_MST."
			SET
			RES_ID = '$res_id',
			VIEW_ORDER = '$view_order',
			$sql_update_data
		";
		
	else:
		header("Location: {$_SERVER['PHP_SELF']}");
	endif;

	break;
default:
	die("致命的エラー：登録フラグ（regist_type）が設定されていません");
endswitch;

// ＳＱＬを実行
if(!empty($sql)){
	$db_result = dbOpe::regist($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	if($db_result)die("DB登録失敗しました<hr>{$db_result}");	
}

#=================================================================================
# 共通処理；画像アップロード処理
#=================================================================================
// 画像処理クラスimgOpeのインスタンス生成
$imgObj = new imgOpe(R_IMG_PATH);

// 設定ファイルの画像最大登録枚数分ループ
for($i=1;$i<= R_IMG_CNT;$i++):

// アップロードされた画像ファイルがあればアップロード処理
if(is_uploaded_file($_FILES['up_img']['tmp_name'][$i])){
//echo $i;

	// アップされてきた画像のサイズを計る
		$size = getimagesize($_FILES['up_img']['tmp_name'][$i]);

	//画像サイズを調整
		$size_x = $ox[$i-1];//横の固定サイズ
		$size_y = $size[1]/($size[0]/$size_x);

	// 画像のアップロード：画像名は(記事ID_画像番号.jpg)
		//$imgObj->setSize($ox[$i-1],$oy[$i-1]);
		//$imgObj->isFixed=true;
		$imgObj->setSize($size_x, $size_y);//横固定、縦可変型
		
	if(!$imgObj->up($_FILES['up_img']['tmp_name'][$i],$for_imgname."_".$i)){
		exit("画像のアップロード処理に失敗しました。");
	}

}
endfor;

?>