<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
Logic：ＤＢ情報取得処理ファイル


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
/*if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#--------------------------------------------------------------------------------
# 選択された処理action（$_POST["action"]）により発行するＳＱＬを分岐
#--------------------------------------------------------------------------------
switch($_POST["action"]):
case "update":
///////////////////////////////////////////
// 更新指示のあった該当記事データの取得


	// POSTデータの受け取りと共通な文字列処理
	if($_POST)extract(utilLib::getRequestParams("post",array(8,7,1,4)));

	// 対象記事IDデータのチェック
	if(!ereg("([0-9]{10,})-([0-9]{6})",$res_id)||empty($res_id)){
		die("致命的エラー：不正な処理データが送信されましたので強制終了します！<br>{$res_id}");
	}

	$sql = "
	SELECT
		RES_ID,TITLE,
		PRO_CODE,ROOM_NO,REGIST_NO,
		CONTENT1,
		CONTENT2,
		CONTENT3,
		CONTENT4,
		CONTENT5,
		CONTENT6,
		CONTENT7,
		CONTENT8,
		CONTENT9,
		CONTENT10,
		CONTENT11,
		CONTENT12,
		CONTENT13,
		CONTENT14,
		CONTENT15,
		CONTENT16,
		CONTENT17,
		CONTENT18,
		CONTENT19,
		CONTENT20,
		CONTENT21,
		CONTENT22,
		URL,
		DISPLAY_FLG,
		OPEN_FLG
	FROM
		".ROOM_MST."
	WHERE
		(RES_ID = '$res_id')
	";

	break;
default:
///////////////////////////////////////////
// 記事リスト一覧用データの取得と

	// POSTデータの受け取りと共通な文字列処理
	if($_POST)extract(utilLib::getRequestParams("post",array(8,7,1,4)));


	// 一覧表示用データの取得（リスト順番は設定ファイルに従う）
	$sql = "
	SELECT
		RES_ID,TITLE,
		PRO_CODE,ROOM_NO,REGIST_NO,
		YEAR(DISP_DATE) AS Y,
		MONTH(DISP_DATE) AS M,
		DAYOFMONTH(DISP_DATE) AS D,
		VIEW_ORDER,DISPLAY_FLG
	FROM
		".ROOM_MST."
	WHERE
		(DEL_FLG = '0')
	AND
		(CATEGORY_CODE = '$ca')
	ORDER BY
		VIEW_ORDER ASC
	";

	$sqlcnt = "
	SELECT
		RES_ID,
		TITLE,
		DISPLAY_FLG
	FROM
		".ROOM_MST."
	WHERE
		(DEL_FLG = '0')
		AND
		(DEL_FLG = '0')
	";

	$fetchCNT = dbOpe::fetch($sqlcnt,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

endswitch;

// ＳＱＬを実行
$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

//物件情報
$sql_pro = "
SELECT
	RES_ID,TITLE,CATEGORY_CODE,
	CONTENT1,CONTENT2,CONTENT3,CONTENT4,CONTENT5,CONTENT6,
	TYPE,SIZE,EXTENTION,TYPE2,SIZE2,EXTENTION2
FROM
	".S7_PRODUCT_LST."
WHERE
	(RES_ID = '".$_POST['pro_id']."')
";
$fetch_pro = dbOpe::fetch($sql_pro,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
?>