<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
View：新規登録画面表示


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#---------------------------------------------------------------
/*if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#=============================================================
# HTTPヘッダーを出力
#	文字コードと言語：EUCで日本語
#	他：ＪＳとＣＳＳの設定／有効期限の設定／キャッシュ拒否／ロボット拒否
#=============================================================
utilLib::httpHeadersPrint("EUC-JP",true,false,false,true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title></title>
<script type="text/javascript" src="inputcheck.js"></script>
<link href="../for_bk.css" rel="stylesheet" type="text/css">
<script src="../tag_pg/cms.js" type="text/javascript"></script>
</head>
<body>
<div class="header"></div>
<table width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<form action="../main.php" method="post">
			<input type="submit" value="管理画面トップへ" style="width:150px;">
		</form>
		</td>
		<td>
		</td>
	</tr>
</table>

<p class="page_title"><?php echo TITLE;?>：部屋の新規登録</p>
<p class="explanation">
▼新規データの登録画面です。<br>
▼入力し終えたら<strong>「上記の内容で登録する」</strong>をクリックしてデータを登録してください。
</p>
<form name="new_regist" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data" onSubmit="return confirm_message(this);" style="margin:0px;">
<table width="510" border="1" cellpadding="2" cellspacing="0">
	<tr>
		<th colspan="4" nowrap class="tdcolored">■新規登録</th>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">物件番号：</th>
		<td class="other-td">
		<input name="regist_no" type="text" value="<?php echo $regist_no;?>" size="40" maxlength="127" style="ime-mode:active">
		</td>
		<th width="15%" nowrap class="tdcolored">部屋番号：</th>
		<td class="other-td">
		<input name="room_no" type="text" value="<?php echo $room_no;?>" size="40" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">名称：</th>
		<td colspan="3" class="other-td">
		<input name="title" type="text" value="<?php echo $title;?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">Googleマップ用URL：</th>
		<td colspan="3" class="other-td">
		<input name="url" type="text" value="<?php echo $url;?>" size="60" maxlength="127" style="ime-mode:disabled">
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">住所：</th>
		<td colspan="3" class="other-td">
		<textarea name="content1" cols="60" rows="3" style="ime-mode:active"><?php echo $content1;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">交通：</th>
		<td colspan="3" class="other-td">
		<textarea name="content2" cols="60" rows="3" style="ime-mode:active"><?php echo $content2;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">賃料：</th>
		<td class="other-td">
		<textarea name="content3" cols="15" rows="3" style="ime-mode:active"><?php echo $content3;?></textarea>
		</td>
		<th nowrap class="tdcolored">管理費：</th>
		<td class="other-td">
		<textarea name="content4" cols="15" rows="3" style="ime-mode:active"><?php echo $content4;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">敷金/保証金：</th>
		<td class="other-td">
		<textarea name="content5" cols="15" rows="3" style="ime-mode:active"><?php echo $content5;?></textarea>
		</td>
		<th nowrap class="tdcolored">礼金：</th>
		<td class="other-td">
		<textarea name="content6" cols="15" rows="3" style="ime-mode:active"><?php echo $content6;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">保険：</th>
		<td colspan="3" class="other-td">
		<textarea name="content7" cols="60" rows="3" style="ime-mode:active"><?php echo $content7;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">間取り：</th>
		<td colspan="3" class="other-td">
		<textarea name="content8" cols="60" rows="3" style="ime-mode:active"><?php echo $content8;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">専有面積：</th>
		<td class="other-td">
		<textarea name="content9" cols="15" rows="3" style="ime-mode:active"><?php echo $content9;?></textarea>
		</td>
		<th nowrap class="tdcolored">バルコニー面積：</th>
		<td class="other-td">
		<textarea name="content10" cols="15" rows="3" style="ime-mode:active"><?php echo $content10;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">種別/構造：</th>
		<td class="other-td">
		<textarea name="content11" cols="15" rows="3" style="ime-mode:active"><?php echo $content11;?></textarea>
		</td>
		<th nowrap class="tdcolored">物件階層：</th>
		<td class="other-td">
		<textarea name="content12" cols="15" rows="3" style="ime-mode:active"><?php echo $content12;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">築年月：</th>
		<td class="other-td">
		<textarea name="content13" cols="15" rows="3" style="ime-mode:active"><?php echo $content13;?></textarea>
		</td>
		<th nowrap class="tdcolored">ペット：</th>
		<td class="other-td">
		<textarea name="content14" cols="15" rows="3" style="ime-mode:active"><?php echo $content14;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">設備：</th>
		<td colspan="3" class="other-td">
		<textarea name="content15" cols="60" rows="3" style="ime-mode:active"><?php echo $content15;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">取引形態：</th>
		<td class="other-td">
		<textarea name="content16" cols="15" rows="3" style="ime-mode:active"><?php echo $content16;?></textarea>
		</td>
		<th nowrap class="tdcolored">契約期間：</th>
		<td class="other-td">
		<textarea name="content17" cols="15" rows="3" style="ime-mode:active"><?php echo $content17;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">入居日：</th>
		<td class="other-td">
		<textarea name="content18" cols="15" rows="3" style="ime-mode:active"><?php echo $content18;?></textarea>
		</td>
		<th nowrap class="tdcolored">現況：</th>
		<td class="other-td">
		<textarea name="content19" cols="15" rows="3" style="ime-mode:active"><?php echo $content19;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">備考：</th>
		<td colspan="3" class="other-td">
		<textarea name="content20" cols="60" rows="3" style="ime-mode:active"><?php echo $content20;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">一覧キャッチ：</th>
		<td colspan="3" class="other-td">
		<textarea name="content21" cols="60" rows="3" style="ime-mode:active"><?php echo $content21;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">詳細コメント：</th>
		<td colspan="3" class="other-td">
		<textarea name="content22" cols="60" rows="3" style="ime-mode:active"><?php echo $content22;?></textarea>
		</td>
	</tr>

	<?php for($i=1;$i<=R_IMG_CNT;$i++):?>
	<tr>
		<th nowrap class="tdcolored"><?php echo ($i==1)?"画像":"詳細用画像";?>：</th>
		<td height="35" class="other-td" colspan="3">
		アップロード後画像サイズ：<strong>横<?php echo ($i==1)?IMGSIZE_R_MX:IMGSIZE_R_LX;?>px×縦<?php //echo ($i==1)?IMGSIZE_MY1:IMGSIZE_MY2;echo "px";?> 自動算出</strong>
		<br>
		<input type="file" name="up_img[<?php echo $i;?>]" value="">
		</td>
	</tr>
	<?php endfor;?>

	<tr>
		<th nowrap class="tdcolored">申し込みあり／詳細はこちら：</th>
		<td class="other-td" colspan="3">
		<input name="open_flg" id="openpon" type="radio" value="1" checked><label for="openpon">詳細はこちら</label>&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="open_flg" id="openoff" type="radio" value="0"><label for="openoff">申し込みあり</label>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">トップに登録：</th>
		<td class="other-td" colspan="3">
		<input type="checkbox" name="ins_chk" value="1" id="ins_chk">※この内容を一番上に登録する場合はチェックを入れてください
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">表示／非表示：</th>
		<td class="other-td" colspan="3">
		<input name="display_flg" id="dispon" type="radio" value="1" checked><label for="dispon">表示</label>&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="display_flg" id="dispoff" type="radio" value="0"><label for="dispoff">非表示</label>
		</td>
	</tr>
</table>
<input type="submit" value="上記の内容で登録する" style="width:150px;margin-top:1em;">
<input type="hidden" name="action" value="completion">
<input type="hidden" name="regist_type" value="new">
<input type="hidden" name="pro_code" value="<?php echo $_POST['pro_id'];?>">
</form>

<form action="../s7_product/#room_list" method="post">
	<input type="submit" value="物件編集へ戻る" style="width:150px;">
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="res_id" value="<?php echo $_POST['pro_id'];?>">
	<input type="hidden" name="ca" value="<?php echo $_POST['ca'];?>">
</form>
<form action="../s7_product/" method="post">
	<input type="submit" value="物件一覧画面へ戻る" style="width:150px;">
	<input type="hidden" name="ca" value="<?php echo $_POST['ca'];?>">
</form>

<?php 

//ボタン付近に表示する
cp_disp($layer_free,"0","0");

?>
</body>
</html>