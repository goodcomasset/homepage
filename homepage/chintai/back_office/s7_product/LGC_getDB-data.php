<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
Logic：ＤＢ情報取得処理ファイル


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
/*
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

//カテゴリー情報の取得
	$sql = "
	SELECT
		CATEGORY_CODE,CATEGORY_NAME,VIEW_ORDER
	FROM
		".S7_CATEGORY_MST."
	WHERE
		(DEL_FLG = '0')
	ORDER BY
		VIEW_ORDER ASC
	";
	
	// ＳＱＬを実行
	$fetchCA = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

#--------------------------------------------------------------------------------
# 選択された処理action（$_POST["action"]）により発行するＳＱＬを分岐
#--------------------------------------------------------------------------------
$action=($_POST["action"])?$_POST["action"]:$_GET["action"];
switch($action):
case "update":
///////////////////////////////////////////
// 更新指示のあった該当記事データの取得


	// POSTデータの受け取りと共通な文字列処理
	if($_POST)extract(utilLib::getRequestParams("post",array(8,7,1,4)));
	if($_GET)extract(utilLib::getRequestParams("get",array(8,7,1,4)));
	
	// 対象記事IDデータのチェック
	if(!ereg("([0-9]{10,})-([0-9]{6})",$res_id)||empty($res_id)){
		die("致命的エラー：不正な処理データが送信されましたので強制終了します！<br>{$res_id}");
	}

	$sql = "
	SELECT
		RES_ID,TITLE,CONTENT1,CONTENT2,CONTENT3,CONTENT4,CONTENT5,CONTENT6,
		TYPE,SIZE,EXTENTION,TYPE2,SIZE2,EXTENTION2,
		CATEGORY_CODE,DISPLAY_FLG
	FROM
		".S7_PRODUCT_LST."
	WHERE
		(RES_ID = '$res_id')
	";
	
	//部屋一覧データ
	$sql_room = "
	SELECT
		RES_ID,TITLE,REGIST_NO,ROOM_NO,RECOMMEND_FLG,
		YEAR(UPD_DATE) AS Y,
		MONTH(UPD_DATE) AS M,
		DAYOFMONTH(UPD_DATE) AS D,
		VIEW_ORDER,DISPLAY_FLG
	FROM
		".ROOM_MST."
	WHERE
		(DEL_FLG = '0')
	AND
		(PRO_CODE = '$res_id')
	ORDER BY
		VIEW_ORDER ASC
	";
	$fetch_R = dbOpe::fetch($sql_room,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	
	break;
default:
///////////////////////////////////////////
// 記事リスト一覧用データの取得と

	// POSTデータの受け取りと共通な文字列処理
	if($_POST)extract(utilLib::getRequestParams("post",array(8,7,1,4)));


	// POSTでのカテゴリーのデータが無い場合GETを調べる
	if(!$ca){$ca = urldecode($_GET["ca"]);}

	//カテゴリーパラメータが無い場合または数字ではない場合（全て表示されてしまうため）
	if(empty($ca) || !is_numeric($ca)){$ca = $fetchCA[0]['CATEGORY_CODE'];$ca_name=$fetchCA[0]['CATEGORY_NAME'];}
	
	//カテゴリーのコードが存在しない場合もエラー
		for($i=0,$j=0;$i < count($fetchCA);$i++){
			if($fetchCA[$i]['CATEGORY_CODE'] == $ca){
				$ca_name=$fetchCA[$i]['CATEGORY_NAME'];
				$j=1;break;
			}
		}
		
	//カテゴリーコードと一致するのが無かった場合
	if(!$j){$ca = $fetchCA[0]['CATEGORY_CODE'];$ca_name=$fetchCA[0]['CATEGORY_NAME'];}




	// 一覧表示用データの取得（リスト順番は設定ファイルに従う）
	$sql = "
	SELECT
		RES_ID,TITLE,CONTENT1,CONTENT2,CONTENT3,CONTENT4,CONTENT5,CONTENT6,CATEGORY_CODE,
		TYPE,SIZE,EXTENTION,TYPE2,SIZE2,EXTENTION2,
		YEAR(DISP_DATE) AS Y,
		MONTH(DISP_DATE) AS M,
		DAYOFMONTH(DISP_DATE) AS D,
		VIEW_ORDER,DISPLAY_FLG
	FROM
		".S7_PRODUCT_LST."
	WHERE
		(DEL_FLG = '0')
	AND
		(CATEGORY_CODE = '$ca')
	ORDER BY
		VIEW_ORDER ASC
	";

	$sqlcnt = "
	SELECT
		".S7_PRODUCT_LST.".RES_ID,
		".S7_PRODUCT_LST.".TITLE,
		".S7_PRODUCT_LST.".DISPLAY_FLG
	FROM
		".S7_PRODUCT_LST."
		INNER JOIN
		".S7_CATEGORY_MST."
		ON
		(".S7_PRODUCT_LST.".CATEGORY_CODE = ".S7_CATEGORY_MST.".CATEGORY_CODE)
		
	WHERE
		(".S7_PRODUCT_LST.".DEL_FLG = '0')
		AND
		(".S7_CATEGORY_MST.".DEL_FLG = '0')
	";

	$fetchCNT = dbOpe::fetch($sqlcnt,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

endswitch;

// ＳＱＬを実行
$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

//データを受け渡す
$old_type = $fetch[0]["TYPE"];
$old_size = $fetch[0]["SIZE"];
$old_extension = $fetch[0]['EXTENTION'];
//データを受け渡す
$old_type2 = $fetch[0]["TYPE2"];
$old_size2 = $fetch[0]["SIZE2"];
$old_extension2 = $fetch[0]['EXTENTION2'];

?>