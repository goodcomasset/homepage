<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
View：更新画面表示


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#	※厳しく行う場合はIDとPWも一致するかまで行う
#---------------------------------------------------------------
/*
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#=============================================================
# HTTPヘッダーを出力
#	文字コードと言語：EUCで日本語
#	他：ＪＳとＣＳＳの設定／有効期限の設定／キャッシュ拒否／ロボット拒否
#=============================================================
utilLib::httpHeadersPrint("EUC-JP",true,false,false,true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title></title>
<script type="text/javascript" src="inputcheck.js"></script>
<link href="../for_bk.css" rel="stylesheet" type="text/css">
<script src="../tag_pg/cms.js" type="text/javascript"></script>
</head>
<body>
<div class="header"></div>
<table width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<form action="../main.php" method="post">
			<input type="submit" value="管理画面トップへ" style="width:150px;">
		</form>
		</td>
		<td>
		<form action="sort.php" method="post">
		<input type="submit" value="並び替えを行う" style="width:150px;">
		<input type="hidden" name="ca" value="<?php echo $ca;?>">
		</form>
		</td>
	</tr>
</table>

<p class="page_title"><?php echo TITLE;?>：更新画面</p>
<p class="explanation">
▼現在のデータ内容が初期表示されています。<br>
▼内容を編集したい場合は上書きをして「更新する」をクリックしてください。
</p>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data" onSubmit="return confirm_message(this);" style="margin:0px;">
<table width="600" border="1" cellpadding="2" cellspacing="0">
	<tr>
		<th colspan="2" nowrap class="tdcolored">■データの更新</th>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">カテゴリー：</th>
		<td class="other-td">
			<select name="ca">
			<?php for($i=0;$i<count($fetchCA);$i++){?>
			<option value="<?php echo $fetchCA[$i]['CATEGORY_CODE'];?>" <?php if($fetch[0]['CATEGORY_CODE'] == $fetchCA[$i]['CATEGORY_CODE'])echo "selected";?>><?php echo $fetchCA[$i]['CATEGORY_NAME'];?></option>
			<?php }?>
			</select>
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">物件名：</th>
		<td class="other-td">
			<input name="title" type="text" value="<?php echo $fetch[0]['TITLE'];?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">住所：</th>
		<td class="other-td">
			<input name="content1" type="text" value="<?php echo $fetch[0]['CONTENT1'];?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">路線：</th>
		<td class="other-td">
			<input name="content2" type="text" value="<?php echo $fetch[0]['CONTENT2'];?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">見出し：</th>
		<td class="other-td">
			<input name="content3" type="text" value="<?php echo $fetch[0]['CONTENT3'];?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">キャッチ：</th>
		<td class="other-td">
			<input name="content4" type="text" value="<?php echo $fetch[0]['CONTENT4'];?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">テキスト：</th>
		<td class="other-td">
		<textarea name="content5" cols="55" rows="3" style="ime-mode:active"><?php echo $fetch[0]['CONTENT5'];?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">リード文：</th>
		<td class="other-td">
		<textarea name="content6" cols="55" rows="3" style="ime-mode:active"><?php echo $fetch[0]['CONTENT6'];?></textarea>
		</td>
	</tr>


	<?php for($i=1;$i<=IMG_CNT;$i++):?>
	<tr>
		<th nowrap class="tdcolored">
		<?php echo ($i==1)?"画像":"詳細用画像";?>：</th>
		<td height="35" class="other-td">
		<?php if(file_exists(IMG_PATH.$fetch[0]['RES_ID']."_".$i.".jpg")):?>
		現在表示中の画像<br>
		<img src="<?php echo IMG_PATH.$fetch[0]['RES_ID']."_".$i.".jpg";?>?r=<?php echo rand();?>"><br>
		<input type="checkbox" name="del_img[]" value="<?php echo $i;?>" id="<?php echo $i;?>"><label for="<?php echo $i;?>">この画像を削除</label>
		<br>
		<?php endif;?>
		アップロード後画像サイズ：<strong>横<?php echo ($i==1)?IMGSIZE_MX1:IMGSIZE_MX2;?>px×縦<?php //echo ($i==1)?IMGSIZE_MY1:IMGSIZE_MY2;echo "px";?> 自動算出</strong>
		<br>
		<input type="file" name="up_img[<?php echo $i;?>]" value="">
		</td>
	</tr>
	<?php endfor;?>
	<?php /*
	<tr>
		<th nowrap class="tdcolored">法人用申込書アップロード:</th>
		<td class="other-td">
		<?php if(file_exists(PDF_PATH.$res_id.".".$old_extension)):?>
		<a href="<?php echo PDF_PATH.$fetch[0]["RES_ID"].".".$old_extension;?>" target="_blank"><img src="./icon_img/icon_<?php echo $old_extension;?>.jpg" border="0"></a><br>
		ファイルサイズ：<?php echo $old_size;?><br>
		MIMEタイプ　　：<?php echo $old_type;?><br>
		<!--<input type="checkbox" name="del_pdf" value="1" id="dpdf_flg"><label for="dpdf_flg">このファイルを削除</label><br>-->
		<?php endif;?>
		<input type="file" name="up_file_pdf1" value=""><br>
		
		※アップロードファイルサイズ：<strong><?php echo (LIMIT_SIZE - 1);#余裕を持たす為アップできる容量より小さく表記しておく?>MB以内</strong>
	  　</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">個人用申込書アップロード:</th>
		<td class="other-td">
		<?php if(file_exists(PDF_PATH.$res_id."_2.".$old_extension2)):?>
		<a href="<?php echo PDF_PATH.$fetch[0]["RES_ID"]."_2.".$old_extension2;?>" target="_blank"><img src="./icon_img/icon_<?php echo $old_extension2;?>.jpg" border="0"></a><br>
		ファイルサイズ：<?php echo $old_size2;?><br>
		MIMEタイプ　　：<?php echo $old_type2;?><br>
		<!--<input type="checkbox" name="del_pdf" value="1" id="dpdf_flg"><label for="dpdf_flg">このファイルを削除</label><br>-->
		<?php endif;?>
		<input type="file" name="up_file_pdf2" value=""><br>
		
		※アップロードファイルサイズ：<strong><?php echo (LIMIT_SIZE - 1);#余裕を持たす為アップできる容量より小さく表記しておく?>MB以内</strong>
	  　</td>
	</tr>
	*/?>
	<tr>
		<th nowrap class="tdcolored">表示／非表示：</th>
		<td class="other-td">
			<input name="display_flg" id="dispon" type="radio" value="1"<?php echo ($fetch[0]["DISPLAY_FLG"]==1)?" checked":"";?>>
			<label for="dispon">表示</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="display_flg" id="dispoff" type="radio" value="0"<?php echo ($fetch[0]["DISPLAY_FLG"]==0)?" checked":"";?>>
			<label for="dispoff">非表示</label>
		</td>
	</tr>
</table>
<input type="submit" value="更新する" style="width:150px;margin-top:1em;">
<input type="hidden" name="action" value="completion">
<input type="hidden" name="regist_type" value="update">
<input type="hidden" name="res_id" value="<?php echo $fetch[0]["RES_ID"];?>">

<input type="hidden" name="old_type" value="<?php echo $old_type;?>">
<input type="hidden" name="old_size" value="<?php echo $old_size;?>">
<input type="hidden" name="old_extension" value="<?php echo $old_extension;?>">

<input type="hidden" name="old_type2" value="<?php echo $old_type2;?>">
<input type="hidden" name="old_size2" value="<?php echo $old_size2;?>">
<input type="hidden" name="old_extension2" value="<?php echo $old_extension2;?>">

</form>
<br><br>
<a name="room_list"></a>
<p class="page_title"><?php echo TITLE;?>：部屋一覧</p>
<p class="explanation">
▼新規データの登録を行う際は、<strong>「新規追加」</strong>をクリックしてください。<br>
▼最大登録件数は<strong><?php echo R_DBMAX_CNT;?>件</strong>です。
</p>
<?php
#-----------------------------------------------------
# 書込許可（最大登録件数に達していない）の場合に表示
#-----------------------------------------------------
	//最大件数を超えてない、またはカテゴリーが存在している場合新規登録が出来るようにする
	//基本的にカテゴリーが無ければそのカテゴリーに登録されているデータが表示または存在が出来ない為、編集ボタン側の表示は制限をしない
if(count($fetch_R_CNT) < R_DBMAX_CNT):?>
<table width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<form action="../pro_room/" method="post">
		<input type="submit" value="新規追加" style="width:150px;">
		<input type="hidden" name="action" value="new_entry">
		<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
		</form>
		</td>
		<td>
		<form action="room_sort.php" method="post">
		<input type="submit" value="部屋の並び替えを行う" style="width:150px;">
		<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
		</form>
		</td>
		<td>
		<form action="recommend_sort.php" method="post">
		<input type="submit" value="おすすめ物件の並び替えを行う" style="width:150px;">
		<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
		</form>
		</td>
	</tr>
</table>
<?php else:?>
	<p class="err">
	最大登録件数<?php echo R_DBMAX_CNT;?>件に達しています。<br>
	新規登録を行う場合は、いずれかの既存データを削除してください。
	</p>
	<form action="room_sort.php" method="post">
	<input type="submit" value="並び替えを行う" style="width:150px;">
	<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
	</form>
<?php endif;?>

<?php if(!$fetch_R):?>
	<p><b>登録されている部屋のデータはありません。</b></p>
<?php else:?>
<table width="600" border="1" cellpadding="2" cellspacing="0">
	<tr class="tdcolored">
		<th width="5%" nowrap>表示順</th>
		<th width="15%" nowrap>更新日</th>
		<th width="10%" nowrap>画像</th>
		<th nowrap>物件番号</th>
		<th nowrap>部屋番号</th>
		<th nowrap>名称</th>
		<th nowrap>お勧め</th>
		<th width="5%" nowrap>編集</th>
		<th width="10%" nowrap>表示状態</th>
	    <th width="5%" nowrap>削除</th>
	</tr>
	<?php for($i=0;$i<count($fetch_R);$i++):?>
	<tr class="<?php echo (($i % 2)==0)?"other-td":"otherColTd";?>">
		<td align="center"><?php echo $fetch_R[$i]['VIEW_ORDER'];?></td>
		<td align="center"><?php echo $fetch_R[$i]["Y"].".".$fetch_R[$i]["M"].".".$fetch_R[$i]["D"];?></td>	
		<td align="center">
		<?php if(file_exists(R_IMG_PATH.$fetch_R[$i]['RES_ID']."_1.jpg")):?>
		<a href="<?php echo R_IMG_PATH.$fetch_R[$i]['RES_ID'];?>_1.jpg" target="_blank">
		<img src="<?php echo R_IMG_PATH.$fetch_R[$i]['RES_ID'];?>_1.jpg?r=<?php echo rand();?>" alt="画像" border="0" width="<?php echo IMGSIZE_SX;?>">
		</a>
		<?php else:
			echo '&nbsp;';
		endif;?>
		</td>
		
		<td align="center">&nbsp;<?php echo ($fetch_R[$i]['REGIST_NO'])?mb_strimwidth($fetch_R[$i]['REGIST_NO'], 0, 80, "...", euc):"No Title";?></td>
		<td align="center">&nbsp;<?php echo ($fetch_R[$i]['ROOM_NO'])?mb_strimwidth($fetch_R[$i]['ROOM_NO'], 0, 80, "...", euc):"No Title";?></td>
		<td align="center">&nbsp;<?php echo ($fetch_R[$i]['TITLE'])?mb_strimwidth($fetch_R[$i]['TITLE'], 0, 80, "...", euc):"No Title";?></td>
		<td align="center">
			<form method="post" action="../pro_room/">
			<input type="checkbox" name="recommend_flg" value="1"<?php echo ($fetch_R[$i]['RECOMMEND_FLG'] == 1)?" checked":"";?> onClick="javascript:this.form.submit();">
			<input type="hidden" name="action" value="recommend">
			<input type="hidden" name="res_id" value="<?php echo $fetch_R[$i]['RES_ID'];?>">
			<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
			<input type="hidden" name="ca" value="<?php echo $fetch[0]["CATEGORY_CODE"];?>">
			</form>
		</td>
		<td align="center">
			<form method="post" action="../pro_room/" style="margin:0;">
			<input type="submit" name="reg" value="編集">
			<input type="hidden" name="action" value="update">
			<input type="hidden" name="res_id" value="<?php echo $fetch_R[$i]['RES_ID'];?>">
			<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
			<input type="hidden" name="ca" value="<?php echo $fetch[0]["CATEGORY_CODE"];?>">
			</form>
		</td>
		
		<td align="center">
			<form method="post" action="../pro_room/" style="margin:0;">
			<input type="submit" name="reg" value="<?php echo ($fetch_R[$i]["DISPLAY_FLG"] == 1)?"表示中":"現在非表示";?>" style="width:75px;">
			<input type="hidden" name="res_id" value="<?php echo $fetch_R[$i]['RES_ID'];?>">
			<input type="hidden" name="action" value="display_change">
			<input type="hidden" name="display_change" value="<?php echo ($fetch_R[$i]["DISPLAY_FLG"] == 1)?"f":"t";?>">
			<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
			<input type="hidden" name="ca" value="<?php echo $fetch[0]["CATEGORY_CODE"];?>">
			</form>
		</td>
		
		<td align="center">
			<form method="post" action="../pro_room/" style="margin:0;" onSubmit="return confirm('このデータを完全に削除します。\nデータの復帰は出来ません。\nよろしいですか？');">
			<input type="submit" value="削除">
			<input type="hidden" name="res_id" value="<?php echo $fetch_R[$i]['RES_ID'];?>">
			<input type="hidden" name="action" value="del_data">
			<input type="hidden" name="pro_id" value="<?php echo $fetch[0]['RES_ID'];?>">
			<input type="hidden" name="ca" value="<?php echo $fetch[0]["CATEGORY_CODE"];?>">
			</form>
		</td>
	</tr>
	<?php endfor;?>
</table>
<?php endif;?>

<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<input type="submit" value="一覧画面へ戻る" style="width:150px;">
	<input type="hidden" name="ca" value="<?php echo $fetch[0]["CATEGORY_CODE"];?>">
</form>

<?php 

//ボタン付近に表示する
cp_disp($layer_free,"0","0");

?>
</body>
</html>