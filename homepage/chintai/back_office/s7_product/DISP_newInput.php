<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
View：新規登録画面表示


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#---------------------------------------------------------------
/*
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#=============================================================
# HTTPヘッダーを出力
#	文字コードと言語：EUCで日本語
#	他：ＪＳとＣＳＳの設定／有効期限の設定／キャッシュ拒否／ロボット拒否
#=============================================================
utilLib::httpHeadersPrint("EUC-JP",true,false,false,true);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<title></title>
<script type="text/javascript" src="inputcheck.js"></script>
<link href="../for_bk.css" rel="stylesheet" type="text/css">
<script src="../tag_pg/cms.js" type="text/javascript"></script>
</head>
<body>
<div class="header"></div>
<table width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<form action="../main.php" method="post">
			<input type="submit" value="管理画面トップへ" style="width:150px;">
		</form>
		</td>
		<td>
		<form action="sort.php" method="post">
		<input type="submit" value="並び替えを行う" style="width:150px;">
		<input type="hidden" name="ca" value="<?php echo $ca;?>">
		</form>
		</td>
	</tr>
</table>

<p class="page_title"><?php echo TITLE;?>：新規登録</p>
<p class="explanation">
▼新規データの登録画面です。<br>
▼入力し終えたら<strong>「上記の内容で登録する」</strong>をクリックしてデータを登録してください。
</p>
<form name="new_regist" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data" onSubmit="return confirm_message(this);" style="margin:0px;">
<table width="510" border="1" cellpadding="2" cellspacing="0">
	<tr>
		<th colspan="2" nowrap class="tdcolored">■新規登録</th>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">カテゴリー：</th>
		<td class="other-td">
		<select name="ca">
		<?php for($i=0;$i<count($fetchCA);$i++){?>
		<option value="<?php echo $fetchCA[$i]['CATEGORY_CODE'];?>"<?php echo ($ca == $fetchCA[$i]['CATEGORY_CODE'])?" selected":""; ?>><?php echo $fetchCA[$i]['CATEGORY_NAME'];?></option>
		<?php }?>
		</select>
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">物件名：</th>
		<td class="other-td">
		<input name="title" type="text" value="<?php echo $title;?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">住所：</th>
		<td class="other-td">
		<input name="content1" type="text" value="<?php echo $content1;?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">路線：</th>
		<td class="other-td">
		<input name="content2" type="text" value="<?php echo $content2;?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th width="15%" nowrap class="tdcolored">見出し：</th>
		<td class="other-td">
		<input name="content3" type="text" value="<?php echo $content3;?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>

	<tr>
		<th width="15%" nowrap class="tdcolored">キャッチ：</th>
		<td class="other-td">
		<input name="content4" type="text" value="<?php echo $content4;?>" size="60" maxlength="127" style="ime-mode:active">
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">テキスト：</th>
		<td class="other-td">
		<textarea name="content5" cols="55" rows="3" style="ime-mode:active"><?php echo $content5;?></textarea>
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">リード文：</th>
		<td class="other-td">
		<textarea name="content6" cols="55" rows="3" style="ime-mode:active"><?php echo $content6;?></textarea>
		</td>
	</tr>
	<?php for($i=1;$i<=IMG_CNT;$i++):?>
	<tr>
		<th nowrap class="tdcolored"><?php echo ($i==1)?"画像":"詳細用画像";?>：</th>
		<td height="35" class="other-td">
		アップロード後画像サイズ：<strong>横<?php echo ($i==1)?IMGSIZE_MX1:IMGSIZE_MX2;?>px×縦<?php //echo ($i==1)?IMGSIZE_MY1:IMGSIZE_MY2;echo "px";?> 自動算出</strong>
		<br>
		<input type="file" name="up_img[<?php echo $i;?>]" value="">
		</td>
	</tr>
	<?php endfor;?>
	<?php /*
	<tr>
		<th nowrap class="tdcolored">法人用申込書アップロード:</th>
		<td class="other-td">
		<input type="file" name="up_file_pdf1" value=""><br>
		※アップロードファイルサイズ：<strong><?php echo (LIMIT_SIZE - 1);#余裕を持たす為アップできる容量より小さく表記しておく?>MB以内</strong>
	  　	</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">個人用申込書アップロード:</th>
		<td class="other-td">
		<input type="file" name="up_file_pdf2" value=""><br>
		※アップロードファイルサイズ：<strong><?php echo (LIMIT_SIZE - 1);#余裕を持たす為アップできる容量より小さく表記しておく?>MB以内</strong>
	  　	</td>
	</tr>
	*/?>
	<tr>
		<th nowrap class="tdcolored">トップに登録：</th>
		<td class="other-td">
		<input type="checkbox" name="ins_chk" value="1" id="ins_chk">※この内容を一番上に登録する場合はチェックを入れてください
		</td>
	</tr>
	<tr>
		<th nowrap class="tdcolored">表示／非表示：</th>
		<td class="other-td">
		<input name="display_flg" id="dispon" type="radio" value="1" checked><label for="dispon">表示</label>&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="display_flg" id="dispoff" type="radio" value="0"><label for="dispoff">非表示</label>
		</td>
	</tr>
</table>
<input type="submit" value="上記の内容で登録する" style="width:150px;margin-top:1em;">
<input type="hidden" name="action" value="completion">
<input type="hidden" name="regist_type" value="new">
</form>

<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<input type="submit" value="一覧画面へ戻る" style="width:150px;">
	<input type="hidden" name="ca" value="<?php echo $ca;?>">
</form>

<?php 

//ボタン付近に表示する
cp_disp($layer_free,"0","0");

?>
</body>
</html>