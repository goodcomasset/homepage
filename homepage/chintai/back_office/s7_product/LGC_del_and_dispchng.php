<?php
/*******************************************************************************
Sx系プログラム バックオフィス（MySQL対応版）
Logic:以下の処理を行う
	・表示/非表示の切替(DISPLAY_FLGの切替)
	・削除処理	※完全にデータを削除します。(DELETE文)

※$_POST["action"]の内容で分岐


*******************************************************************************/

#---------------------------------------------------------------
# 不正アクセスチェック（直接このファイルにアクセスした場合）
#---------------------------------------------------------------
/*
if( !$_SERVER['PHP_AUTH_USER'] || !$_SERVER['PHP_AUTH_PW'] ){
	header("Location: ../index.php");exit();
}
*/
if(!$accessChk){
	header("Location: ../index.php");exit();
}

#----------------------------------------------------------------
# POSTデータの受取と共通な文字列処理（対象IDが不正：強制終了）
#----------------------------------------------------------------
// POSTデータの受け取りと共通な文字列処理
extract(utilLib::getRequestParams("post",array(8,7,1,4)));
	
// 対象記事IDデータのチェック
if(!ereg("([0-9]{10,})-([0-9]{6})",$res_id)||empty($res_id)){
	die("致命的エラー：不正な処理データが送信されましたので強制終了します！<br>{$res_id}");
}

#---------------------------------------------------------------
# $_POST["action"]の内容で処理を分岐
#---------------------------------------------------------------
switch($_POST["action"]):
case "del_data":
////////////////////////////////////////////////////////////////
// 該当データの完全削除
	//部屋のIDを獲得ため
	$sql = "SELECT RES_ID FROM C_ROOM_LST WHERE (PRO_CODE = '$res_id')";
	$fetch_room = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	if(count($fetch_room)>0){
		foreach($fetch_room AS $v){
			delroomimg(R_IMG_CNT,$v['RES_ID']);
		}
	}
	
	// SQL実行
	$db_result = dbOpe::regist("DELETE FROM ".ROOM_MST." WHERE(PRO_CODE = '$res_id')",DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	if($db_result)die("DB登録失敗しました<hr>{$db_result}");
	
	
	
	$db_result = dbOpe::regist("DELETE FROM ".S7_PRODUCT_LST." WHERE(RES_ID = '$res_id')",DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	if($db_result)die("DB登録失敗しました<hr>{$db_result}");


	for($i=1;$i<=IMG_CNT;$i++){
	// 記事画像の削除
		if(file_exists(IMG_PATH.$res_id."_".$i.".jpg")){
			unlink(IMG_PATH.$res_id."_".$i.".jpg") or die("画像の削除に失敗しました。");
		}
	}
	//資料ファイルの削除
	if(file_exists(PDF_PATH.$res_id.".".$extension)){
		unlink(PDF_PATH.$res_id.".".$extension) or die("ファイルの削除に失敗しました。");
	}
	//資料ファイルの削除
	if(file_exists(PDF_PATH.$res_id."_2.".$extension2)){
		unlink(PDF_PATH.$res_id."_2.".$extension2) or die("ファイルの削除に失敗しました。");
	}
	
	break;
case "display_change":
////////////////////////////////////////////////////////////////
// 表示/非表示の切替（フラグを更新）

	// 表示／非表示のデータ調整
	$up_display = ($display_change == "t")?1:0;
	
	// SQLを実行
	$db_result = dbOpe::regist("UPDATE ".S7_PRODUCT_LST." SET DISPLAY_FLG = '$up_display' WHERE(RES_ID = '$res_id')",DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	if($db_result)die("DB登録失敗しました<hr>{$db_result}");

endswitch;

function delroomimg($path,$id){
	for($i=1;$i<=R_IMG_CNT;$i++){
	// 記事画像の削除
		if(file_exists($path.$id."_".$i.".jpg")){
			unlink($path.$id."_".$i.".jpg") or die("画像の削除に失敗しました。");
		}
	}
}

?>