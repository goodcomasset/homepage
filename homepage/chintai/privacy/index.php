<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。都心に通いやすく、住みやすい物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,賃貸マンション,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title>グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script>
<script src="../js/JUDGE.JS" type="text/javascript"></script>
<script src="../js/ie7_flash.js" type="text/javascript"></script>
</head>

<body class="privacy" id="pTop" onLoad="MM_preloadImages('../common_img/hNav_01_r.gif','../common_img/hNav_02_r.gif','../common_img/hNav_03_r.gif','../common_img/hNav_04_r.gif','../images/bnr_list_r.jpg','../images/bnr_corporate_r.jpg','../common_img/fNav_01_r.gif','../common_img/fNav_02_r.gif','../common_img/fNav_03_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区を中心とした賃貸マンションのお部屋探しなら株式会社グッドコムまで。</h1>
<div id="header">
<p id="logo"><a href="../"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="../common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="../"><img src="../common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','../common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="../sitemap"><img src="../common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','../common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="../news"><img src="../common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','../common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="../common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','../common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<h3><img src="../images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="../property/" method="get">
<h4><img src="../images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="../images/btn_search.gif" onmouseover="this.src='../images/btn_search_r.gif'" onmouseout="this.src='../images/btn_search.gif'">
</form>

<h3><img src="../common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="../qa/" onMouseOver="MM_swapImage('id_useful_01','','../common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="../flow/" onMouseOver="MM_swapImage('id_useful_02','','../common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="../trouble/" onMouseOver="MM_swapImage('id_useful_03','','../common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="../up_img/1264486822-728359.pdf" target="_blank"><img src="../images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','../images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="images/sld.gif" alt="プライバシーポリシー"></h2>

<p id="pan"><a href="../">トップページ</a> > プライバシーポリシー</p>





<h3><img src="images/policy_title01.jpg" alt="個人情報保護方針" width="655" height="25"></h3>
<name>
  <p>制定日　平成 26 年 9月 8日<br><br>株式会社 グッドコム<br>代表取締役社長　長嶋 義和</p>
</name>
            
<gyokan>
<p>当社は、当社が取り扱う全ての個人情報の保護について、社会的使命を十分に認識し、本人の権利の保護、個人情報に関する法規制等を遵守します。また、以下に示す方針を具現化するための個人情報保護マネジメントシステムを構築し、最新のＩＴ技術の動向、社会的要請の変化、経営環境の変動等を常に認識しながら、その継続的改善に、全社を挙げて取り組むことをここに宣言します。</p>
</gyokan>

<ol>
<li>１．個人情報は、賃貸建物管理業務における当社の正当な事業遂行上ならびに従業員の雇用、人事管理上必要な範囲に限定して、取得・利用及び提供をし、特定された利用目的の達成に必要な範囲を超えた個人情報の取扱いを行わないための措置を講じます。</li>
<li>２．個人情報保護に関する法令、国が定める指針及びその他の規範を遵守致します。</li>
<li>３．個人情報の漏えい、滅失、き損などのリスクに対しては、合理的な安全対策を講じて防止すべく事業の実情に合致した経営資源を注入し個人情報セキュリティ体制を継続的に向上させます。また、万一の際には速やかに是正措置を講じます。</li>
<li>４．個人情報取扱いに関する苦情及び相談に対しては、迅速かつ誠実に、適切な対応をさせていただきます。</li>
<li>５．個人情報保護マネジメントシステムは、当社を取り巻く環境の変化を踏まえ、適時・適切に見直してその改善を継続的に推進します。</li>
</ol>

<line>
<p>本方針は、全ての従業者に配付して周知させるとともに、当社のホームページ、パンフレットなどに掲載することにより、いつでもどなたにも入手可能な措置を取るものとします。</p>
<p>【お問合せ窓口】</p>
<p>個人情報保護方針に関するお問合せにつきましては、下記窓口で受付けております。</p>
<p>〒160-0023　東京都新宿区西新宿７-20-１　住友不動産西新宿ビル17階<br>株式会社グッドコム　個人情報問合せ窓口<br>TEL：03‐5338‐0160 （受付時間　10:00〜18:00※）<br>※　土・日曜日、祝日、年末年始、ゴールデンウィーク期間は翌営業日以降の対応とさせていただきます。</p>
</line>


<h3><img src="images/policy_title02.jpg" alt="個人情報の取り扱いについて" width="655" height="25"></h3>

<line_level2>
<p>１．当社が取り扱う個人情報の利用目的</p>
</line_level2>

<line_level3>
<p>（1）ご本人から直接書面によって取得する個人情報（ホームページや電子メール等によるものを含む）の利用目的<br>取得に先立ち、ご本人に対し書面により明示します。</p>
</line_level3>

<line_level3>
<p>（2）前項以外の方法によって取得する個人情報の利用目的</p>
</line_level3>
<table style="margin-left:3.0em;" border="1" cellpadding="3" cellspacing="0">
<thead>
	<tr>
		<th width="220">分類</th>
		<th width="260">利用目的</th>
	</tr>
	</thead>

<tbody>
	<tr>
		<td width="220">お客様情報（お取引先等）</td>
		<td width="260">弊社商品・サービスのご案内のため</td>
	</tr>
	<tr>
		<td width="220">業務の受託に伴い、お客様からお預かりする個人情報</td>
		<td width="260">賃貸管理業務、建物管理業務など、委託された当該業務を適切に遂行するため</td>
	</tr>
	<tr>
		<td width="220">電話応対情報</td>
		<td width="260">お客様相談室への相談内容の確認のため</td>
	</tr>
</tbody>
</table>

<h3><img src="images/policy_title03.jpg" alt="開示対象個人情報に関する周知事項" width="655" height="25"></h3>

<br />

<gyokan>
&nbsp; &nbsp;当社で保有している開示対象個人情報に関して、ご本人様又はその代理人様からの利用目的の通知、開示、内容の訂正、追加又は削除、利用の停止、消去及び第三者への提供の停止の請求（以下「開示等の請求」といいます。）につきましては、以下の要領にて対応させていただきます。
</gyokan>

<line_level2>
<p>１．事業者の名称<br>株式会社グッドコム</p>
</line_level2>

<line_level2>
<p>２．個人情報の管理者<br>管理者名職名：個人情報保護管理者<br>所属部署：株式会社グッドコム　賃貸・建物管理部<br>連絡先：電話03(5338)0160</p>
</line_level2>


<line_level2>
<p>３．全ての開示対象個人情報の利用目的</p>
</line_level2>
<table style="margin-left:3.0em;" border="1" cellpadding="3" cellspacing="0">
<thead>
	<tr>
        <th width="220">分類</th>
        <th width="265">利用目的</th>
	</tr>
</thead>
	                
<tbody>
    <tr>
        <td width="220">お客様情報（ユーザー様等）</td>
        <td width="265">お問合せ対応のため<br>弊社商品・サービスのご案内のため</td>
    </tr>
    <tr>
        <td width="220">お客様情報（お取引先等）</td>
        <td width="265">弊社商品・サービスのご案内のため</td>
    </tr>
    <tr>
        <td width="220">当社従業員情報</td>
        <td width="265">社員の人事労務管理、業務管理、健康管理、セキュリティ管理のため</td>
    </tr>
    <tr>
        <td width="220">当社への採用応募者情報</td>
        <td width="265">採用応募者への連絡と当社の採用業務管理のため</td>
    </tr>
    <tr>
		<td width="220">電話応対情報</td>
		<td width="265">お客様相談室への相談内容の確認のため</td>
    </tr>
</tbody>
</table>

<line_level2>
<p>４．個人情報取扱いの委託<br>当社は事業運営上、前項利用目的の範囲に限って個人情報を外部に委託することがあります。<br>この場合、個人情報保護水準の高い委託先を選定し、個人情報の適正管理・機密保持について<br>の契約を交わし、適切な管理を実施させます。</p>
</line_level2>


<line_level2>
<p>５．開示対象個人情報の取扱いに関する苦情の申し出先<br>〒160-0023　東京都新宿区西新宿７丁目20番１号　住友不動産西新宿ビル17Ｆ<br>株式会社グッドコム　個人情報問合せ窓口<br>TEL：03‐5338‐0160</p>
</line_level2>


<line_level2>
<p>６．認定個人情報保護団体<br>現在、当社が加盟する認定個人情報保護団体はありません。</p>
</line_level2>

<line_level2>
<p>７．開示対象個人情報の開示等の求めに応じる手続き</p>
</line_level2>

<line_level3>
<p>（1）開示等の求めの申し出先<br>開示等のお求めは、上記個人情報問合せ窓口にお申し出ください。</p>
</line_level3>

<line_level3>
<p>（2）開示等の求めに関するお手続き</p>
</line_level3>

<line_level4>
<p>�，�申し出受付け後、当社からご利用いただく所定の請求書様式を郵送いたします。<br>・ 利用目的の通知の場合：「開示対象個人情報利用目的通知請求書」<br>・ 開示の場合：「開示対象個人情報開示請求書」<br>・ 訂正、追加又は削除の場合、利用の停止、消去及び第三者への提供の停止の場合：<br>&nbsp; &nbsp;「開示対象個人情報訂正等及び利用停止等請求書」</p>
</line_level4>


<line_level4>
<p>�△患�入いただいた請求書、代理人によるお求めの場合は代理人であることを確認する書類、手数料分の郵便為替（利用目的の通知並びに開示の請求の場合のみ）を上記個人情報問合せ窓口までご郵送ください。</p>
<p>��上記請求書を受領後、ご本人確認のため、当社に登録していただいている個人情報のうちご本人確認可能な２項目程度（例：電話番号と生年月日等）の情報をお問合せさせていただきます。</p>
<p>�げ鹽�は原則としてご本人に対して書面（封書郵送）にておこないます。</p>
</line_level4>

<line_level3>
<p>（3）代理人によるお求めの場合、代理人であることを確認する資料<br>開示等をお求めになる方が代理人様である場合は、代理人である事を証明する資料及び代理人様ご自身を証明する資料を同封してください。各資料に含まれる本籍地情報は都道府県までとし、それ以降の情報は黒塗り等の処理をしてください。</p>
</line_level3>

<line_level4>
<p>�‖緲�人である事を証明する資料</p>
</line_level4>

<line_level5>
<p>＜開示等の求めをすることにつき本人が委任した代理人様の場合＞<br>・本人の委任状（原本）</p>
</line_level5>

<line_level5>
<p>＜代理人様が未成年者の法定代理人の場合＞いずれかの写し<br>・戸籍謄本<br>・住民票（続柄の記載されたもの）<br>・その他法定代理権の確認ができる公的書類</p>
</line_level5>
<line_level5>
<p>＜代理人様が成年被後見人の法定代理人の場合＞いずれかの写し<br>・後見登記等に関する登記事項証明書<br>・その他法定代理権の確認ができる公的書類</p>
</line_level5>

<line_level4>
<p>��代理人様ご自身を証明する資料<br>・運転免許証<br>・パスポート<br>・健康保険の被保険者証<br>・住民票<br>・住民基本台帳カード</p>
</line_level4>

<line_level3>
<p>（4）利用目的の通知又は開示のお求めについての手数料<br>１回のお求めにつき1,000円（消費税抜き）<br>（お送りいただく請求書等に郵便為替を同封していただきます。）</p>
</line_level3>

<finisher><p>以上</p></finisher>

<line>

</line>


<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="../contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
<li class="nav_01"><a href="../company"><img src="../common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','../common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_02"><a href="../privacy"><img src="../common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','../common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_03"><a href="../sitemap"><img src="../common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','../common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>
<div id="footer">
<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="../common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>
こちらからダウンロードしてください。</a></p>
</div>
<div class="clear"></div>
<!--　footer end　--></div>



</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
