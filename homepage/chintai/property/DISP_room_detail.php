<?php
/*******************************************************************************
SiteWiN 10 20 30（MySQL版）
新着情報の内容を表示するプログラム

DSP：DBより取得情報をHTML出力

*******************************************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("Location: ../");exit();
}

#=============================================================
# HTTPヘッダーを出力
#	文字コードと言語：EUCで日本語
#	他：ＪＳとＣＳＳの設定／有効期限の設定／キャッシュ拒否／ロボット拒否
#=============================================================
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンション<?php echo $fetch_pro[0]['TITLE'];?>／<?php echo $fetch[0]['ROOM_NO'];?>のお部屋探しをサポートしております。<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>,賃貸マンション,<?php echo $fetch_pro[0]['TITLE'];?>,<?php echo $fetch[0]['ROOM_NO'];?>,部屋探し  ">
<meta name="robots" content="INDEX,FOLLOW">
<title><?php echo $fetch_pro[0]['TITLE'];?>（<?php echo $fetch[0]['ROOM_NO'];?>）｜グッドコム（goodcom）｜<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の賃貸マンション </title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script>
<script src="../js/JUDGE.JS" type="text/javascript"></script>
<script src="../js/ie7_flash.js" type="text/javascript"></script>
</head>

<body class="property detail" id="pTop" onLoad="MM_preloadImages('../common_img/hNav_01_r.gif','../common_img/hNav_02_r.gif','../common_img/hNav_03_r.gif','../common_img/hNav_04_r.gif','../images/bnr_list_r.jpg','../images/bnr_corporate_r.jpg','../common_img/fNav_01_r.gif','../common_img/fNav_02_r.gif','../common_img/fNav_03_r.gif','images/btn_01_r.gif','images/btn_corporation_r.gif','images/btn_individual_r.gif','images/sample2_01.jpg','images/sample2_02.jpg','images/sample2_03.jpg','images/sample2_04.jpg','images/sample2_05.jpg')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1><?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の賃貸マンションをお探しなら、株式会社グッドコムが提供する<?php echo $fetch_pro[0]['TITLE'];?>（<?php echo $fetch[0]['ROOM_NO'];?>）。</h1>
<div id="header">
<p id="logo"><a href="../"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="../common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="../"><img src="../common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','../common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="../sitemap"><img src="../common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','../common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="../news"><img src="../common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','../common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="../common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','../common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<h3><img src="../images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="../property/" method="get">
<h4><img src="../images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="../images/btn_search.gif" onmouseover="this.src='../images/btn_search_r.gif'" onmouseout="this.src='../images/btn_search.gif'">
</form>

<h3><img src="../common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="../qa/" onMouseOver="MM_swapImage('id_useful_01','','../common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="../flow/" onMouseOver="MM_swapImage('id_useful_02','','../common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="../trouble/" onMouseOver="MM_swapImage('id_useful_03','','../common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="../up_img/1264486822-728359.pdf" target="_blank"><img src="../images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','../images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="images/sld.gif" alt="物件紹介"></h2>

<p id="pan"><a href="../">トップページ</a> > <a href="../property">物件紹介</a> &gt; <?php echo $fetch_pro[0]['TITLE'];?> - <?php echo $fetch[0]['ROOM_NO'];?> </p>

<h3><?php echo $fetch_pro[0]['TITLE'];?>　‐　<?php echo $fetch[0]['ROOM_NO'];?>　</h3>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">
<div id="mainImg">
<?php 
$img1="./up_room_img/".$fetch[0]['RES_ID']."_3.jpg";
if(file_exists($img1)){?>
	<img src="./up_room_img/<?php echo $fetch[0]['RES_ID'];?>_3.jpg" name="id_bImg" width="527" id="id_bImg">
<?php }?>
</div></td>
<td valign="top"><table border="0" cellspacing="0" cellpadding="0" id="sImgBox">
<tr>
<td valign="top">
<?php 
if(file_exists($img1)){?>
<a href="javascript:void(0)"><img src="<?php echo $img1;?>" alt="<?php echo $fetch_pro[0]['TITLE'];?>" width="101" border="0" onClick="MM_swapImage('id_bImg','','<?php echo $img1;?>',1)"></a>
<?php }?>
</td>
</tr>
<tr><td valign="top">
<?php 
$img2="./up_room_img/".$fetch[0]['RES_ID']."_4.jpg";
if(file_exists($img2)){?>
<a href="javascript:void(0)"><img src="<?php echo $img2;?>" alt="<?php echo $fetch_pro[0]['TITLE'];?>" width="101" border="0" onClick="MM_swapImage('id_bImg','','<?php echo $img2;?>',1)"></a>
<?php }?>
</td></tr>
<tr><td valign="top">
<?php 
$img3="./up_room_img/".$fetch[0]['RES_ID']."_5.jpg";
if(file_exists($img3)){?>
<a href="javascript:void(0)"><img src="<?php echo $img3;?>" alt="<?php echo $fetch_pro[0]['TITLE'];?>" width="101" border="0" onClick="MM_swapImage('id_bImg','','<?php echo $img3;?>',1)"></a>
<?php }?>
</td></tr>
<tr><td valign="top">
<?php 
$img4="./up_room_img/".$fetch[0]['RES_ID']."_6.jpg";
if(file_exists($img4)){?>
<a href="javascript:void(0)"><img src="<?php echo $img4;?>" alt="<?php echo $fetch_pro[0]['TITLE'];?>" width="101" border="0" onClick="MM_swapImage('id_bImg','','<?php echo $img4;?>',1)"></a>
<?php }?>
</td></tr>
<tr><td valign="top">
<?php 
$img5="./up_room_img/".$fetch[0]['RES_ID']."_7.jpg";
if(file_exists($img5)){?>
<a href="javascript:void(0)"><img src="<?php echo $img5;?>" alt="<?php echo $fetch_pro[0]['TITLE'];?>" width="101" border="0" onClick="MM_swapImage('id_bImg','','<?php echo $img5;?>',1)"></a>
<?php }?>
</td></tr>

</table></td>
</tr>
</table>



<p class="click"><span class="attention">※</span>クリックすると写真が切り替わります</p>

<?php if($fetch[0]['CONTENT22']){?>
<div class="bg_txtBox">
<div class="txtBox"><p><?php echo nl2br($fetch[0]['CONTENT22']);?></p>
</div></div>
<?php }?>
<table width="640" border="0" align="center" cellpadding="8" cellspacing="1" class="listTable">
<tr>
<th width="89">物件番号</th>
<td><?php echo $fetch[0]['REGIST_NO'];?></td>
<th>部屋番号</th>
<td width="190"><?php echo $fetch[0]['ROOM_NO'];?></td>
</tr>

<tr>
<th>名称</th>
<td colspan="3"><?php echo $fetch[0]['TITLE'];?></td>
</tr>

<tr>
<th>住所</th>
<td colspan="3"><?php echo nl2br($fetch[0]['CONTENT1']);?><br>
<?php if($fetch[0]['URL']){?>
<a href="<?php echo $fetch[0]['URL'];?>" target="_blank"><img src="images/btn_google.gif" alt="Googleマップを見る" name="id_btn_google" border="0" id="id_btn_google" onMouseOver="MM_swapImage('id_btn_google','','images/btn_google_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a> 
<?php }?>
</td>
</tr>

<tr>
<th>交通</th>
<td colspan="3"><?php echo nl2br($fetch[0]['CONTENT2']);?></td>
</tr>

<tr>
<th>賃料</th>
<td width="200"><?php echo nl2br($fetch[0]['CONTENT3']);?></td>
<th width="90">管理費</th>
<td><?php echo nl2br($fetch[0]['CONTENT4']);?></td>
</tr>

<tr>
<th>敷金/保証金</th>
<td><?php echo nl2br($fetch[0]['CONTENT5']);?></td>
<th>礼金</th>
<td><?php echo nl2br($fetch[0]['CONTENT6']);?></td>
</tr>

<tr>
<th>保険</th>
<td colspan="3"><?php echo nl2br($fetch[0]['CONTENT7']);?></td>
</tr>

<tr>
<th>間取り</th>
<td colspan="3"><?php echo nl2br($fetch[0]['CONTENT8']);?></td>
</tr>

<tr>
<th>専有面積</th>
<td><?php echo nl2br($fetch[0]['CONTENT9']);?></td>
<th>バルコニー面積</th>
<td><?php echo nl2br($fetch[0]['CONTENT10']);?></td>
</tr>

<tr>
<th>種別/構造</th>
<td><?php echo nl2br($fetch[0]['CONTENT11']);?></td>
<th>物件階層</th>
<td><?php echo nl2br($fetch[0]['CONTENT12']);?></td>
</tr>

<tr>
<th>築年月</th>
<td><?php echo nl2br($fetch[0]['CONTENT13']);?></td>
<th>ペット</th>
<td><?php echo nl2br($fetch[0]['CONTENT14']);?></td>
</tr>

<tr>
<th>設備</th>
<td colspan="3"><?php echo nl2br($fetch[0]['CONTENT15']);?></td>
</tr>

<tr>
<th>取引形態</th>
<td><?php echo nl2br($fetch[0]['CONTENT16']);?></td>
<th>契約期間</th>
<td><?php echo nl2br($fetch[0]['CONTENT17']);?></td>
</tr>

<tr>
<th>入居日</th>
<td><?php echo nl2br($fetch[0]['CONTENT18']);?></td>
<th>現況</th>
<td><?php echo nl2br($fetch[0]['CONTENT19']);?></td>
</tr>

<tr>
<th>備考</th>
<td colspan="3" valign="top"><?php echo nl2br($fetch[0]['CONTENT20']);?></td>
</tr>
</table>


<dl id="koushin">
<dt>更新日</dt><dd><?php echo $fetch[0]['Y'].'年'.$fetch[0]['M'].'月'.$fetch[0]['D'].'日';?></dd>
</dl>

<table border="0" align="center" cellpadding="0" cellspacing="5" class="btnBox">
<tr>
<td>
<?php
$pdf1 = "../up_img/1264734876-820072.pdf";
if(file_exists($pdf1)){?>
<a href="<?php echo $pdf1;?>" target="_blank"><img src="images/btn_corporation.gif" alt="法人用申込書" name="id_btn_corporation" border="0" id="id_btn_corporation" onMouseOver="MM_swapImage('id_btn_corporation','','images/btn_corporation_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
<?php }?>
</td>
<td>
<?php
$pdf2 = "../up_img/1264734894-283903.pdf";
if(file_exists($pdf2)){?>
<a href="<?php echo $pdf2;?>" target="_blank"><img src="images/btn_individual.gif" alt="個人様用申込書" name="id_btn_individual" border="0" id="id_btn_individual" onMouseOver="MM_swapImage('id_btn_individual','','images/btn_individual_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a> 
<?php }?>
</td>
</tr>
</table>


<div class="btnBox" align="center"><a href="../flow/"><img src="images/btn_flow.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_btn_flow" border="0" id="id_btn_flow" onMouseOver="MM_swapImage('id_btn_flow','','images/btn_flow_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></div>

<div class="btnBox2"><a href="./?ca=<?php echo $fetch_pro[0]['CATEGORY_CODE'];?>"><img src="images/btn_list.gif" alt="マンションの一覧へ戻る" name="id_btn_list" border="0" id="id_btn_list" onMouseOver="MM_swapImage('id_btn_list','','images/btn_list_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a>
<a href="./?id=<?php echo $fetch_pro[0]['RES_ID'];?>"><img src="images/btn_room_back.gif" alt="部屋の一覧に戻る" name="id_btn_room_back" border="0" id="id_btn_room_back" onMouseOver="MM_swapImage('id_btn_room_back','','images/btn_room_back_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></div>

<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
<li class="nav_01"><a href="../company"><img src="../common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','../common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_02"><a href="../privacy"><img src="../common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','../common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_03"><a href="../sitemap"><img src="../common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','../common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">
<div class="clear"></div>
<!--　footer end　--></div>




</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
