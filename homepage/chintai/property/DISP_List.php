<?php
/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム
View：取得したデータをHTML出力

***********************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("Location: ../");exit();
}

	#--------------------------------------------------------
	# ページング用リンク文字列処理
	#--------------------------------------------------------
	
	//ページリンクの初期化
	$link_prev = "";
	$link_next = "";
	
		// 次ページ番号
		$next = $p + 1;
		// 前ページ番号
		$prev = $p - 1;
		
		// 商品全件数
		$tcnt = count($fetchCNT);
		// 全ページ数
		$totalpage = ceil($tcnt/DISP_MAXROW);
	
		// カテゴリー別で表示していればページ遷移もカテゴリーパラメーターをつける
		if($ca)$cpram = "&ca=".urlencode($ca);

		// 前ページへのリンク
		if($p > 1){
			$link_prev = "<a href=\"".$_SERVER['PHP_SELF']."?p=".urlencode($prev).$cpram."\">&lt;&lt; Prev</a>";
		}
		
		//次ページリンク
		if($totalpage > $p){
			$link_next = "<a href=\"".$_SERVER['PHP_SELF']."?p=".urlencode($next).$cpram."\">Next &gt;&gt;</a>";
		}


#-------------------------------------------------------------
# HTTPヘッダーを出力
#	１．文字コードと言語：EUCで日本語
#	２．ＪＳとＣＳＳの設定：する
#	３．有効期限：設定しない
#	４．キャッシュ拒否：設定する
#	５．ロボット拒否：設定しない
#-------------------------------------------------------------
utilLib::httpHeadersPrint("EUC-JP",true,true,false,false);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>,賃貸マンション,部屋探し  ">
<meta name="robots" content="INDEX,FOLLOW">
<title><?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の物件情報｜グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script>
<script src="../js/JUDGE.JS" type="text/javascript"></script>
<script src="../js/ie7_flash.js" type="text/javascript"></script>
</head>

<body class="property list" id="pTop" onLoad="MM_preloadImages('../common_img/hNav_01_r.gif','../common_img/hNav_02_r.gif','../common_img/hNav_03_r.gif','../common_img/hNav_04_r.gif','../images/bnr_list_r.jpg','../images/bnr_corporate_r.jpg','../common_img/fNav_01_r.gif','../common_img/fNav_02_r.gif','../common_img/fNav_03_r.gif','images/btn_01_r.gif','images/btn_room_r.gif','images/bg_area_r1.jpg')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区、<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の賃貸マンションのお部屋探しなら株式会社グッドコム。 </h1>
<div id="header">
<p id="logo"><a href="../"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="../common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="../"><img src="../common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','../common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="../sitemap"><img src="../common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','../common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="../news"><img src="../common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','../common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="../common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','../common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<h3><img src="../images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>


<?php include("../menu.php");?>
<form action="../property/" method="get">
<h4><img src="../images/title_search.gif" alt="エリアで検索"></h4>
<?php echo ShowMenu();?>
<input type="image" src="../images/btn_search.gif" onmouseover="this.src='../images/btn_search_r.gif'" onmouseout="this.src='../images/btn_search.gif'">
</form>

<h3><img src="../common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="../qa/" onMouseOver="MM_swapImage('id_useful_01','','../common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="../flow/" onMouseOver="MM_swapImage('id_useful_02','','../common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="../trouble/" onMouseOver="MM_swapImage('id_useful_03','','../common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="../up_img/1264486822-728359.pdf" target="_blank"><img src="../images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','../images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="images/sld.gif" alt="物件紹介"></h2>

<p id="pan"><a href="../">トップページ</a> > 物件紹介</p>

<h3><img src="images/title_area.gif" alt="エリアで検索" width="633" height="32"></h3>

<form action="../property/#region_01" method="get">

<div id="area">
<img src="images/bg_area.jpg" alt="※ご希望のエリアをクリックしてください。" width="633" height="364" border="0" usemap="#map_area" id="id_area" href="#">
<map name="map_area">
<?php foreach($per_array AS $v){?>
<?php if(GetProCount($v['category_code'])>0){?>
<area shape="rect" coords="<?php echo $v['maparea'];?>" href="../property/?ca=<?php echo $v['category_code'];?>#region_01" onMouseOver="MM_swapImage('id_area','','images/<?php echo $v['image_name'];?>',1)" onMouseOut="MM_swapImgRestore()">
<?php }?>
<?php }?>
</map>
<?php echo ShowMenu2();?>
</div>


</form>


<h3 class="category" id="region_01"><?php echo $fetchCANAME[0]['CATEGORY_NAME'];?></h3>

<?php
// 登録がない場合のエラーメッセージ
echo $disp_no_data;
?>
<?php
	// 全商品分ループ(縦ループ)
	for($i=0;$i<count($fetch);$i++){
?>
<?php
		// クロス回数ループ(横ループ) ※クロス表示でなくてもそのまま(スクリプトの変更不要)
		for($j=0;$j<LINE_MAXCOL;$j++):

		#===============================================================================================
		# 変数を整形する
		# DBから取り出して整形が必要な変数等は軽い変数名に代入してテーブルテンプレートに貼り付ける
		# 例１）$id : 画像名等で頻繁に使用するので変数名を短くする
		# 例２）金額用変数 : number_formatを指定
		# 例３）改行込み文章 : nl2br
		# 例４）GET送信用変数 : urlencode
		# 例５）画像用変数
		#===============================================================================================
		
		//ＩＤ
		$id = $fetch[$i+$j]['RES_ID'];
		
		// タイトル
		$title = $fetch[$i+$j]['TITLE'];

		// コメント
		$content1 = nl2br($fetch[$i+$j]['CONTENT1']);
		$content2 = nl2br($fetch[$i+$j]['CONTENT2']);
		$content3 = nl2br($fetch[$i+$j]['CONTENT3']);
		$content4 = nl2br($fetch[$i+$j]['CONTENT4']);
		$content5 = nl2br($fetch[$i+$j]['CONTENT5']);
		$content6 = nl2br($fetch[$i+$j]['CONTENT6']);

		// 画像
		$img = "./up_img/".$id."_1.jpg";
		if(!file_exists($img)){
			$image = "";
			//$tsize1 = "";
			//$tsize2 = "width=\"100%\" colspan=\"2\" align=\"left\" valign=\"top\" class=\"s_text2\"";
		}else{

			//画像サイズが固定でない場合（サイズ自動調整、横固定縦可変など）
				$size = getimagesize($img);//画像サイズを取得
				
				$image = "<a href=\"./?id=".urlencode($id)."\">";
				$image .= "<img src=\"".$img."?r=".rand()."\" width=\"".$size[0]."\" height=\"".$size[1]."\" alt=\"".$fetch[$i+$j]['TITLE']."\" border=\"0\" class=\"photo\"></a>";

				//$tsize1 = "width=\"30%\" class=\"s_image\"";
				//$tsize2 = "width=\"70%\" align=\"left\" class=\"s_text\"";
		}
		
		//詳細ページへのリンク
			$detail = "<a href=\"./?id=".urlencode($id)."\"><img src=\"../image/detail.jpg\" alt=\"詳細\" border=\"0\"></a>";
		
		#==============================================================================================
		# テーブルテンプレート貼り付け
		# HTMLから商品情報テーブルソースを貼り付け変数を展開
		# 必ずソースをすっきりさせるためヒアドキュメントは使用せず
		# 上記で変数を整形してから貼り付ける
		#==============================================================================================
		//最初のtdに設置する、表示するサイズを指定（指定が無い場合は空白)
		//（例　$tdsize = "style=\"width:180px;\""; $tdsize = "class=\"list_width\"";）
		//$tdsize ="width=\"450px\"";

		$table = "
		<h4><strong>{$title}</strong><br>
		{$content1}／{$content2}</h4>

		<div class=\"bg_listBox\">
		<div class=\"listBox\">

		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
		<td align=\"left\" valign=\"top\">{$image}</td>
		<td width=\"100%\" align=\"left\" valign=\"top\"><dl>
		<dt>{$content3}</dt>
		<dd>{$content4}</dd>
		</dl><p align=\"left\">{$content5}</p>

		<div class=\"buy\">{$content6}</div>
		<p id=\"btn_room\"><a href=\"./?id=".urlencode($id)."\"><img src=\"images/btn_room.gif\" alt=\"この物件の部屋を見る\" name=\"id_btn_room11\" border=\"0\" id=\"id_btn_room".($i+$j)."\" onMouseOver=\"MM_swapImage('id_btn_room".($i+$j)."','','images/btn_room_r.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a> </p>

		</td>
		</tr>
		</table>
		</div></div>					
		";
		
		#=====================================================================
		# テーブルを表示
		# ※クロス表示でなくてもそのまま
		# ＊＊＊ スクリプトの変更不要 ＊＊＊
		#=====================================================================
		echo (!empty($id))?$table:"";

		endfor;
		
		$i=$i+(LINE_MAXCOL-1);
	}
?>

<table width="520px" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td width="50%" align="left">&nbsp;
	<?php echo $link_prev;?>
	</td>
	<td width="50%" align="right">&nbsp;
	<?php echo $link_next;?>
	</td>
</tr>
</table>

<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
<li class="nav_01"><a href="../company"><img src="../common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','../common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_02"><a href="../privacy"><img src="../common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','../common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_03"><a href="../sitemap"><img src="../common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','../common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">
<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="../common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<div class="clear"></div>
<!--　footer end　--></div>




</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
