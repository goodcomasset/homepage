<?php
/***********************************************************
SiteWin10 20 30（MySQL対応版）
S系表示用プログラム コントローラー
	

***********************************************************/
error_reporting (E_ALL);
//error_reporting (E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
error_reporting (E_ERROR | E_WARNING | E_PARSE);
ini_set('track_errors',1);
ini_set('display_errors',1);
// 共通ライブラリ読み込み
require_once('../common/config_S7.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/util_lib.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/dbOpe.php');
require_once('/home/users/web02/9/2/0095529/www.goodcomasset.co.jp/common/tmpl2.class.php');// テンプレートクラスライブラリ

	// 不正アクセスチェックのフラグ
	$injustice_access_chk = 1;


	// 商品IDが送信されパラメーターが不正でなければ商品詳細を表示
	if( isset($_GET['id']) && ereg("([0-9]{10,})-([0-9]{6})", $_GET['id']) ){
		// 商品情報取得
		include("LGC_getDB-data.php");
		include("DISP_room_list.php");
		
	}elseif( isset($_GET['roomid']) && ereg("([0-9]{10,})-([0-9]{6})", $_GET['roomid']) ){

		// 商品情報取得
		include("LGC_getRoomData.php");
		include("DISP_room_detail.php");
		
	}else{
	//詳細画面の処理
		// 商品情報取得
		include("LGC_getDB-data.php");
		// 取得件数分のデータをHTML出力
		include("DISP_List.php");
	}		
?>