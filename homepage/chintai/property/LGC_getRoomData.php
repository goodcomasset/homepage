<?php
/*******************************************************************************

	LOGIC:DBよりデータの取得
 
2006.06.09 fujiyama
*******************************************************************************/

// 不正アクセスチェック
if(!$injustice_access_chk){
	header("HTTP/1.0 404 Not Found");exit();
}
if($_GET)extract(utilLib::getRequestParams("get",array(8,7,1,4,5)));

$room_id = urldecode($roomid);
#-------------------------------------------------------------------------
# 詳細画面へのデータ処理関連
#-------------------------------------------------------------------------
if(!empty($room_id)):

	// パラメータがないもしくは不正なデータを混入された状態でアクセスされた場合のエラー処理
	if(empty($room_id) || !ereg("([0-9]{10,})-([0-9]{6})",$room_id) ){
		header("Location: ../");exit();
	}


	//部屋一覧データ
	$sql_room = "
	SELECT
		RES_ID,TITLE,REGIST_NO,ROOM_NO,PRO_CODE,
		CONTENT1,CONTENT2,CONTENT3,CONTENT4,CONTENT5,CONTENT6,
		CONTENT7,CONTENT8,CONTENT9,CONTENT10,CONTENT11,CONTENT12,
		CONTENT13,CONTENT14,CONTENT15,CONTENT16,CONTENT17,CONTENT18,
		CONTENT19,CONTENT20,CONTENT21,CONTENT22,URL,
		YEAR(UPD_DATE) AS Y,
		MONTH(UPD_DATE) AS M,
		DAYOFMONTH(UPD_DATE) AS D,
		VIEW_ORDER,DISPLAY_FLG,OPEN_FLG
	FROM
		".ROOM_MST."
	WHERE
		(DEL_FLG = '0')
	AND
		(RES_ID = '$room_id')
	AND
		(DISPLAY_FLG = '1')
	ORDER BY
		VIEW_ORDER ASC
	";
	$fetch = dbOpe::fetch($sql_room,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

	$sql = "
	SELECT
		RES_ID,TITLE,CONTENT1,CONTENT2,CONTENT3,CONTENT4,CONTENT5,CONTENT6,
		TYPE,SIZE,EXTENTION,TYPE2,SIZE2,EXTENTION2,
		CATEGORY_CODE,DISPLAY_FLG
	FROM
		".S7_PRODUCT_LST."
	WHERE
		(RES_ID = '".$fetch[0]['PRO_CODE']."')
	AND
		(DEL_FLG = '0')
	AND
		(DISPLAY_FLG = '1')
	";

	$fetch_pro = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);
	
	$sql = "
	SELECT
		CATEGORY_CODE,CATEGORY_NAME,CATEGORY_DETAILS,VIEW_ORDER
	FROM
		".S7_CATEGORY_MST."
	WHERE
		(DEL_FLG = '0')
		AND
		(DISPLAY_FLG = '1')
		AND
		(CATEGORY_CODE = '".$fetch_pro[0]['CATEGORY_CODE']."')
	ORDER BY
		VIEW_ORDER ASC
	";
	
	// ＳＱＬを実行
	$fetchCANAME = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);


endif;
// 商品が何も登録されていない場合に表示
if(count($fetch) == 0):
	$disp_no_data = "<br><div align=\"center\"><br><br><br>ただいま準備中のため、もうしばらくお待ちください。<br><br><br></div>";
else:
	$disp_no_data = "";
endif;
?>
