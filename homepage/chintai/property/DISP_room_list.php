<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンション<?php echo $fetch_pro[0]['TITLE'];?>のお部屋探しをサポートしております。<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>,賃貸マンション,<?php echo $fetch_pro[0]['TITLE'];?>,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title><?php echo $fetch_pro[0]['TITLE'];?>｜グッドコム（goodcom）｜<?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の賃貸マンション </title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script>
<script src="../js/JUDGE.JS" type="text/javascript"></script>
<script src="../js/ie7_flash.js" type="text/javascript"></script>
</head>

<body class="property room" id="pTop" onLoad="MM_preloadImages('../common_img/hNav_01_r.gif','../common_img/hNav_02_r.gif','../common_img/hNav_03_r.gif','../common_img/hNav_04_r.gif','../images/bnr_list_r.jpg','../images/bnr_corporate_r.jpg','../common_img/fNav_01_r.gif','../common_img/fNav_02_r.gif','../common_img/fNav_03_r.gif','images/btn_01_r.gif','images/btn_room_r.gif','images/btn_detail_r.gif','images/btn_madori_r.gif','images/btn_list_r.gif','images/btn_room_back_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1><?php echo $fetchCANAME[0]['CATEGORY_NAME'];?>の賃貸マンションをお探しなら、株式会社グッドコムが提供する<?php echo $fetch_pro[0]['TITLE'];?>。</h1>
<div id="header">
<p id="logo"><a href="../"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="../common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="../"><img src="../common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','../common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="../sitemap"><img src="../common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','../common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="../news"><img src="../common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','../common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="../common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','../common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<h3><img src="../images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="../property/" method="get">
<h4><img src="../images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="../images/btn_search.gif" onmouseover="this.src='../images/btn_search_r.gif'" onmouseout="this.src='../images/btn_search.gif'">
</form>

<h3><img src="../common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="../qa/" onMouseOver="MM_swapImage('id_useful_01','','../common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="../flow/" onMouseOver="MM_swapImage('id_useful_02','','../common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="../trouble/" onMouseOver="MM_swapImage('id_useful_03','','../common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="../up_img/1264486822-728359.pdf" target="_blank"><img src="../images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','../images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">

<h2><img src="images/sld.gif" alt="物件紹介"></h2>

<p id="pan"><a href="../">トップページ</a> > <a href="../property">物件紹介</a> &gt; <?php echo $fetch_pro[0]['TITLE'];?></p>


<h3 class="category"><?php echo $fetchCANAME[0]['CATEGORY_NAME'];?></h3>
<?php
// 登録がない場合のエラーメッセージ
echo $disp_no_data;
?>
<?php
	// 全商品分ループ(縦ループ)
	for($i=0;$i<count($fetch);$i++){
?>
<?php
		// クロス回数ループ(横ループ) ※クロス表示でなくてもそのまま(スクリプトの変更不要)
		for($j=0;$j<LINE_MAXCOL;$j++):

		#===============================================================================================
		# 変数を整形する
		# DBから取り出して整形が必要な変数等は軽い変数名に代入してテーブルテンプレートに貼り付ける
		# 例１）$id : 画像名等で頻繁に使用するので変数名を短くする
		# 例２）金額用変数 : number_formatを指定
		# 例３）改行込み文章 : nl2br
		# 例４）GET送信用変数 : urlencode
		# 例５）画像用変数
		#===============================================================================================
		
		//ＩＤ
		$id = $fetch[$i+$j]['RES_ID'];
		
		// タイトル
		$title = $fetch[$i+$j]['TITLE'];

		// 画像
		$img = "./up_room_img/".$id."_1.jpg";
		if(!file_exists($img)){
			$image = "";
			//$tsize1 = "";
			//$tsize2 = "width=\"100%\" colspan=\"2\" align=\"left\" valign=\"top\" class=\"s_text2\"";
		}else{

			//画像サイズが固定でない場合（サイズ自動調整、横固定縦可変など）
				$size = getimagesize($img);//画像サイズを取得
				
				$image = "<a href=\"./?roomid=".urlencode($id)."\" class=\"photo\">";
				$image .= "<img src=\"".$img."?r=".rand()."\" width=\"".$size[0]."\" height=\"".$size[1]."\" alt=\"".$fetch[$i+$j]['TITLE']."\" border=\"0\"></a>";

				//$tsize1 = "width=\"30%\" class=\"s_image\"";
				//$tsize2 = "width=\"70%\" align=\"left\" class=\"s_text\"";
		}
		
		//詳細ページへのリンク
			$detail = "<a href=\"./?roomid=".urlencode($id)."\"><img src=\"../image/detail.jpg\" alt=\"詳細\" border=\"0\"></a>";
			
		//間取り画像
		$img2 = "./up_room_img/".$id."_2.jpg";
		if(!file_exists($img2)){
			$image2 = "";
		}else{

			//画像サイズが固定でない場合（サイズ自動調整、横固定縦可変など）
				$size = getimagesize($img2);//画像サイズを取得
				
				$image2 = "<a href=\"javascript:void(0);\" onClick=\"MM_openBrWindow('nw.php?id=".urlencode($id)."','','scrollbars=yes,resizable=yes,width=700,height=500')\">";
				$image2 .= "<img src=\"images/btn_madori.gif\" alt=\"間取画像\" name=\"id_btn_madori\" border=\"0\" id=\"id_btn_madori_".($i+$j)."\" onMouseOver=\"MM_swapImage('id_btn_madori_".($i+$j)."','','images/btn_madori_r.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>";
		}
		
		//申し込みあり／詳細はこちら
		if($fetch[$i+$j]['OPEN_FLG']==1){
			$btn = "<a href=\"./?roomid=".urlencode($id)."\"><img src=\"images/btn_detail.gif\" alt=\"詳細はこちら\" name=\"id_btn_detail111\" border=\"0\" id=\"id_btn_detail111\" onMouseOver=\"MM_swapImage('id_btn_detail111','','images/btn_detail_r.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">";
		}else{
			$btn = "<img src=\"images/btn_app.gif\" alt=\"申し込みあり\" name=\"id_btn_app1\" border=\"0\" id=\"id_btn_app1\">";
		}
		
		#==============================================================================================
		# テーブルテンプレート貼り付け
		# HTMLから商品情報テーブルソースを貼り付け変数を展開
		# 必ずソースをすっきりさせるためヒアドキュメントは使用せず
		# 上記で変数を整形してから貼り付ける
		#==============================================================================================
		//最初のtdに設置する、表示するサイズを指定（指定が無い場合は空白)
		//（例　$tdsize = "style=\"width:180px;\""; $tdsize = "class=\"list_width\"";）
		//$tdsize ="width=\"450px\"";

		$table = "
		<h4>".$fetch_pro[0]['TITLE']."　‐　".$fetch[$i+$j]['ROOM_NO']."　</h4>
		<p class=\"no\">".$fetch[$i+$j]['REGIST_NO']."</p>

		<div class=\"clear\"></div>
		<p class=\"read\">".nl2br($fetch[$i+$j]['CONTENT21'])."</p>

		<div class=\"roomBox\">

		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
		<td align=\"left\" valign=\"top\">{$image}</td>
		<td width=\"100%\" align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
		<td valign=\"top\">".nl2br($fetch_pro[0]['CONTENT1'])."</td>
		</tr>
		<tr>
		<td valign=\"top\">".nl2br($fetch_pro[0]['CONTENT2'])."</td>
		</tr>
		<tr>
		<td valign=\"top\">".nl2br($fetch[$i+$j]['CONTENT8'])."／".nl2br($fetch[$i+$j]['CONTENT9'])."　".$image2."</td>
		</tr>
		<tr>
		<td width=\"100%\" valign=\"top\">
		賃料／管理費：".nl2br($fetch[$i+$j]['CONTENT3'])."／".nl2br($fetch[$i+$j]['CONTENT4'])."</td>
		</tr>
		</table>
		<p class=\"btn\">{$btn}</p>
		<div class=\"clear\"></div></td>
		</tr>
		</table>
		</div>
		";
		
		#=====================================================================
		# テーブルを表示
		# ※クロス表示でなくてもそのまま
		# ＊＊＊ スクリプトの変更不要 ＊＊＊
		#=====================================================================
		echo (!empty($id))?$table:"";

		endfor;
		
		$i=$i+(LINE_MAXCOL-1);
	}
?>


<div class="btnBox2"><a href="javascript:history.back();"><img src="images/btn_list.gif" alt="マンションの一覧へ戻る" name="id_btn_list" border="0" id="id_btn_list" onMouseOver="MM_swapImage('id_btn_list','','images/btn_list_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></div>
<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>

<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
<!-- contents end --></div>

<div class="clear"></div>

<ul id="fNav">
<li class="nav_01"><a href="../company"><img src="../common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','../common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_02"><a href="../privacy"><img src="../common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','../common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_03"><a href="../sitemap"><img src="../common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','../common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">
<div class="clear"></div>
<!--　footer end　--></div>




</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
