<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。都心に通いやすく、住みやすい物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,賃貸マンション,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title>グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script>
<script src="../js/JUDGE.JS" type="text/javascript"></script>
<script src="../js/ie7_flash.js" type="text/javascript"></script>
</head>

<body class="qa" id="pTop" onLoad="MM_preloadImages('../common_img/hNav_01_r.gif','../common_img/hNav_02_r.gif','../common_img/hNav_03_r.gif','../common_img/hNav_04_r.gif','../images/bnr_list_r.jpg','../images/bnr_corporate_r.jpg','../common_img/fNav_01_r.gif','../common_img/fNav_02_r.gif','../common_img/fNav_03_r.gif','../common_img/fBnr_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区を中心とした賃貸マンションのお部屋探しなら株式会社グッドコムまで。</h1>
<div id="header">
<p id="logo"><a href="../"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="../common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="../"><img src="../common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','../common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="../sitemap"><img src="../common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','../common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="../news"><img src="../common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','../common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="../common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','../common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<h3><img src="../images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>

<?php include("../menu.php");?>
<form action="../property/" method="get">
<h4><img src="../images/title_search.gif" alt="エリアで検索"></h4>

<?php echo ShowMenu();?>
<input type="image" src="../images/btn_search.gif" onmouseover="this.src='../images/btn_search_r.gif'" onmouseout="this.src='../images/btn_search.gif'">
</form>

<h3><img src="../common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="../qa/" onMouseOver="MM_swapImage('id_useful_01','','../common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="../flow/" onMouseOver="MM_swapImage('id_useful_02','','../common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="../trouble/" onMouseOver="MM_swapImage('id_useful_03','','../common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="../up_img/1264486822-728359.pdf" target="_blank"><img src="../images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','../images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">
<h2><img src="images/sld.gif" alt="お部屋探しのQ&amp;A"></h2>
<p id="pan"><a href="../">トップページ</a> > お部屋探しのQ&amp;A</p>
<h3>申し込みには何が必要なの？</h3>
<p class="anthor"> お部屋を借りるためにお申し込みされる場合、まず、入居申込書に記入していただきます。その際、ご本人確認のため、身分証明書の提示が必要です。お部屋を探す時には、<strong>運転免許証・健康保険証・パスポート</strong>などを必ず持っていくようにしましょう。<br>
外国籍の方は、<strong>外国人登録証明書</strong>が身分証明書になります。<br>
また、新入学生・新社会人・転職される方は<strong>合格通知書や内定通知書</strong>を身分証と一緒に持っていくようにしましょう。そうすると、申し込みがよりスムーズに進みます。</p>
<h3>申し込みや契約にはどんな手続きが必要なの？</h3>
<p class="anthor"> 「<a href="../flow/">お部屋探しから引っ越しまでの流れ</a>」ページでご確認ください。</p>
<h3>入居審査って何？</h3>
<p class="anthor"> 入居審査では、「申し込みの内容に<strong>虚偽がないか</strong>」「収入に比べてあまりにも高額な賃料の部屋に申し込みしていないか」といったことを確認します。</p>
<h3>申し込みの時、費用はかかるの？</h3>
<p class="anthor"> 基本的にお申し込み時にお金はかかりませんが、場合によっては申し込み保証金が必要になることがあります。多額のお金を持っていく必要はありませんが、多少用意しておくとよいでしょう。</p>
<h3>保証人になってくれる人がいないんだけど…連帯保証人は絶対必要？</h3>
<p class="anthor"><br>
お部屋を借りる時には、連帯保証人が必要です。そして連帯保証人はご親族の方になっていただくようお願いしています。しかし、ご親族の方が高齢であったり、親族が日本にいなかったりと、様々な理由で連帯保証人を立てられない方もいらっしゃるでしょう。当社ではそのような方も入居できるよう、<strong>保証会社を利用する</strong>というシステムを採用しています。「保証人がいないから…」とあきらめず、まずはお問い合わせください。</p>
<h3>契約の時ってどれくらいの費用がかかるの？</h3>
<p class="anthor"> 募集条件によって金額は様々です。例えば、『敷金2ヶ月・礼金2ヶ月』という募集条件の場合、およそ家賃の6ヶ月分強くらいが必要となります。敷金・礼金の他に、賃料1ヶ月分と仲介手数料がかかるためです。しかし、6ヶ月というのはあくまで目安で、もし入居開始日が月の半ばを過ぎている場合、日割りの賃料と翌月分の賃料が合算されますので、オーバーすることもあります。保証人ではなく保証会社を利用する場合は保証契約料もかかりますので、詳しい金額についてはお問い合わせください。<br>
また、その他に火災(家財)保険料・鍵交換費用などが必要ですので、条件は必ず確認しましょう。<br>
なお、家賃が賃料と管理費に分かれている場合、敷金・礼金は賃料をもとに計算されます。管理費は含まれません。</p>
<h3> 共益費・管理費って何？</h3>
<p class="anthor"> 月々の家賃の中に、賃料とは別にある共益費や管理費。これは建物の共用部分の使用料です。共用部分とは、玄関外の廊下やエレベーター、エントランス等を指します。</p>
<h3>どれくらいの期間で住めるようになるの？</h3>
<p class="anthor"> 入居可能日を参考にしてください。新築で入居可能日が日にちまで記載されている物件はその日から入居できますが、「入居可能日の前日に申し込みをしたから、翌日に入居できる」ということはもちろんできません。審査や契約の時間が必要となるからです。<br>
即日入居可能となっている場合、入居できるのは申し込みをしてからだいたい10日前後ほど。早急に契約を進めたいという時は、必要書類などをあらかじめ準備しておくとよいでしょう。</p>
<h3>契約の時には何がいるの？</h3>
<p class="anthor"> 入居者の住民票・連帯保証人の確約書（これは不動産会社から渡される書類です）・連帯保証人の印鑑証明書が必要となります。住民票と印鑑証明書は3ヶ月以内にとったものを提出してください。<br>
この他に、申し込みの時に必要な身分証や内定通知書なども用意しておくとよいでしょう。なお、当社では入居される方の顔写真のご提出をお願いいたしております。身分証に顔写真がない場合は、別途ご用意ください。<br>
また、ご契約時には書類に捺印していただくためハンコをご用意ください。シャチハタは不可です。</p>
<h3>契約期間はどのくらい？</h3>
<p class="anthor"> 契約期間は2年間です。その後も引き続き住む時は更新契約をして、さらに2年間住むことができます。</p>
<h3>ワンルームだけど2人で住んでも大丈夫？</h3>
<p class="anthor"> 「2人入居可能」とお断りしている部屋がありますので、まずはお問い合わせください。2人で住む場合、同居人の方の住民票や身分証なども必要になります。<br>
無断で同居人を入れるのは違反になりますので、必ず確認をしてください。</p>
<h3>ペットを飼いたいんだけど…。</h3>
<p class="anthor"> ペットの飼育が可能な物件かどうか、お問い合わせください。飼育可能な物件でも、飼える種類や数に制限があります。<br>
また、飼うためには敷金などの追加入金や、飼育申請書類の提出などが必要です。</p>
<h3>楽器を弾きたい！</h3>
<p class="anthor"> 申し訳ございませんが、当社の物件は楽器不可です。</p>
<h3>火災(家財)保険は入らないといけないの？</h3>
<p class="anthor"> 賃貸契約をするにあたって、<strong>火災(家財)保険の加入は必須</strong>です。火災の時だけでなく、水漏れ事故や盗難などにも対応している保険ですので、万が一の時のため、必ず加入してください。</p>
<h3>会社で借りることはできる？</h3>
<p class="anthor"> 社宅として法人申し込みしていただくことは可能です。通常のお申し込み同様、審査などを行います。また、法人契約の場合、ご提出していただく書類が変わってきますので、詳しくはお問い合わせください。</p>
<h3>お部屋を事務所として使用したいんだけど…。</h3>
<p class="anthor"> 事務所として使用可能な建物か確認いたしますので、まずはお問い合わせください。使用可能な物件でも、<strong>職種などに制限</strong>がございます。人の出入りの多い職種などはお断りさせていただいておりますので、あらかじめご了承ください。SOHO（自宅兼事務所）として使用される場合も同様です。</p>
<h3>やっぱり借りるのをやめたいんだけど…。</h3>
<p class="anthor"> 契約キャンセルはとても残念ですが、仕方ありませんね。契約開始日前であれば契約金をご返金いたします。ただし、お客様のご要望により鍵交換などを行っているなど、すでに費用が発生している場合、その費用についてはお支払いいただくことがございます。</p>
<p>その他、ご不明な点がございましたら、お気軽にお問い合わせください。</p>
<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>
<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
<!-- contents end -->
</div>
<div class="clear"></div>

<ul id="fNav">
<li class="nav_01"><a href="../company"><img src="../common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','../common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_02"><a href="../privacy"><img src="../common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','../common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_03"><a href="../sitemap"><img src="../common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','../common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">
<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="../common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>
こちらからダウンロードしてください。</a></p>
</div>
<div class="clear"></div>
<!--　footer end　--></div>



</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
