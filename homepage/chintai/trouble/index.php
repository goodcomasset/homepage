<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta http-equiv="Content-Language" content="ja">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="株式会社グッドコムは、東京都心部を中心とした賃貸マンションのお部屋探しをサポートしております。都心に通いやすく、住みやすい物件をお探しの方は、お気軽にご相談ください。">
<meta name="keywords" content="グッドコム,goodcom,東京,都心,23区,賃貸マンション,部屋探し ">
<meta name="robots" content="INDEX,FOLLOW">
<title>グッドコム（goodcom）｜東京都心部、23区を中心とした賃貸マンションのお部屋探し </title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script>
<script src="../js/JUDGE.JS" type="text/javascript"></script>
<script src="../js/ie7_flash.js" type="text/javascript"></script>
</head>

<body class="trouble" id="pTop" onLoad="MM_preloadImages('../common_img/hNav_01_r.gif','../common_img/hNav_02_r.gif','../common_img/hNav_03_r.gif','../common_img/hNav_04_r.gif','../images/bnr_list_r.jpg','../images/bnr_corporate_r.jpg','../common_img/fNav_01_r.gif','../common_img/fNav_02_r.gif','../common_img/fNav_03_r.gif','../common_img/fBnr_r.gif')">
<div id="bg2_wrapper">
<div id="bg_wrapper">
<div id="wrapper">


<h1>東京都心部、23区を中心とした賃貸マンションのお部屋探しなら株式会社グッドコムまで。</h1>
<div id="header">
<p id="logo"><a href="../"><img src="../common_img/logo.jpg" alt="東京都心部の賃貸マンション検索サイト株式会社グッドコム賃貸物件検索サイト" border="0"></a></p>
<p id="hAdd"><img src="../common_img/hAdd.gif" alt="平日10時〜20時　TEL 03-5338-0160"></p>


<ul id="hNav">
<li class="nav_01"><a href="../"><img src="../common_img/hNav_01.gif" alt="HOME" name="id_hNav_01" border="0" id="id_hNav_01" onMouseOver="MM_swapImage('id_hNav_01','','../common_img/hNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li> 
<li class="nav_02"><a href="../sitemap"><img src="../common_img/hNav_02.gif" alt="サイトマップ" name="id_hNav_02" border="0" id="id_hNav_02" onMouseOver="MM_swapImage('id_hNav_02','','../common_img/hNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_03"><a href="../news"><img src="../common_img/hNav_03.gif" alt="お知らせ" name="id_hNav_03" border="0" id="id_hNav_03" onMouseOver="MM_swapImage('id_hNav_03','','../common_img/hNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
<li class="nav_04"><a href="https://ssl.goodcomasset.co.jp/chintai/contact/"><img src="../common_img/hNav_04.gif" alt="お問い合わせ・資料請求" name="id_hNav_04" border="0" id="id_hNav_04" onMouseOver="MM_swapImage('id_hNav_04','','../common_img/hNav_04_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>
<div class="clear"></div>
<!-- header end --></div>



<div id="container">

<div id="primary">
<div id="leftBox">
<h3><img src="../images/title_property.gif" alt="物件紹介" width="215" height="29"></h3>
<?php include("../menu.php");?>
<form action="../property/" method="get">
<h4><img src="../images/title_search.gif" alt="エリアで検索"></h4>
<?php echo ShowMenu();?>
<input type="image" src="../images/btn_search.gif" onmouseover="this.src='../images/btn_search_r.gif'" onmouseout="this.src='../images/btn_search.gif'">
</form>

<h3><img src="../common_img/title_useful.gif" alt="お部屋探しのお役立ち情報"></h3>

<ul id="useful">
<li class="nav_01"><a href="../qa/" onMouseOver="MM_swapImage('id_useful_01','','../common_img/useful_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_01.gif" alt="お部屋探しのQ&amp;A" name="id_useful_01" border="0" id="id_useful_01"></a></li> 
<li class="nav_02"><a href="../flow/" onMouseOver="MM_swapImage('id_useful_02','','../common_img/useful_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_02.gif" alt="お部屋探しからお引っ越しまでの流れ" name="id_useful_02" border="0" id="id_useful_02"></a></li>
<li class="nav_03"><a href="../trouble/" onMouseOver="MM_swapImage('id_useful_03','','../common_img/useful_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/useful_03.gif" alt="住んでいて困ったことがあったら・・・" name="id_useful_03" border="0" id="id_useful_03"></a></li>
</ul>

<p><a href="../up_img/1264486822-728359.pdf" target="_blank"><img src="../images/bnr_list.jpg" alt="業者空室一覧" name="id_bnr_list" border="0" id="id_bnr_list" onMouseOver="MM_swapImage('id_bnr_list','','../images/bnr_list_r.jpg',1)" onMouseOut="MM_swapImgRestore()"></a></p>

</div>
<!-- primary end --></div>

<div id="contents">
<h2><img src="images/sld.gif" alt="住んでいて困ったことがあったら・・・"></h2>
<p id="pan"><a href="../">トップページ</a> > 住んでいて困ったことがあったら・・・</p>
<h3>電気がつかない・電球が切れた。</h3>
<p class="anthor"> お部屋の照明器具がつかない時は、まずブレーカーを確認してみましょう。大元のブレーカーとそれぞれの設備ごとの小ブレーカー、どちらもONになっていますか？一見ONになっているように見えても、ちょっとだけ落ちていることがあります。<strong>一度OFFにしてもう一度入れ直し</strong>をしてみてください。<br>
電球が切れた時は、新しい電球を買って交換してください。お部屋の中の<strong>電球は消耗品なので、入居者様負担</strong>での交換になります。ブレーカーも電球も問題ない時は照明器具本体の故障が考えられますので、ご連絡ください。</p>
<h3>ガスコンロが点かなくなった。</h3>
<p class="anthor"> 使用中に<strong>吹きこぼし</strong>たりしませんでしたか？吹きこぼした場合、ガスの噴出口にこぼしたものが付着してしまったり、湿ってしまったりすることによって着火しなくなることがあります。付着物はきれいに取り除いてください。湿ってしまったものは、乾燥させればつくようになります(自然乾燥の場合、2〜3日かかることもございます)。<br>
吹きこぼし等もなくつかなくなった場合、電池切れが考えられますので、<strong>電池交換</strong>をしてください。つまみの側やコンロ下に電池がついていることが多いので、確認してください。</p>
<h3>エアコンがつかなくなった。</h3>
<p class="anthor"> 電源は入っていますか？　電気がつかない時と同様に<strong>ブレーカーの入れ直し</strong>をしてみてください。また、<strong>リモコンの電池交換</strong>をしてみてください。<br>
どちらをやってもつかない時はお問い合わせください。</p>
<h3>エアコンをつけるとポコポコという異音がする。</h3>
<p class="anthor"> 機密性の高いマンションや新築物件の場合、このようなことがよく起こります。部屋の外と中との気圧に差が出ている時に起こる現象です。お部屋の中にある通風孔を開けたり、窓を細めに開けたりするなど<strong>空気の通り道を作って</strong>あげてください。<br>
「換気扇を回した時にエアコンから音がする」というのも同じ現象です。</p>
<h3>エアコンをつけると水漏れがする。</h3>
<p class="anthor"> エアコンの掃除を怠ると、ほこりがたまったり汚れが付着したりして、水漏れの原因となります。<strong>こまめな掃除</strong>を心がけてください。<br>
水漏れしてしまったものについては修理が必要ですので、ご連絡ください。</p>
<h3>携帯番号や勤務先が変わったんだけど…。</h3>
<p class="anthor"> お申込書に記載していただいた情報から変更があった際は、当社までご連絡ください。お客様からいただいた個人情報は、個人情報保護法に基づき厳重に保管いたします。</p>
<h3>隣の部屋がうるさいんだけど…。</h3>
<p class="anthor"> うるさい時間帯やどのような騒音か(話し声・音楽・足音など)を教えてください。こちらから注意させていただきます。<br>
マンションは意外と音が響くものです。夜、窓を開けたままで話したり音楽を聴いたりする時は特に注意をしましょう。</p>
<h3>トイレが詰まった！！</h3>
<p class="anthor"> トイレットペーパーを大量に流したり、お掃除シートを複数枚流したりしていませんか？特にお掃除シートは厚手のため、一度に何枚も流すと詰まる原因になります。もし詰まってしまった場合はお近くのホームセンターなどで『スッポン(トイレの詰まりを直す吸引器具)』を買って直してください。それでも直らない場合は、業者手配となりますのでご連絡ください。<strong>費用は入居者様負担</strong>となります。</p>
<h3>トイレの水が止まらない！！</h3>
<p class="anthor"> トイレタンクの中に固形の洗浄剤などを入れていませんか？トイレにはフロートバルブという水を流したり止めたりするフタのようなものがついています。まれにフロートバルブに洗浄剤がはさまってしまい、フタがしっかり閉まらず流れたままになってしまうことがありますので、確認してみてください。また、フロートバルブの鎖がタンク内で引っかかって流れたままになってしまうこともありますので、ご確認ください。<br>
どちらにも該当しない時はご連絡ください。</p>
<h3>2年後もこのまま住みたい。</h3>
<p class="anthor"> 当社の契約は2年契約となっています。2年後もそのままお住まいになりたい時は<strong>『更新契約』</strong>を行い、引き続き住んでいただくことができます。契約満了の2〜3ヶ月前に更新するかどうかのご確認のご連絡をいたしますので、ご検討後お返事ください。<br>
引き続きお住まいになる場合は、<strong>更新料（新賃料の1ヶ月分）をお支払い</strong>いただき、更新契約書を取り交わします。更新の際は、火災保険の更新手続きが（保証会社加入の場合は<strong>保証契約の更新</strong>手続きも）必要となります。</p>
<h3>部屋を退出しようと思っているんだけど…。</h3>
<p class="anthor"> お部屋を解約する場合はご一報ください。契約書に記載させていただいております解約予告の期間をもって、解約となります。月半ば等の解約の場合、<strong>家賃は日割り計算いたします</strong>ので、日割り分の賃料をお支払いください。<br>
お部屋の明け渡し立会いを行いますので、正式なお引っ越し日が決まりましたら改めてご連絡ください。</p>
<h3>天井から水漏れしてきた！！</h3>
<p class="anthor"> 上のお部屋からの水漏れ事故や、水道管からの水漏れが考えられます。至急ご連絡ください。水漏れ原因究明のため、お客様お立会いのもと<strong>お部屋に立ち入らせていただくことがございます</strong>ので、あらかじめご了承ください。</p>
<h3>海外旅行・出張などで留守にする。</h3>
<p class="anthor"> 海外旅行や出張などで部屋を長期にわたって不在にする時は、当社までご連絡をお願いいたします。万が一何かあった時（水漏れ事故や火災など）にご連絡が取れないと騒ぎになってしまうためです。<br>
また、<strong>不在の期間に家賃の支払い期限が含まれる場合は、前もって家賃をご入金</strong>するようにしてください。<br>
お出かけ前には電気・水道・ガスなどを今一度確認して出発しましょう。</p>
<h3>窓ガラスにひびが入った！</h3>
<p class="anthor"> ガラスの内部に<strong>ワイヤーが入っているタイプの窓ガラスの場合</strong>、ものをぶつけたわけではなくても窓ガラスにひびが入る<strong>『熱割れ』</strong>という現象が起こることがあります。そのような場合はご一報ください。<br>
窓が隠れるような位置にベッドや家具を置くと『熱割れ』が起こりやすくなるので、家具の配置を変えるなど、ご対応をお願いいたします。</p>
<h3>毎月振込の手続きをするのが面倒だ。</h3>
<p class="anthor"> 忙しかったり、うっかり忘れたり…毎月の振込に金融機関に行くのが面倒だという方は、自動引落しのお手続きをお取りください。金融機関の窓口で自動引落しのお申し込みをしていただければ、その後の手間を省くことができます。なお、<strong>手続き完了までに多少お時間がかかります</strong>ので、完了までは引き続きお振込ください。<br>
残高不足だと引落しができませんのでご注意ください。また、お部屋を解約される時は自動引落しも解約してください。</p>
<h3>泥棒に入られた！！</h3>
<p class="anthor"> 不幸にも泥棒に入られてしまった時は、まず110番通報をしてから、当社までご連絡ください。<br>
警察から<strong>事故ナンバー</strong>をもらい、契約時に加入していただいた火災(家財)保険会社に事故ナンバーを報告すると、被害に応じて<strong>保険料が支払われます</strong>。窓ガラスや鍵を壊された場合も、保険で対応することができます。万が一のためにも必ず保険に加入しておきましょう。</p>
<p class="page-up"><a href="#pTop">▲ページのトップに戻る</a></p>
<p align="center" id="fBnr"><img src="../common_img/fBnr.gif" alt="東京都心部の賃貸マンション検索サイト 株式会社グッドコム賃貸物件検索サイトお電話でのお問い合わせは　TEL 03-5338-0160" name="id_fBnr" border="0" usemap="#map_fBnr" id="id_fBnr"></p>
<map name="map_fBnr">
<area shape="rect" coords="475,64,631,95" href="https://ssl.goodcomasset.co.jp/chintai/contact/" alt="お問い合わせ・資料請求" onMouseOver="MM_swapImage('id_fBnr','','../common_img/fBnr_r.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
<!-- contents end -->
</div>
<div class="clear"></div>

<ul id="fNav">
<li class="nav_01"><a href="../company"><img src="../common_img/fNav_01.gif" alt="会社概要" name="id_fNav_01" border="0" id="id_fNav_01" onMouseOver="MM_swapImage('id_fNav_01','','../common_img/fNav_01_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_02"><a href="../privacy"><img src="../common_img/fNav_02.gif" alt="プライバシーポリシー" name="id_fNav_02" border="0" id="id_fNav_02" onMouseOver="MM_swapImage('id_fNav_02','','../common_img/fNav_02_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></l
i><li class="nav_03"><a href="../sitemap"><img src="../common_img/fNav_03.gif" alt="サイトマップ" name="id_fNav_03" border="0" id="id_fNav_03" onMouseOver="MM_swapImage('id_fNav_03','','../common_img/fNav_03_r.gif',1)" onMouseOut="MM_swapImgRestore()"></a></li>
</ul>

<!--　container end　--></div>

<!--　wrapper end　--></div></div></div>

<div id="footer">
<div id="adobereader">
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank"><img src="../common_img/Get_ADOBE_READER_sml.jpg" alt="Get ADOBE READER" width="110" height="28" border="0"></a><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&Lang=Japanese" target="_blank"></a></p>
<p><a href="http://www.adobe.com/jp/products/acrobat/readstep2.html?promoid=BPBQN" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>
こちらからダウンロードしてください。</a></p>
</div>
<div class="clear"></div>
<!--　footer end　--></div>




</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
