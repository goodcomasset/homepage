<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=euc-jp">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<meta name="description" content="株式会社グッドコムアセットは、分譲マンションの販売を通じて、お客様と一生涯のお付き合いを築きます。不動産の企画、開発、分譲、並びに建物管理に関するお問い合せは、弊社までお気軽にご相談ください。">
<meta name="keywords" content="グッドコムアセット,goodcomasset,東京,23区,賃貸マンション,ワンルームマンション,経営,不動産投資,マンション投資,マンション経営,資産運用,投資用マンション">
<meta name="robots" content="index,follow">
<title>お知らせ　全表示｜グッドコムアセット（goodcomasset）｜東京23区の賃貸マンション、ワンルームマンションの経営、不動産投資、マンション投資、マンション経営</title>
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all">
<script src="../js/dw_common.js" type="text/javascript"></script><!--Dreamweaver生成のrollover等-->
<script src="../js/judge.js" type="text/javascript"></script><!--MacIE5用alert-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22983299-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body onLoad="MM_preloadImages('../common_img/navi1_on.gif','../common_img/navi2_on.gif','../common_img/navi3_on.gif','../common_img/navi4_on.gif','../common_img/navi5_on.gif','../common_img/navi6_on.gif','../common_img/menu1_on.gif','../common_img/menu2_on.gif','../common_img/menu3_on.gif','../common_img/menu4_on.gif')">
<div id="wrapper">
<div id="seo"><h1>株式会社グッドコムアセット：マンションの企画、開発、分譲、建物管理</h1><br class="clear"></div>

<div id="header">
  <span class="left2">
    <h2><a href="/"><img src="/common_img/new_logo.jpg" width="343" height="100" alt="不動産投資・マンション経営　株式会社グッドコムアセット"></a></h2>
  </span>
  <span class="right2">
    <p><img src="/common_img/new_contact.jpg" width="292" height="65" alt="フリーダイヤル　0800-919-9156　平日10時〜20時"></p>
    <div id="h_gnavi2">
      <ul>
        <li class="s_btn2"><a href="/" title="HOME" onMouseOver="MM_swapImage('header1','','/common_img/new_headnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_01.jpg" alt="HOME" width="85" height="17" id="header1"></a></li>
        <li class="s_btn2"><a href="/sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('header2','','/common_img/new_headnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_02.jpg" alt="サイトマップ" width="85" height="17" id="header2"></a></li>
        <li class="s_btn2"><a href="/news/" title="お知らせ" onMouseOver="MM_swapImage('header3','','/common_img/new_headnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_03.jpg" alt="お知らせ" width="85" height="17" id="header3"></a></li>
        <li class="s_btn3"><a href="https://ssl.goodcomasset.co.jp/contact/" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('header4','','/common_img/new_headnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/common_img/new_headnavi_04.jpg" alt="お問い合わせ・資料請求" width="135" height="17" id="header4"></a></li>
      </ul>
    </div>
    <div class="clear"></div><!--ヘッダーナビ終了-->
    <p><img src="/common_img/space.jpg" width="292" height="13" alt=""></p>
  </span>
  <div class="clear"></div>
</div><!--ヘッダー終了-->
	
	<div id="gnavi">
		<ul>
			<li><a href="/about/jigyo.html" title="今。私たちにできること" onMouseOver="MM_swapImage('gnavi1','','/common_img/new_gnavi_01on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_01.jpg" alt="今。私たちにできること" width="185" height="44" id="gnavi1"></a></li>
			<li><a href="/about/index.html" title="グッドコムアセットとは　About GOOD COM ASSET" onMouseOver="MM_swapImage('gnavi2','','/common_img/new_gnavi_02on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_02.jpg" alt="グッドコムアセットとは　About GOOD COM ASSET" width="165" height="44" id="gnavi2"></a></li>
			<li><a href="/future/index.html" title="将来に不安を抱えている皆様へ　Anxiety about the future" onMouseOver="MM_swapImage('gnavi3','','/common_img/new_gnavi_03on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_03.jpg" alt="将来に不安を抱えている皆様へ　Anxiety about the future" width="219" height="44" id="gnavi3"></a></li>
			<li><a href="/adviser/" title="アドバイザー紹介　Adviser" onMouseOver="MM_swapImage('gnavi4','','/common_img/new_gnavi_04on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_04.jpg" alt="アドバイザー紹介　Adviser" width="136" height="44" id="gnavi4"></a></li>
			<li><a href="/success/index.html" title="成功体験談　Success story" onMouseOver="MM_swapImage('gnavi5','','/common_img/new_gnavi_05on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_05.jpg" alt="成功体験談　Success story" width="101" height="44" id="gnavi5"></a></li>
			<li><a href="/about/recruit.php" title="求人情報　Recruit" onMouseOver="MM_swapImage('gnavi6','','/common_img/new_gnavi_06on.jpg',1)" onMouseOut="MM_swapImgRestore()" ><img src="/common_img/new_gnavi_06.jpg" alt="求人情報　Recruit" width="93" height="44" id="gnavi6"></a></li>
		</ul>
		<div class="clear"></div><!--float.hack box-->
	</div>

	<div id="main">
		
		<div id="leftside"><!--左カラムここから-->

			<p><img src="../common_img/left_waku01.jpg" width="225" height="7" alt=""></p>
			<p><img src="image/news_left_title01.jpg" width="215" height="33" alt="お知らせ　What's new" class="mt2"></p>
			<div id="contents_navi">
				<ul>
					<li><a href="../news/" title="全て見る" onMouseOver="MM_swapImage('leftnavi1','','image/news_btn04_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/news_btn04_on.jpg" alt="全て見る" name="leftnavi1" width="215" height="37" border="0" id="leftnavi1"></a></li>
					<li><a href="../news/?ca=2" title="お知らせ" onMouseOver="MM_swapImage('leftnavi2','','image/news_btn05_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/news_btn05.jpg" alt="お知らせ" name="leftnavi2" width="215" height="33" border="0" id="leftnavi2"></a></li>
					<li><a href="../news/?ca=3" title="事業・物件情報" onMouseOver="MM_swapImage('leftnavi3','','image/news_btn06_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/news_btn06.jpg" alt="事業・物件情報" name="leftnavi3" width="215" height="36" border="0" id="leftnavi3"></a></li>
				</ul>
			</div>		
			<p><img src="../about/image/about_left_waku01.jpg" width="215" height="5" alt=""></p>

			<div id="leftmenu">
				<ul class="mt10">
					<li><a href="../property/?ca=2" title="分譲実績　Results" onMouseOver="MM_swapImage('leftmenu2','','../common_img/menu2_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/menu2.gif" alt="分譲実績　Results" width="215" height="48" border="0" id="leftmenu2"></a></li>
				</ul>
			</div><!--左メニュー終了-->

			<div id="banner">
				<ul>
					<li><a href="http://www.team-6.jp/" title="みんなで止めよう温暖化　チームマイナス6%" onMouseOver="MM_swapImage('banner3','','../common_img/menu5_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu5.gif" alt="みんなで止めよう温暖化　チームマイナス6%" width="215" height="59" id="banner3"></a></li>
					<li><a href="http://ecocap007.com/" title="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" onMouseOver="MM_swapImage('banner4','','../common_img/menu6_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu6.gif" alt="NPO法人（内閣府認証）エコキャップ推進協会　ECOCAP" width="215" height="59" id="banner4"></a></li>
				</ul>
			</div><!--左バナー終了-->

			<div id="left_info"> 
		 <!--<p><a href="#" title="グッドコムアセットの賃貸物件情報サイト" onMouseOver="MM_swapImage('l_info1','','../common_img/menu7_on.gif',1)" onMouseOut="MM_swapImgRestore()" target="_blank"><img src="../common_img/menu7.gif" alt="グッドコムアセットの賃貸物件情報サイト" width="195" height="49" id="l_info1"></a></p>-->
   </div>
			<p><img src="../common_img/left_waku02.jpg" width="225" height="7" alt=""></p>
		
		</div><!--左カラム終了-->
		
		<div id="content"><!--メインコンテンツ-->
			
			<div id="news">
			
			<h3><img src="image/news_title03.jpg" width="655" height="50" alt="お知らせ What's new 全表示"></h3>
		 <p class="pan"><a href="../">トップページ</a> > お知らせ　全表示</p>
			
			<div id="tab">
			<ul>
			<li><a href="../news/" title="全て見る" onMouseOver="MM_swapImage('tab1','','image/news_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/news_btn01.jpg" alt="全て見る" width="171" height="27" border="0" id="tab1"></a></li>
			<li><a href="../news/?ca=2" title="お知らせ" onMouseOver="MM_swapImage('tab2','','image/news_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/news_btn02.jpg" alt="お知らせ" name="tab2" width="171" height="27" border="0" id="tab2"></a></li>
			<li><a href="../news/?ca=3" title="事業・物件情報" onMouseOver="MM_swapImage('tab3','','image/news_btn03_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="image/news_btn03.jpg" alt="事業・物件情報" width="171" height="27" border="0" id="tab3"></a></li>
			</ul>
			<div class="clear"></div>
			</div>
			<p><img src="image/news_ill01.jpg" width="655" height="5" alt=""></p>
			<div id="news_waku">
<?php 
if(!count($fetch)){
	echo "<center><br>ただいま準備中のため、もうしばらくお待ちください。<br><br></center>\n";//表示件数が０件の場合
}else{
?>
<?php 
				for($i=0;$i<count($fetch);$i++){
					if($i != 0){echo "<a name=\"".$fetch[$i]['RES_ID']."\"></a>";}
					
					$img = 'up_img/'.$fetch[$i]['RES_ID'].'.jpg';
					
					$pdf_file = 'up_img/'.$fetch[$i]['RES_ID'].".".$fetch[$i]['EXTENTION'];
?>
			<div class="line">
			<table width="625" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td width="140"><strong><?php echo $fetch[$i]['Y'].'年'.str_pad(($fetch[$i]['M']),2,"0",STR_PAD_LEFT).'月'.str_pad(($fetch[$i]['D']),2,"0",STR_PAD_LEFT).'日';?></strong></td>
				<td width="485"><img src="image/news_ico0<?php echo $fetch[$i]['CATEGORY_CODE'];?>.jpg" width="60" height="12"></td>
				</tr>
				<tr>
				<td colspan="2"><span class="ico"><?php if(file_exists($pdf_file)){?><a href="<?php echo $pdf_file;?>" target="_blank"><img src="image/news_ico01.jpg" width="25" height="22"></a><?php }?></span><span class="title"><?php if($fetch[$i]['URL']){?><a href="<?php echo $fetch[$i]['URL'];?>"<?php echo ($fetch[$i]['TARGET_FLG'])?" target=\"_blank\"":"";?>><?php }?><?php echo nl2br($fetch[$i]['TITLE']);?><?php if($fetch[$i]['URL']){?></a><?php }?></span><div class="clear"></div></td>
				</tr>
			</table>
			<table width="625" border="0" cellspacing="0" cellpadding="0" class="mt10">
				<tr>
					<?php if(file_exists($img)){?>
					<td width="290"><img src="<?php echo $img;?>?r=<?php echo rand();?>" width="280" height="210" class="photo"></td>
					<?php }?>
					<td><?php echo nl2br($fetch[$i]['CONTENT']);?></td>
				</tr>
			</table>
			</div>
<?php 
	}
}
?>
			</div>
			<p><img src="image/news_ill01.jpg" width="655" height="5" alt=""></p>

			<table width="655" border="0" cellspacing="0" cellpadding="0" class="adobe">
				<tr>
					<td width="125"><a href="http://get.adobe.com/jp/reader/" target="_blank"><img src="image/pdf_banner.jpg" alt="Get ADOBE READER" width="112" height="33" border="0"></a></td>
			  <td width="530"><a href="http://get.adobe.com/jp/reader/" target="_blank">PDFファイルをご覧いただくためには、最新のAdobe Readerが必要です。<br>こちらからダウンロードしてください(無料)。</a></td>
				</tr>
			</table>

			<div id="f_banner"><a href="https://ssl.goodcomasset.co.jp/contact" title="お問い合わせ・資料請求" onMouseOver="MM_swapImage('f_banner1','','../common_img/header_btn03_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/header_btn03.gif" alt="お問い合わせ・資料請求" width="156" height="22" id="f_banner1"></a></div>

			<p class="page-up"><a href="#wrapper" title="▲ページTOPへ" onMouseOver="MM_swapImage('page1','','../common_img/page_top_on.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/page_top.gif" alt="▲ページTOPへ" width="100" height="24" id="page1"></a></p>
			
			</div><!--下層ページ・コンテンツ終了-->
			
		</div><!--コンテンツ終了-->
		
		<div class="clear"></div><!--float.hack box-->

		<div id="f_gnavi">
		<ul>
		<li><img src="../common_img/footer_parts01.jpg" width="648" height="36" alt="" id="footer1"></li>
		<li><a href="../policy/index.html" title="プライバシーポリシー" onMouseOver="MM_swapImage('footer2','','../common_img/footer_btn01_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn01.jpg" alt="プライバシーポリシー" width="148" height="36" id="footer2"></a></li>
		<li><a href="../sitemap/index.html" title="サイトマップ" onMouseOver="MM_swapImage('footer3','','../common_img/footer_btn02_on.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="../common_img/footer_btn02.jpg" alt="サイトマップ" width="103" height="36" id="footer3"></a></li>
		</ul>
		</div>
		<div class="clear"></div><!--フッターナビ終了-->
		
	</div><!--コンテンツラッパー終了-->
	
</div><!--ラッパー終了-->

<div id="footer">
<p><img src="../common_img/footer_parts02.jpg" width="960" height="29" alt=""></p>
<table cellpadding="0" cellspacing="0" summary="フッターテーブル">
	<tr>
		<td><div id="ai"></div></td>
	</tr>
</table>
</div><!--フッター終了-->
</body>
<script language="JavaScript" type="text/javascript">
<!--
document.write('<img src="../log.php?referrer='+escape(document.referrer)+'" width="1" height="1">');
//-->
</script>
</html>
