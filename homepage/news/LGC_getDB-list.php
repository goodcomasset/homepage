<?php
/*******************************************************************************
SiteWiN20 20 30（MySQL版）N3_2
新着情報の内容をFlashに出力するプログラム

LGC：DBより新着情報の情報を取得


*******************************************************************************/
// 不正アクセスチェック
if(!$injustice_access_chk){
	header("HTTP/1.0 404 Not Found");exit();
}

if($_GET["ca"] == 2){
	$category_code = "
	AND
		(CATEGORY_CODE = '2')
	";
}elseif($_GET["ca"] == 3){
	$category_code = "
	AND
		((CATEGORY_CODE = '3') || (CATEGORY_CODE = '4'))
	";
}else{
	$category_code = "";
}

$sql = "
SELECT
	RES_ID,
	CATEGORY_CODE,
	TITLE,
	CONTENT,
	TYPE,
	SIZE,
	EXTENTION,
	URL,
	TARGET_FLG,
	YEAR(DISP_DATE) AS Y,
	MONTH(DISP_DATE) AS M,
	DAYOFMONTH(DISP_DATE) AS D,
	DISPLAY_FLG
FROM
	N3_2WHATSNEW
WHERE
	(DISPLAY_FLG = '1')
	".$category_code."
AND
	(CATEGORY_CODE != '5')
AND
	(DEL_FLG = '0')
ORDER BY
	DISP_DATE DESC,
	CATEGORY_CODE ASC
LIMIT
	0 , ".N3_2DISP_MAXROW."
";


// ＳＱＬを実行
$fetch = dbOpe::fetch($sql,DB_USER,DB_PASS,DB_NAME,DB_SERVER);

?>