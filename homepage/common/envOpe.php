<?php
/***********************************************************************************************************
 環境変数操作クラスライブラリ
 	※環境変数情報の判別、取得、ファイル書き出し等を行う

 2004/8/11 Yossee
***********************************************************************************************************/

############################################################################################################
#
# ユーザーエージェント情報操作クラス	UA_Info("HTTP_USER_AGENTの情報");
#	※パラメーター省略可。省略した場合は$_SERVER['HTTP_USER_AGENT']を設定して情報を取得する
#
############################################################################################################
class UA_Info{


var $ua;	# HTTP_USER_AGENT

// コンストラクタ
function UA_Info($ev = ""){
	$this->ua = (empty($ev))?$_SERVER['HTTP_USER_AGENT']:$ev;
}

#------------------------------------------------------------------------------------------------
# 使用ＯＳ／ブラウザ判定メソッド
#
#	メソッド：getNavInfo()			
#	引数：	なし（コンストラクタで設定した情報を使用）
#	戻り値：結果情報を配列で返す
#	
#	※戻り値の内容（番号は要素番号）
#		0:主要モダンブラウザの判定(bool) ※WinIE5〜6 MacIE5／NN6〜7／Gecko系／Opera6〜7／Safari
#		1:総合判定したUA情報(String)
#		2:ＯＳ名(String)
#		3:ＯＳのバージョン(String)
#		4:ブラウザ名(String)
#		5:ブラウザのバージョン(String)
#		6:文字列処理してUAを格納
#
#------------------------------------------------------------------------------------------------
function getNavInfo(){

	///////////////////////////////////////////
	// 結果格納の配列を初期化
	$result[0] = false;
	$result[1] = "";
	$result[2] = "";
	$result[3] = "";
	$result[4] = "";
	$result[5] = "";
	$result[6] = "";

	///////////////////////////////////////////
	// 文字列処理（危険文字回避）の匿名関数
	$strSyori = create_function('$str','strip_tags(&$str);htmlspecialchars(&$str);'.str_replace(array("\t","/etc/passwd","sendmail","\\","|"),"",&$str).';return $str;');


	///////////////////////////////////////////
	// Windows95
	if(stristr($this->ua,"MSIE 4.0")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 95")&&!stristr($this->ua,"Opera")){
		$result[1] = "Windows95 IE4.0";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Internet Explorer";
		$result[5] = "4.0";
	}
	elseif(stristr($this->ua,"MSIE 5.0")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 95")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows95 IE5.0";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Internet Explorer";
		$result[5] = "5.0";
	}
	elseif(stristr($this->ua,"MSIE 5.5;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 95")&& !stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows95 IE5.5";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Internet Explorer";
		$result[5] = "5.5";
	}
	elseif(stristr($this->ua,"MSIE 6.0;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 95")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows95 IE6.0";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Internet Explorer";
		$result[5] = "6.0";
	}
	elseif(stristr($this->ua,"Netscape6")&&stristr($this->ua,"Win95")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows95 NN6.x";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&&stristr($this->ua,"Win95")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows95 NN7.x";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/")&&stristr($this->ua,"Win95")&&!stristr($this->ua,"Netscape/7")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows95 Mozilla.org";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif((stristr($this->ua,"Opera 6")||stristr($this->ua,"Opera/6"))&&stristr($this->ua,"Windows 95")){
		$result[0] = true;
		$result[1] = "Windows95 Opera6.x";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Opera";
		$result[5] = "6.x";
	}
	elseif((stristr($this->ua,"Opera 7")||stristr($this->ua,"Opera/7"))&&stristr($this->ua,"Windows 95")){
		$result[0] = true;
		$result[1] = "Windows95 Opera7.x";
		$result[2] = "Windows";
		$result[3] = "95";
		$result[4] = "Opera";
		$result[5] = "7.x";
	}


	///////////////////////////////////////////
	// Windows98
	elseif(stristr($this->ua,"MSIE 4.0")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[1] = "Windows98 IE4.0";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Internet Explorer";
		$result[5] = "4.0";
	}
	elseif(stristr($this->ua,"MSIE 5.0")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows98 IE5.0";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Internet Explorer";
		$result[5] = "5.0";
	}
	elseif(stristr($this->ua,"MSIE 5.5;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows98 IE5.5";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Internet Explorer";
		$result[5] = "5.5";
	}
	elseif(stristr($this->ua,"MSIE 6.0;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows98 IE6.0";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Internet Explorer";
		$result[5] = "6.0";
	}
	elseif(!stristr($this->ua,"compatible;")&&stristr($this->ua,"Mozilla/4.")&&stristr($this->ua,"Win98")&&!stristr($this->ua,"Opera")){
		$result[1] = "Windows98 NN4.x";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Netscape";
		$result[5] = "4.x";
	}
	elseif(stristr($this->ua,"Netscape6")&&stristr($this->ua,"Win98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows98 NN6.x";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&&stristr($this->ua,"Win98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows98 NN7.x";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/")&&stristr($this->ua,"Win98")&&!stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Netscape/7")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows98 Mozilla.org";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif((stristr($this->ua,"Opera 6")||stristr($this->ua,"Opera/6"))&&stristr($this->ua,"Windows 98")&&!stristr($this->ua,"Win 9x")){
		$result[0] = true;
		$result[1] = "Windows98 Opera6.x";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Opera";
		$result[5] = "6.x";
	}
	elseif((stristr($this->ua,"Opera 7")||stristr($this->ua,"Opera/7"))&&stristr($this->ua,"Windows 98")&&!stristr($this->ua,"Win 9x")){
		$result[0] = true;
		$result[1] = "Windows98 Opera7.x";
		$result[2] = "Windows";
		$result[3] = "98";
		$result[4] = "Opera";
		$result[5] = "7.x";
	}


	///////////////////////////////////////////
	// WindowsMe
	elseif(stristr($this->ua,"MSIE 5.0;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98; Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsMe IE5.0";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Internet Explorer";
		$result[5] = "5.0";
	}
	elseif(stristr($this->ua,"MSIE 5.5;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98; Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsMe IE5.5";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Internet Explorer";
		$result[5] = "5.5";
	}
	elseif(stristr($this->ua,"MSIE 6.0;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows 98; Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsMe IE6.0";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Internet Explorer";
		$result[5] = "6.0";
	}
	elseif(!stristr($this->ua,"compatible;")&&stristr($this->ua,"Mozilla/4.")&&stristr($this->ua,"Win95")&&!stristr($this->ua,"Opera")){
		$result[1] = "WindowsMe or 95 NN4.x";
		$result[2] = "Windows";
		$result[3] = "Me or 95";
		$result[4] = "Netscape";
		$result[5] = "4.x";
	}
	elseif(stristr($this->ua,"Netscape6")&&stristr($this->ua,"Win 9x")&& !stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsMe NN6.x";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&&stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsMe NN7.x";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/")&&stristr($this->ua,"Win 9x")&&!stristr($this->ua,"Netscape/7")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsMe Mozilla.org";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif((stristr($this->ua,"Opera 6")||stristr($this->ua,"Opera/6"))&&stristr($this->ua,"Windows ME")){
		$result[0] = true;
		$result[1] = "WindowsMe Opera6.x";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Opera";
		$result[5] = "6.x";
	}
	elseif((stristr($this->ua,"Opera 7")||stristr($this->ua,"Opera/7"))&&stristr($this->ua,"Windows ME")){
		$result[0] = true;
		$result[1] = "WindowsMe Opera7.x";
		$result[2] = "Windows";
		$result[3] = "Me";
		$result[4] = "Opera";
		$result[5] = "7.x";
	}


	///////////////////////////////////////////	
	// Windows 2000 
	elseif(stristr($this->ua,"MSIE 5.0")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows NT 5.0")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows2000 IE5.0";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Internet Explorer";
		$result[5] = "5.0";
	}
	elseif(stristr($this->ua,"MSIE 5.5;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows NT 5.0")&& !stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows2000 IE5.5";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Internet Explorer";
		$result[5] = "5.5";
	}
	elseif(stristr($this->ua,"MSIE 6.0;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows NT 5.0")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows2000 IE6.0";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Internet Explorer";
		$result[5] = "6.0";
	}
	elseif(!stristr($this->ua,"compatible;")&&stristr($this->ua,"Mozilla/4.")&&(stristr($this->ua,"WinNT;")||stristr($this->ua,"Windows NT 5.0"))&&!stristr($this->ua,"Opera")){
		$result[1] = "Windows2000 or XP NN4.x";
		$result[2] = "Windows";
		$result[3] = "2000 or XP";
		$result[4] = "Netscape";
		$result[5] = "4.x";
	}
	elseif(stristr($this->ua,"Netscape6")&&stristr($this->ua,"NT 5.0")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows2000 NN6.x";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&&stristr($this->ua,"NT 5.0")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows2000 NN7.x";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/")&&stristr($this->ua,"NT 5.0")&&!stristr($this->ua,"Netscape/7")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "Windows2000 Mozilla.org";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif(stristr($this->ua,"Opera 6")||stristr($this->ua,"Opera/6")&&stristr($this->ua,"Windows 2000")){
		$result[0] = true;
		$result[1] = "Windows2000 Opera6.x";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Opera";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Opera 7")||stristr($this->ua,"Opera/7")&&stristr($this->ua,"NT 5.0")){
		$result[0] = true;
		$result[1] = "Windows2000 Opera7.x";
		$result[2] = "Windows";
		$result[3] = "2000";
		$result[4] = "Opera";
		$result[5] = "7.x";
	}


	///////////////////////////////////////////	
	// Windows XP
	elseif(stristr($this->ua,"MSIE 5.0")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows NT 5.1")&&stristr($this->ua,"Windows NT 5.1")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsXP IE5.0";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Internet Explorer";
		$result[5] = "5.0";
	}
	elseif(stristr($this->ua,"MSIE 5.5;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows NT 5.1")&& !stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsXP IE5.5";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Internet Explorer";
		$result[5] = "5.5";
	}
	elseif(stristr($this->ua,"MSIE 6.0;")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Windows NT 5.1")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsXP IE6.0";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Internet Explorer";
		$result[5] = "6.0";
	}
	elseif(stristr($this->ua,"Netscape6")&&stristr($this->ua,"NT 5.1")&& !stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsXP NN6.x";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&&stristr($this->ua,"NT 5.1")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsXP NN7.x";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/")&&stristr($this->ua,"NT 5.1")&&!stristr($this->ua,"Netscape/7")&&!stristr($this->ua,"Opera")){
		$result[0] = true;
		$result[1] = "WindowsXP Mozilla.org";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif((stristr($this->ua,"Opera 6")||stristr($this->ua,"Opera/6"))&&stristr($this->ua,"Windows NT 5.1;")){
		$result[0] = true;
		$result[1] = "WindowsXP Opera6.x";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Opera";
		$result[5] = "6.x";
	}
	elseif((stristr($this->ua,"Opera 7")||stristr($this->ua,"Opera/7"))&&stristr($this->ua,"Windows NT 5.1;")){
		$result[0] = true;
		$result[1] = "WindowsXP Opera7.x";
		$result[2] = "Windows";
		$result[3] = "XP";
		$result[4] = "Opera";
		$result[5] = "7.x";
	}


	///////////////////////////////////////////	
	// MacClassic
	elseif(stristr($this->ua,"MSIE 4.5")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Mac_PowerPC")){
		$result[1] = "Macintosh PPC MSIE 4.5";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Internet Explorer";
		$result[5] = "4.5";
	}
	elseif(stristr($this->ua,"MSIE 5.")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Mac_PowerPC")){
		$result[0] = true;
		$result[1] = "Macintosh PPC MSIE 5.x";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Internet Explorer";
		$result[5] = "5.x";
	}
	elseif(stristr($this->ua,"Mozilla/4.")&&stristr($this->ua,"PPC")&&stristr($this->ua,"Macintosh")){
		$result[1] = "Macintosh PPC NN4.x";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Netscape";
		$result[5] = "4.x";
	}
	elseif(stristr($this->ua,"Netscape6/")&&stristr($this->ua,"Macintosh")&&!stristr($this->ua,"Mac OS X")){
		$result[0] = true;
		$result[1] = "Macintosh PPC NN6.x";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&&stristr($this->ua,"Macintosh")&&!stristr($this->ua,"Mac OS X")){
		$result[0] = true;
		$result[1] = "Macintosh PPC NN7.x";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/")&&stristr($this->ua,"Macintosh")&&!stristr($this->ua,"Netscape")&&!stristr($this->ua,"Mac OS X")){
		$result[0] = true;
		$result[1] = "Macintosh PPC Mozilla.org";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif(stristr($this->ua,"Opera")&&stristr($this->ua,"Mac_PowerPC")){
		$result[0] = true;
		$result[1] = "Macintosh PPC Opera";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "Opera";
		$result[5] = "Unknown";
	}
	elseif(stristr($this->ua,"iCab")&&stristr($this->ua,"Macintosh")&&stristr($this->ua,"PPC")&&!stristr($this->ua,"Mac OS X")){
		$result[1] = "Macintosh PPC iCab";
		$result[2] = "Macintosh";
		$result[3] = "PPC";
		$result[4] = "iCab";
		$result[5] = "Unknown";
	}


	///////////////////////////////////////////	
	// MacOS X
	elseif(stristr($this->ua,"MSIE 5.2")&&stristr($this->ua,"compatible;")&&stristr($this->ua,"Mac_PowerPC")){
		$result[0] = true;
		$result[1] = "Macintosh OS X MSIE 5.2x";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "Internet Explorer";
		$result[5] = "5.2x";
	}
	elseif(stristr($this->ua,"Netscape6/")&& stristr($this->ua,"Mac OS X")){
		$result[0] = true;
		$result[1] = "Macintosh OS X NN6.x";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "Netscape";
		$result[5] = "6.x";
	}
	elseif(stristr($this->ua,"Netscape/7")&& stristr($this->ua,"Mac OS X")){
		$result[0] = true;
		$result[1] = "Macintosh OS X NN7.x";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "Netscape";
		$result[5] = "7.x";
	}
	elseif(stristr($this->ua,"Gecko/") && stristr($this->ua,"Mac OS X") && !stristr($this->ua,"Netscape")){
		$result[0] = true;
		$result[1] = "Macintosh OS X Mozilla.org";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "Mozilla.org";
		$result[5] = "Unknown";
	}
	elseif(stristr($this->ua,"Opera")&&stristr($this->ua,"Mac OS X")){
		$result[0] = true;
		$result[1] = "Macintosh OS X Opera";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "Opera";
		$result[5] = "Unknown";
	}
	elseif(stristr($this->ua,"Mac OS X")&&stristr($this->ua,"Safari")){
		$result[0] = true;
		$result[1] = "Macintosh OS X Safari";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "Safari";
		$result[5] = "Unknown";
	}
	elseif(stristr($this->ua,"iCab")&&stristr($this->ua,"OS X")&&!stristr($this->ua,"68")){
		$result[1] = "Macintosh OS X iCab";
		$result[2] = "Macintosh";
		$result[3] = "OS X";
		$result[4] = "iCab";
		$result[5] = "Unknown";
	}


	///////////////////////////////////////////////////////	
	// Unix系OS（Linux含む）	※ＵＡを$result[6]に格納
	elseif(stristr($this->ua,"Linux") || stristr($this->ua,"SunOS") || stristr($this->ua,"BSD")){
		$result[1] = "UNIX OS";
		$result[2] = "UNIX OS";
		$result[3] = "Unknown";
		$result[4] = "Unknown";
		$result[5] = "Unknown";
	}


	//////////////////////////////////////////////////////////	
	// インターネット対応ゲーム機等	※ＵＡを$result[6]に格納
	elseif(stristr($this->ua,"Mozilla/3.0 (DreamPassport/3.")){
		$result[1] = "Multimedia Apparatus(DreamPassport)";
		$result[2] = "Multimedia Apparatus(DreamPassport)";
		$result[3] = "Multimedia Apparatus(DreamPassport)";
		$result[4] = "Multimedia Apparatus(DreamPassport)";
		$result[5] = "Unknown";		
	}
	elseif(stristr($this->ua,"Mozilla/3.0")&&(stristr($this->ua,"BrowserInfo")||stristr($this->ua,"Screen=")||stristr($this->ua,"InputMethod=")||stristr($this->ua,"Product="))){
		$result[1] = "Other Multimedia Apparatus";
		$result[2] = "Other Multimedia Apparatus";
		$result[3] = "Other Multimedia Apparatus";
		$result[4] = "Other Multimedia Apparatus";
		$result[5] = "Unknown";			
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	// その他不明	※ＵＡを$result[6]に格納
	else{

		// Windows系その他（タブブラウザ系もしくは環境変数偽造野郎の疑い）
		if(stristr($this->ua,"Sleipnir")||stristr($this->ua,"MSIE")||stristr($this->ua,"Win")||stristr($this->ua,"Cuam")||stristr($this->ua,"compatible;")){	
			$result[1] = "Windows IE";
			$result[2] = "Windows";
			$result[3] = "Unknown";
			$result[4] = "Internet Explorer";
			$result[5] = "Unknown";	
		}
		else{
			// 超極少数または環境変数偽造野郎の疑い。
			$result[1] = "Unknown";
			$result[2] = "Unknown";
			$result[3] = "Unknown";
			$result[4] = "Unknown";
			$result[5] = "Unknown";
		}
	
	}


	///////////////////////////////////////////////////////////////////////////
	// 最後に文字列処理をした元データを、$result[6]に格納して判定結果を返す
	$result[6] = $strSyori($this->ua);
	return $result;


/* メソッド“getNavInfo”の終了 */	
}






} /* クラス“UA_Info”の終了 */

############################################################################################################
#
# 環境変数のファイル出力／調査クラス 	ENV_Info("配列で格納された環境変数");
#	※１部パラメーター省略可。
#
############################################################################################################

class ENV_Info{


var $env;	# ENV

// コンストラクタ
function ENV_Info($ev = array()){

	if(empty($ev)):
		$this->env[0] = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
		$this->env[1] = $_SERVER["HTTP_USER_AGENT"];
		$this->env[2] = $_SERVER["HTTP_REFERER"];
	else:
		foreach($ev as $e)$this->env[] = $e; 
	endif;
	
}

#--------------------------------------------------------------------------------------------------------
# 環境変数を抜き取りファイルに格納するメソッド
#
#	メソッド：fileOutput("ファイルパス",["ファイル名"],["串情報取得の設定"]);
#	引	数：１：必須。２：省略したら年月日がファイル名。３：true = 取得する falseまたは省略 = 取得しない
#	戻り値：なし（環境変数を指定したファイルに出力して終了）
#
#	注1：ファイルパスを設定しないと強制終了。書き込みエラーは、エラー非表示で実行されないので注意する事
#	注2：不正な環境変数対策にRep()を使用しているので、必ず“PDC_function.php”を読み込んでおく事！
#---------------------------------------------------------------------------------------------------------
function fileOutput($path,$file_name = "",$getProxy = false){

	if(!$path)die("ファイルパスが未指定です！ ※fileOutput実行時");

	/// 取得するファイル名（引数２）の処理	※デフォルトはファイル名が年月日
	$getFileName = (empty($file_name))?date("Ymd").".dat":$file_name;

	// ファイルネームを含むパスが完成
	$ualogfile = $path;
	if(!ereg($path,"./$"))$ualogfile .= "/";
	$ualogfile .= $getFileName;

	// 串情報を取得するフラグ（引数３）があったら、串関連の環境変数を全部抜いておく
	$pxy = "";
	if($getProxy):

		if($_ENV['HTTP_VIA'])$pxy .= "HTTP_VIA:".$_ENV['HTTP_VIA']."\t";
		if($_ENV['HTTP_FORWARDED'])$pxy .= "HTTP_FORWARDED:".$_ENV['HTTP_FORWARDED']."\t";
		if($_ENV['HTTP_X_FORWARDED_FOR'])$pxy .= "HTTP_X_FORWARDED_FOR:".$_ENV['HTTP_X_FORWARDED_FOR']."\t";
		if($_ENV['HTTP_CACHE_INFO'])$pxy .= "HTTP_CACHE_INFO:".$_ENV['HTTP_CACHE_INFO']."\t";
		if($_ENV['HTTP_XONNECTION'])$pxy .= "HTTP_XONNECTION:".$_ENV['HTTP_XONNECTION']."\t";
		if($_ENV['HTTP_SP_HOST'])$pxy .= "HTTP_SP_HOST:".$_ENV['HTTP_SP_HOST']."\t";
		if($_ENV['HTTP_FROM'])$pxy .= "HTTP_FROM:".$_ENV['HTTP_FROM']."\t";
		if($_ENV['HTTP_X_LOCKING'])$pxy .= "HTTP_X_LOCKING:".$_ENV['HTTP_X_LOCKING']."\t";
		if($_ENV['HTTP_PROXY_CONNECTION'])$pxy .= "HTTP_PROXY_CONNECTION:".$_ENV['HTTP_PROXY_CONNECTION']."\t";

	endif;

	
	/////////////////////////////////////////////////////////////////////////////
	// 文字列置換とNGワードをチェックし、データを一つにまとめてファイル書き込み

	// NGワード
	$ng_word = array("\t","/etc/passwd","sendmail","\\","|");

	// 文字列置換とNGワードチェック（あったら削除）
	foreach($this->env as $e){
		if(!empty($e)):
			strip_tags(&$e);
			ereg_replace("^[[:space:]]{0,}","",&$e);
			ereg_replace("[[:space:]]{0,}$","",&$e);
			mb_ereg_replace("^(　){0,}","",&$e);
			mb_ereg_replace("(　){0,}$","",&$e);
			trim(&$e);
			htmlspecialchars(&$e);
			if(get_magic_quotes_gpc())stripslashes(&$e);
			str_replace($ng_word,"",&$e);
		endif;
	}
	
	// ひとつにまとめる（先頭に現在の日付）
	$env_data = date("Y.m.d H:i:s")."\t";
	for($i=0;$i<count($this->env);$i++){
		$env_data .= $this->env[$i];
		if($i != (count($this->env)-1))$env_data .= "\t";
	}
	if(!empty($pxy))$env_data .= "\t".$pxy;

	// ファイル書き込み（追記型）
	$FP = @fopen($ualogfile,"a");
	@flock($FP,LOCK_EX);
	@fwrite($FP,$env_data);
	@fwrite($FP,"\n");
	@flock($FP,LOCK_UN);
	@fclose($FP);

}  /* メソッド“fileOutput”の終了 */



} /* クラス“ENV_Info”の終了 */

?>