<?php
/*******************************************************************************
アクセス解析設定ファイル SQLite版

2006.04.19 fujiyama
*******************************************************************************/

// マルチバイト関数用に言語と文字コードを指定する（文字列置換、メール送信等で必須）
mb_language("Japanese");
mb_internal_encoding("EUC-JP");

#===================================================================================
# バージョンの設定(１か2を設定する)
# 設定値 1 : 検索エンジン、検索キーワードの表示なしオプションのためデフォルトにしておく
# 設定値 2 : 検索エンジン、キーワードの表示あり
#===================================================================================
define('VARSION_CONFIG',2);

#=================================================================================
#ログファイルのPV数の上限値を設定
#=================================================================================
define('LOGFILE_SIZE_MAX',100000);

#=================================================================================
#PV数が上限値を越えた際のメール送信の有無（0:送信しない、1:送信する）
#=================================================================================
define('ALERT_MAIL_SEND',1);

#=================================================================================
# DBのファイルパスとDB及びテーブルを自動生成するためのSQL文を設定
#	※ディレクトリ名は必要に応じて記述し直す事！（DB名はこのままでＯＫ）
#	※DBを置くディレクトリ“db”のパーミッションは“777”にすること！
#=================================================================================

$date_name = date('Y_m');

//zeeksdgが含まれていたら、デモ用
if(strpos($_SERVER['PHP_SELF'],"zeeksdg")==""){
	$db_path = "/db/";					//本番用
}else{
	//$db_path = "/zeeksdg/ /db/";	//デモ用
	//配列で区切る
	$auto_db_path = explode("/", $_SERVER['PHP_SELF']);
	$db_path = "/".$auto_db_path[1]."/".$auto_db_path[2]."/db/";
}

define('ACCESS_PATH',$_SERVER['DOCUMENT_ROOT'].$db_path);
define('DB_FILEPATH',ACCESS_PATH.$date_name."_access_log_db");
define('CREATE_SQL',"CREATE TABLE ACCESS_LOG(ID INTEGER PRIMARY KEY,REMOTE_ADDR,USER_AGENT,REFERER,QUERY_STRING,ENGINE,OS,BROWSER,PAGE_URL,UNIQUE_FLG,USER_FLG,INS_DATE,TIME,DEL_FLG DEFAULT 0);");


#==================================================================================
# 検索エンジン及び検索キーワードを取得する関数
# listファイルにあるテキストファイルの読み込み
#==================================================================================

function setting_read($uri){
	if(file_exists($uri)){
		if($arr_exclude = @file($uri)){
			$arr_exclude = @array_unique($arr_exclude);
			foreach($arr_exclude as $k => $v){
				$arr_exclude[$k] = trim($v);
			}
			return $arr_exclude;
		}
	}
}


//検索キーワード取得
function get_keyword($query ,$query_key){
	global $google_cache;
	
	$keyword = "";
	foreach(explode("&", $query) as $tmp){
		unset($k,$v);
		list($k,$v) = explode("=", $tmp);
		$k = eregi_replace('amp;', '', $k);
		if($k == $query_key){
			if(trim($v) == "") continue;
			$v = urldecode($v);
			if(function_exists('mb_convert_encoding')){
				$v = @mb_convert_encoding($v, "EUC", "auto");
			}else{
				$v = jcode_convert_encoding($v,'euc-jp');
			}
			$v = str_replace('+', ' ', $v);
			if(function_exists('mb_ereg_replace')){
				$v = @mb_ereg_replace('　', ' ', $v);
			}else{
				$v = jstr_replace('　', ' ', $v); 
			}
			$v = ereg_replace(" {2,}", " ", $v);
			$v = trim($v);
			
			//Googleキャッシュのスキップ
			if($google_cache && ereg('^cache:',$v)) continue;
			if($v == "") continue;

			$v = "［".ereg_replace(' ', '］&nbsp;［', $v)."］";
			
			$keyword = $v;
			break;
		}
	}
	
	return $keyword;
}


?>