/* *
 * jquery.tgImageChangeV2.1
 * 
 * 【概要】
 * メイン画像とサムネイル画像を用意し、カーソルを合わせた画像をメイン画像として切り替えて表示、
 * 同時に、設定しているメッセージを表示させるスクリプトです。
 * 
 * アニメーション時間を両方「0」にすると、パラパラと切り替わります。
 * メイン画像のclass名と同名の"name"名をサムネイルに設定してください（必須）。これにより、同画面に複数個の設置が可能です。
 * 
 * ＜ファイル名の命名規則＞
 * メイン画像：finename1.jpg
 * サムネイル画像：finename1_thumb.jpg
 * ※サムネイル画像は「メイン画像ファイル名_thumb」の形にしてください。"_thumb"部分は固定です。
 * 
 * ＜画像のclass名＞
 * メイン画像：任意
 * サムネイル画像："selectorThumb"オプションにて指定
 * 
 * ＜メイン画像とサムネイル画像の関連付け＞
 * メイン画像：<img src="./img/img2.jpg" width="350" height="350" alt="img2" class="mainImage" />
 *                                                                                  ^^^^^^^^^
 * サムネイル：<img src="./img/img2_thumb.jpg"  width="90" height="90" alt="img2_thumb" class="thumb" name="mainImage" />
 *                                                                                                    ^^^^^^^^^^^^^^^^
 * ※サムネイルにメイン画像のclass名と同じname名を指定してください。
 * 
 * ＜メッセージ＞
 * リスト形式のメッセージを用意し、オプションで指定するclass名を付与した<div>で括ってください。
 * liタグのclass名は、サムネイル画像のrelと同名になるようにしてください。
 * 
 * 
 * @Copyright : 2014 toogie | http://wataame.sumomo.ne.jp/archives/1841
 * @Version   : 2.1
 * @Modified  : 2014-05-20
 * 
 */
;(function($){
    $.fn.tgImageChange = function(options){

        var opts = $.extend({}, $.fn.tgImageChange.defaults, options);

        $(opts.selectorThumb).mouseover(function(){

            // "_thumb"を削った画像名取得
            var selectedSrc = $(this).attr('src').replace(/^(.+)_thumb(\.gif|\.jpg|\.png+)$/, "$1"+"$2");

            // name取得
            var selectedName = $(this).attr('name');

            // 関連付けされているメイン画像入れ替え
            $('img.'+selectedName).stop().fadeOut(opts.fadeOutTime,
                function(){
                    $('img.'+selectedName).attr('src', selectedSrc).stop().fadeIn(opts.fadeInTime);
                }
            );

            // サムネイルの枠を変更
            $(this).css({"border":opts.thumbCssBorder});

            // メッセージ処理
            // 表示させる"note番号"を取得
            var targetNote = $(this).attr('rel');

            // 対象テキストの class="hide" を削除して表示
            $('div.'+opts.messageClass+' ul.'+selectedName+' li.'+targetNote).removeClass('hide');

            // 対象テキスト以外に class="hide" を付加して非表示
            $('div.'+opts.messageClass+' ul.'+selectedName+' li').not('li.'+targetNote).addClass('hide');
        });

        // マウスアウトでサムネイル枠を元に戻す
        $(opts.selectorThumb).mouseout(function(){
            $(this).css({"border":""});
        });
    }
})(jQuery);
